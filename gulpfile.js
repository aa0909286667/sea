const { exec } = require('child_process')
const gulp = require('gulp')
const replace = require('gulp-replace')
const zip = require('gulp-zip')
const gzip = require('gulp-gzip')
// const uglify = require('gulp-uglify')
const clean = require('gulp-clean')
const rename = require('gulp-rename')
const path = require('path')
const cleanCSS = require('gulp-clean-css')
const cssWrap = require('gulp-css-wrap')
const args = require('process.args')()
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const cssmin = require('gulp-cssmin')

// gulp themes --theme=custom-theme
gulp.task('generateTheme', () => exec('yarn build:theme', (err, stdout, stderr) => {
  if (err) {
    return err
  }
}))
gulp.task('css-wrap', () => {
  const themeName = args.theme.theme ? args.theme.theme : 'custom-theme'
  const customThemeName = args.theme.theme ? `.${args.theme.theme}` : '.custom-theme'
  return gulp.src(path.resolve('./theme/index.css')).pipe(cssWrap({ selector: customThemeName })).pipe(cleanCSS()).pipe(gulp.dest(`./src/styles/theme/${themeName}`))
})

gulp.task('move-font', () => {
  const themeName = args.theme.theme ? args.theme.theme : 'custom-theme'
  return gulp.src(['./theme/fonts/**']).pipe(gulp.dest(`./src/styles/theme/${themeName}/fonts`))
})
gulp.task('cleanThemeFile', () => {
  console.log('\n开始清空theme目录...\n')
  return gulp.src(['./theme'], {
    read: false, allowEmpty: true
  }).pipe(clean())
})

const apiConfig = {
  prod: {
    configsHttp: '[\'ind-ocs361.helowod.com:1765\',\'ocsapi1964.ofgdii.cn\',\'ocsapi-ws.lanyunlanyun.com\',\'ocsapi-aka.yujuntools.com\',\'ocsincp.foshanmsk.com\',\'ind-ocs371.jindoulm.com:1865\',\'ocsapi1964.yalanson.com\',\'ocsapi-aka.blackkhaki918.com\']',
    configsHttps: '[\'ind-ocs361.helowod.com:1766\',\'ocsapi1964.ofgdii.cn\',\'ocsapi-ws.lanyunlanyun.com\',\'ocsapi-aka.yujuntools.com\',\'ocsincp.foshanmsk.com\',\'ind-ocs371.jindoulm.com:1866\',\'ocsapi1964.yalanson.com\',\'ocsapi-aka.blackkhaki918.com\']'
  },
  uat: {
    configsHttp: '[\'fnt2uat.bytechn.com\']',
    configsHttps: '[\'fnt2uat.bytechn.com\']'
  }
}
const assetDir = 'seasys'
const placeholder = '%cdn%'

function genAppVersion () {
  const version = JSON.parse(process.env.npm_config_argv).original.find(v => /^--ver=/.test(v))
  if (version) return version.split('=')[1]

  const now = new Date()
  const yy = now.getYear().toString().slice(1, 3)
  let mm = now.getMonth() + 1
  mm = mm < 10 ? `0${mm}` : mm
  let dd = now.getDate()
  dd = dd < 10 ? `0${dd}` : dd
  return `${yy}${mm}${dd}`
}

const appVersion = genAppVersion()

gulp.task('rewritejs', () => {
  console.log('js文件处理中...')
  const env = JSON.parse(process.env.npm_config_argv).cooked[1].split(':')[1] || 'uat'
  const jpath = `"${assetDir}/${appVersion}/js/"`
  const cpath = `"${assetDir}/${appVersion}/css/"`
  const opath = `"${assetDir}/${appVersion}`
  const cdn = env === 'uat'
    ? '(window.localPath ? window.localPath : window.cdnPath) + "/"'
    : 'window.cdnPath + "/"'
  console.log('cdn => ', cdn)
  return gulp.src(`./dist/${assetDir}/${appVersion}/js/*.js`)
    // .pipe(replace(/['"]\/cp\/['"]/g, 'window.cdnPath+"/"'))
    .pipe(replace(new RegExp(`['"]${placeholder}/['"]`, 'g'), cdn))
    .pipe(replace(new RegExp(jpath, 'g'), `"${assetDir}/"+window.verPath+"/js/"`))
    .pipe(replace(new RegExp(cpath, 'g'), `"${assetDir}/"+window.verPath+"/css/"`))
    .pipe(replace(new RegExp(opath, 'g'), `"/${assetDir}/"+window.verPath+"`))
    .pipe(gulp.dest(`./dist/${assetDir}/${appVersion}/js/`))
})
gulp.task('rewritecss', () => {
  const cpath = `../../${assetDir}/${appVersion}/`
  return gulp.src(`./dist/${assetDir}/${appVersion}/css/*.css`).pipe(replace(new RegExp(cpath, 'g'), '')).pipe(gulp.dest(`./dist/${assetDir}/${appVersion}/css/`))
})

gulp.task('compress-js', () => {
  console.log('开始压缩js文件...')
  return gulp.src([`./dist/${assetDir}/${appVersion}/js/*`, `./dist/${assetDir}/${appVersion}/js/*/*`])
    // .pipe(uglify())
    // .pipe(gzip({ threshold: 1024 }))
    .pipe(gulp.dest(`./dist/${assetDir}/${appVersion}/js/`))
})
gulp.task('compress-css', () => {
  console.log('开始压缩css文件...')
  return gulp.src(`./dist/${assetDir}/${appVersion}/css/*`)
  // .pipe(gzip({ threshold: 1024 }))
  .pipe(gulp.dest(`./dist/${assetDir}/${appVersion}/css/`))
})

gulp.task('uathtml', () => gulp.src(['./dist/index.html']).pipe(replace(/['"]APPVERSION['"]/, appVersion)).pipe(replace(/['"]CONFIGSHTTP['"]/, apiConfig.uat.configsHttp)).pipe(replace(/['"]CONFIGSHTTPS['"]/, apiConfig.uat.configsHttps)).pipe(gulp.dest('./dist/')))
gulp.task('prodhtml', () => gulp.src(['./dist/index.html']).pipe(replace(/['"]APPVERSION['"]/, appVersion)).pipe(replace(/['"]CONFIGSHTTP['"]/, apiConfig.prod.configsHttp)).pipe(replace(/['"]CONFIGSHTTPS['"]/, apiConfig.prod.configsHttps)).pipe(gulp.dest('./dist/')))

gulp.task('UAT_ZipHtml', () => {
  console.log('\n开始打包index.html文件\n')
  return gulp.src(['./dist/index.html']).pipe(zip(`${assetDir}_uat_v${appVersion}_WEB.zip`)).pipe(gulp.dest('./dist/'))
})
gulp.task('UAT_ZipSource', () => {
  console.log('\n开始打包资源文件\n')
  return gulp.src(`./dist/${assetDir}/**/*.*`).pipe(zip(`${assetDir}_uat_v${appVersion}_CDN.zip`)).pipe(gulp.dest('./dist/'))
})
gulp.task('PROD_ZipHtml', () => {
  console.log('\n开始zip打包index.html文件\n')
  return gulp.src(['./dist/index.html']).pipe(zip(`${assetDir}_prod_v${appVersion}_WEB.zip`)).pipe(gulp.dest('./dist/'))
})
gulp.task('PROD_ZipSource', () => {
  console.log('\n开始打包资源文件\n')
  return gulp.src(`./dist/${assetDir}/**/*.*`).pipe(zip(`${assetDir}_prod_v${appVersion}_CDN.zip`)).pipe(gulp.dest('./dist/'))
})

gulp.task('publish', () => exec('python3 publish.py', (err, stdout, stderr) => {
  if (err) {
    return err
  }
}))

gulp.task('compileThemeChalk', () => {
  return gulp.src('./src/core/theme-chalk/index.scss').pipe(sass.sync()).pipe(autoprefixer({
    browsers: ['ie > 9', 'last 2 versions'],
    cascade: false
  })).pipe(cssmin()).pipe(rename('theme-chalk.css')).pipe(gulp.dest('./public/static/css'))
})

gulp.task('copyThemeFonts', () => {
  return gulp.src('./src/core/theme-chalk/fonts/**').pipe(cssmin()).pipe(gulp.dest('./public/static/css/fonts'))
})

gulp.task('uat', gulp.series('rewritejs', 'rewritecss', 'compress-js', 'compress-css', 'uathtml', 'UAT_ZipHtml', 'UAT_ZipSource'))
gulp.task('prod', gulp.series('rewritejs', 'rewritecss', 'compress-js', 'compress-css', 'prodhtml', 'PROD_ZipHtml', 'PROD_ZipSource'))
gulp.task('release', gulp.series('prod'))
gulp.task('publish', gulp.series('uat', 'publish'))
gulp.task('default', gulp.series('uat'))
gulp.task('theme', gulp.series('generateTheme', 'css-wrap', 'move-font', 'cleanThemeFile'))

// yarn build:uat
// yarn build:prod

// yarn build:uat --ver=211229
// yarn build:prod --ver=210701
