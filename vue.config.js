const path = require('path')
const fs = require('fs')
const pkg = require('./package.json')

// const ReplaceSource = require('./replaceSource')

function resolve (dir) {
  return path.join(__dirname, dir)
}

const name = '东南亚彩票后台管理系统' || pkg.name
const ocsKey = 'seasys'

function genAppVersion () {
  const version = JSON.parse(process.env.npm_config_argv).original.find(v => /^--ver=/.test(v))
  if (version) return version.split('=')[1]
  const now = new Date()
  const yy = now.getYear().toString().slice(1, 3)
  let mm = now.getMonth() + 1
  mm = mm < 10 ? `0${mm}` : mm
  let dd = now.getDate()
  dd = dd < 10 ? `0${dd}` : dd
  return `${yy}${mm}${dd}`
}
const isProd = process.env.NODE_ENV === 'production'
const isPreview = process.env.ENV === 'preview'
// console.log('process.env.ENV', process.env.ENV)
const port = process.env.port || process.env.npm_config_port || 8100 // dev port
const placeholder = '%cdn%'
const publicPath = isProd ? placeholder : ''
const outputDir = 'dist'
const assetsDir = !isProd ? 'static' : `${ocsKey}/${genAppVersion()}`
module.exports = {
  publicPath,
  outputDir,
  assetsDir,
  productionSourceMap: false,

  // 去除所有文件hash
  filenameHashing: false,

  devServer: {
    port,
    open: true,
    overlay: {
      warnings: false,
      errors: false
    },
    // before: require('./mock/mock-server.js'),
    proxy: null,
    after: undefined
  },

  configureWebpack: (config) => {
    const configNew = {}
    configNew.name = name
    configNew.resolve = {
      alias: {
        '@': resolve('src'),
        src: path.resolve(__dirname, 'src'),
        assets: path.resolve(__dirname, 'src/assets'),
        components: path.resolve(__dirname, 'src/components'),
        views: path.resolve(__dirname, 'src/views'),
        lang: path.resolve(__dirname, 'src/lang'),
        styles: path.resolve(__dirname, 'src/styles'),
        api: path.resolve(__dirname, 'src/api'),
        utils: path.resolve(__dirname, 'src/utils'),
        store: path.resolve(__dirname, 'src/store'),
        router: path.resolve(__dirname, 'src/router'),
        vendor: path.resolve(__dirname, 'src/vendor'),
        static: path.resolve(__dirname, 'public/static'),
        core: path.resolve(__dirname, 'src/core')
      }
    }
    return configNew
  },

  chainWebpack (config) {
    config.plugins.delete('preload') // TODO: need test
    config.plugins.delete('prefetch') // TODO: need test

    config.plugins.delete('named-chunks')
    config.module.rule('md').test(/\.md$/).use('text-loader').loader('text-loader').end()
    config.module.rule('svg').exclude.add(resolve('src/icons')).end()
    config.module.rule('icons').test(/\.svg$/).include.add(resolve('src/icons')).end().use('svg-sprite-loader').loader('svg-sprite-loader').options({
      symbolId: 'icon-[name]'
    }).end()
    const imagesRule = config.module.rule('images')
    imagesRule.test(/\.(png|jpe?g|gif|webp|svg)(\?.*)?$/).exclude.add(resolve('src/icons')).end()
    config.module.rule('vue').use('vue-loader').loader('vue-loader').tap((options) => {
      options.compilerOptions.preserveWhitespace = true
      return options
    }).end()
    config.plugin('html').tap((args) => {
      const init = !isProd ? '' : (!isPreview ? fs.readFileSync('./config/init.js', 'utf8') : fs.readFileSync('./config/init-view.js', 'utf8'))
      if (isProd) {
        // console.log(process.env.ENV)
        args[0].minify = {
          removeComments: true,
          collapseWhitespace: false,
          removeAttributeQuotes: true,
          collapseBooleanAttributes: true,
          removeScriptTypeAttributes: true
        }
        args[0].inject = false // 手动写入资源
      }
      args[0].exjs = {
        init
      }
      return args
    })

    config.when(!isProd,
      (config) => config.devtool('cheap-source-map'))

    config.when(isProd,
      (config) => {
        config.plugin('ScriptExtHtmlWebpackPlugin').after('html').use('script-ext-html-webpack-plugin', [{
          inline: /runtime\..*\.js$/
        }]).end()

        config.optimization.splitChunks({
          chunks: 'all',
          cacheGroups: {
            libs: {
              name: 'vendor',
              test: /[\\/]node_modules[\\/]/,
              priority: 10,
              chunks: 'initial' // only package third parties that are initially dependent
            },
            // elementUI: {
            //   name: 'globals', // split elementUI into a single package
            //   priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
            //   test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
            // },
            commons: {
              name: 'commons',
              test: resolve('src/components'), // can customize your rules
              minChunks: 3, //  minimum common number
              priority: 5,
              reuseExistingChunk: true
            }
          }
        })
        config.optimization.runtimeChunk({ name: 'manifest' })
        // use 新增插件
        // config.plugin('replaceSource').use(
        //   new ReplaceSource({
        //     // pattern: 'manifest', // 包含manifest的文件名
        //     // jsPlaceholder: placeholder, // 占位字符
        //     manifestPlaceholder: [
        //       {
        //         holder: placeholder,
        //         solution: `window.cdnPath+"/${ocsKey}/"`
        //       },
        //       {
        //         holder: `${assetsDir}/js/`,
        //         solution: 'window.verPath+"/js/"'
        //       },
        //       {
        //         holder: `${assetsDir}/css/`,
        //         solution: 'window.verPath+"/css/"'
        //       }
        //     ],
        //     cssPlaceholder: `../../${assetsDir}`, // css文件需要替换字符
        //     // jsTarget: `window.cdnPath+"/"+${ocsKey}`, // 替换目标字符
        //     // manifestTarget: `window.cdnPath+"/"`, // 替换目标字符
        //     cssTarget: '..' // css替换目标字符
        //   })
        // )
        config.plugin('copy').tap((args) => {
          args[0] = []
          args[0].push(
            {
              from: path.resolve(__dirname, 'public/index.html'),
              to: path.resolve(__dirname, 'dist/')
            },
            {
              from: path.resolve(__dirname, 'public/static'),
              to: path.resolve(__dirname, `dist/${assetsDir}`),
              ignore: ['.*']
            }
          )
          return args
        })
      })
  },

  pwa: {},

  // css相关配置
  css: {
    loaderOptions: {}
  },

  pluginOptions: {}
}
