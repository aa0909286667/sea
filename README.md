# tcgsea-gulp-ui   东南亚彩项目说明

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn start
```
### Compiles and minifies for UAT
```
yarn build:uat
```
### Compiles and minifies for production
```
yarn build:prod
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
