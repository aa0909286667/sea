#!/bin/bash
# admin 自动部署脚本 uat@version 0.0.2
# @svn更新资源及打包部署
# @author xw 2018-11-1 16:47:45
CDNPATH=/home/www/webstatic/cp/seasys/

# 取第一个参数 版本号
ver=$1
# 部署CDN
echo '开始更新资源 ...'
echo '切换root用户 ...'
sudo -i
echo '切换到资源目录并更新资源 ...'
cd /home/lotuser1/admin-svn && svn update .
echo '开始程序打包'
npm run build:uat
echo '开始解压资源到CDN目录 ...'
unzip -o /home/lotuser1/admin-svn/dist/seasys_uat_v${ver}_CDN.zip -d ${CDNPATH}
chmod -R 775 ${CDNPATH}
chown -R www-data:www-data ${CDNPATH}
echo '发版完成,请更新ocs版本号!!!'

echo 'done！'
