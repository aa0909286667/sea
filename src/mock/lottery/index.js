/**
 * Created by Jane on 2017/7/15.
 */
import Mock from 'mockjs'
const Random = Mock.Random

// 获取彩种维护接口
const getLotteryTypeUpStatus = () => {
  const params = {
    status: '1',
    lotteryCode: '010',
    lotteryName: 'aaa',
    createTime: '121212'
  }
  return {
    data: {
      data: [params, params]
    }
  }
}
export { getLotteryTypeUpStatus }
