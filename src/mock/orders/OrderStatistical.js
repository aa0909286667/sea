import Mock from 'mockjs'

const Random = Mock.Random

// 统计列表
const getOrderStatisticalList = () => {
  const elements = [];
  for (let i = 0; i < 50; i++) {
    const rData = {
      unsettleOrderNum: Random.natural(0, 10),
      unPayoutOrderNum: Random.natural(0, 10),
      agenRebateOrderNum: Random.natural(0, 10),
      totalOrderNum: Random.natural(0, 10000),
      revokeOrderNum: Random.natural(0, 10000),
      winOrderNum: Random.natural(0, 10000),
      drawOrderNum: Random.natural(0, 10000),
      loseOrderNum: Random.natural(0, 10000),
      spCode: Random.word(),
    }
    elements.push(rData)
  }
  const data = {
    data: {
      items: elements
    }
  }
  return data
}

export { getOrderStatisticalList }
