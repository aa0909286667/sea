import Mock from 'mockjs'

const Random = Mock.Random

// 订单列表
const getOrderInfoList = () => {
  const elements = [];
  for (let i = 0; i < 50; i++) {
    const rData = {
      spCode: Random.word(),
      channel: Random.word(),
      lotteryName: Random.cname(),
      clientType: Random.natural(0, 5),
      issue: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      orderCode: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      playName: Random.cname(),
      loginId: Random.natural(100, 10000),
      ip: Random.ip(),
      buyNumber: Random.natural(1, 10000),
      singleMoney: Random.natural(1, 10000),
      orderMoney: Random.natural(1, 10000) * 2,
      prize: Random.natural(1, 2000),
      rebateRate: Random.natural(0, 100) + '%',
      rebateMoney: Random.natural(0, 100),
      openCode: '',
      buyCode: '',
      winMoney: Random.float(0, 1000000, 2, 2),
      winCount: Random.natural(0, 100),
      createTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
      status: Random.natural(0, 6),
      profitMoney: Random.natural(0, 1000000),
      trace: Random.natural(0, 1),
      winningToStop: Random.natural(0, 1),
      traceCode: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      settleNum: Random.natural(0, 100),
      settleTime: Random.datetime('yyyy-MM-dd HH:mm:ss')
    }
    elements.push(rData)
  }
  const data = {
    totalCount: elements.length,
    data: {
      elements: elements
    }
  }
  return data
}

export {getOrderInfoList}
