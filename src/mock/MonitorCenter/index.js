import Mock from 'mockjs'

const Random = Mock.Random

// 订单列表
const getOrderExceptionList = () => {
  const items = [];
  for (let i = 0; i < 10; i++) {
    const rData = {
      id: Random.guid(),
      spCode: Random.word(),
      lotteryName: Random.cname(),
      issue: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      orderCode: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      playName: Random.cname(),
      loginId: Random.natural(100, 10000),
      createTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
      type: Random.natural(1, 9),
      selfType: Random.natural(0, 2),
      endTime: Random.datetime('yyyy-MM-dd HH:mm:ss')
    }
    items.push(rData)
  }
  const data = {
    data: {
      items: items,
      totalCount: items.length,
    }
  }
  return data
}

export { getOrderExceptionList }
