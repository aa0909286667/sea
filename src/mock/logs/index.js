import Mock from 'mockjs'

const Random = Mock.Random

const getMemberLoggerMembeList = () => {
  const elements = [];
  for (let i = 0; i < 100; i++) {
    const rData = {
      spCode: Random.word(),
      loginId: Random.word(),
      createTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
      loginIp: Random.ip()
    }
    elements.push(rData)
  }
  const data = {
    totalCount: elements.length,
    data: {
      elements: elements
    }
  }
  return data
}
export {getMemberLoggerMembeList}
