/* Created by fx 2017-07-15 */
import Mock from 'mockjs'
const Random = Mock.Random


// IF_5417平台商列表接口
const getSpInfoList = () => {
  const receiveFiled = {
    id: 2,
    spCode: 'cm1',
    spName: '时时彩',
    creditMoney: 10000,
    cashMode: 1,
    parentId: 0,
    parentName: 'test',
    status: 1,
    domain: 'www.abc.com',
    link: 'link href',
    createTime: 1500801642938,
    description: 'desc info'
  }
  return {
    code: 0,
    data: {
      elements: [receiveFiled]
    }
  }
}

/* 5.4.24 平台商盘口显示列表
 * 接口 SpTplInfoList
 */
const getSpTplInfoList = () => {
  const randomData = {
    id: 1,
    spCode: 'SP_001',
    tplName: '名称A',
    tplType: 1,
    status: 1,
    description: 'desc'
  }
  return {
    code: 0,
    data: {
      spTplInfoList: [randomData]
    }
  }
}

/**
 * IF_5428平台商已开通彩种类型接口
 */
const getSpLotteryTypeOpenList = () => {
  const receiveFiledFn = function(name, id) {
    return {
      id: id || 0,
      typeName: name || '时时彩'
    }
  }
  return {
    code: 0,
    data: {
      lotteryTypeList: [receiveFiledFn('时时彩', 10), receiveFiledFn('十一选五', 12)]
    }
  }
}


/**
 * IF_5430平台商盘口的返水设置列表接口
 */
const getSpTplRebateSetList = () => {
  const receiveFiledFn = function(name, id) {
    return {
      id: 2,
      spCode: 'SP_001',
      spTplId: 1,
      lotteryTypeId: id || 0,
      lotteryTypeName: name || '时时彩',
      lotteryTypeRate: 1200,
      rebateRate: 1000
    }
  }
  return {
    code: 0,
    data: {
      spTplRebateSetList: [receiveFiledFn('时时彩', 20), receiveFiledFn('十一选五', 21)]
    }
  }
}

/**
 * IF_5431平台商盘口的返水设置接口
 */
const postSpTplRebateSetUp = () => {
  const msg = 'sucess';
  return {
    code: 0,
    desc: msg
  }
}
// 其它

export { getSpInfoList, getSpTplInfoList, getSpTplRebateSetList, postSpTplRebateSetUp, getSpLotteryTypeOpenList }
