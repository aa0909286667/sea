



import Mock from 'mockjs'

const Random = Mock.Random

// 金流列表
const getMemberCashflowList = () => {
  const elements = [];
  for (let i = 0; i < 50; i++) {
    const rData = {
      spCode: Random.word(),
      lotteryName: Random.cname(),
      orderCode: Random.datetime('yyyyMMdd') + Random.natural(1000, 10000),
      loginId: Random.natural(100, 10000),
      singleMoney: Random.natural(1, 10000),
      cashType: Random.natural(1, 4),
      cashMode: Random.natural(1, 2),
      historyMoney: Random.float(0, 1000000, 2, 2),
      cashMoney: Random.float(0, 1000000, 2, 2),
      balanceMoney: Random.float(0, 1000000, 2, 2),
      createTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
      description: Random.cword(20),
    }
    elements.push(rData)
  }
  const data = {
    totalCount: elements.length,
    data: {
      elements: elements
    }
  }
  return data
}

export {getMemberCashflowList}
