import { fetchCmdData } from './fetchCmdData'

export default {
  res: req => {
    const body = JSON.parse(req.body)
    console.log('cmd:', body.cmd, ' Fetch Mock Data -->')
    const data = fetchCmdData(body)
    console.log(data)
    console.log('<-- Fetch Mock Data')
    return data
  }
}

