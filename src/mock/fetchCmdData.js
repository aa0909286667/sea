// 导入各模块下需要模拟的数据
// import * as SPTPL from './sptpl'
// import {getLotteryTypeUpStatus} from './dataBase'
// import {getMemberList, getMemberDetail} from './members'
// import {getMemberLoggerMembeList} from './logs'
import { getOrderInfoList } from './orders'
// import {getMemberCashflowList} from './fund'

/**
 * 匹配根据接口cmd 返回数据给mock
 * @param body
 * @returns {*}
 */
const fetchCmdData = body => {
  const cmd = body.cmd
  console.log(body)
  // if (cmd === 'SpInfoList') return SPTPL.getSpInfoList();
  // if (cmd === 'SpTplInfoList') return SPTPL.getSpTplInfoList();
  // if (cmd === 'SpTplRebateSetList') return SPTPL.getSpTplRebateSetList();
  // if (cmd === 'SpTplRebateSetUp') return SPTPL.postSpTplRebateSetUp();
  // if (cmd === 'SpLotteryTypeOpenList') return SPTPL.getSpLotteryTypeOpenList();
  // if (cmd === 'LotteryTypeUpStatus') return getLotteryTypeUpStatus();
  // // if (cmd === 'MemberInfoList') return getMemberList();
  // if (cmd === 'MemberDetail') return getMemberDetail();
  // if (cmd === 'MemberLoggerMembeList') return getMemberLoggerMembeList();
  if (cmd === 'OrderInfoList') return getOrderInfoList()
  // if (cmd === 'MemberCashflowList') return getMemberCashflowList();
  return {}
}

export { fetchCmdData }
