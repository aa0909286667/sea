import Mock from 'mockjs'

const Random = Mock.Random

// 会员列表
const getMemberList = () => {
  const elements = [];
  for (let i = 0; i < 100; i++) {
    const rData = {
      uid: Random.natural(100, 10000),
      spCode: Random.word(),
      cashMode: Random.natural(0, 1),
      loginId: Random.natural(100, 10000),
      balance: Random.float(0, 1000000, 2, 2),
      regTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
      status: Random.natural(0, 1),
      isLogin: Random.natural(0, 1)
    }
    elements.push(rData)
  }
  const data = {
    // pageNo:query.pageNo,
    // pageSize:query.pageSize,
    totalCount:elements.length,
    // totalPages:Math.ceil(query.totalCount/query.pageSize),
    elements: elements
  }
  return data
}

// 会员详情
const getMemberDetail = () => {
  const data = {
    uid: Random.natural(100, 10000),
    spCode: 'sp-' + Random.natural(1, 1000),
    loginId: Random.natural(100, 10000),
    accountType: Random.natural(1, 3),
    realName: Random.cname(),
    cashMode: Random.natural(0, 1),
    balance: Random.float(0, 1000000, 3, 2),
    regTime: Random.datetime('yyyy-MM-dd HH:mm:ss'),
    status: Random.natural(0, 1),
    regIp: Random.ip(),
    description: Random.cparagraph(),
    parentId: Random.natural(100, 10000),
    agentStatus: Random.natural(0, 1),
    levelPath: '',
    rebateRateRule: '',
    spTplId: Random.natural(100, 10000),
    spTplName: Random.cword(5, 7)
  }
  return data
}
export {getMemberList, getMemberDetail}
