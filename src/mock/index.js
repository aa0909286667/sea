import Mock from 'mockjs'
import LocalApi from './localApi'
// 注入ISMOCK 走如下请求路径
Mock.mock(/\/local/, 'post', LocalApi.res)

export default Mock
