/**
 * Created by fx on 2017/7/19.
 */
import fetch from 'utils/fetch'

// IF_5417平台商列表接口 取spCode
export function getSpInfoList (query) {
  const params = {}
  params.cmd = 'SpInfoList'
  params.pageSize = query.pageSize
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5424平台商盘口显示列表
 * 服务器未返回分页参数
 * @param query
 */
export function getSpTplInfoList (query) {
  const params = {}
  // 接口名
  params.cmd = 'SpTplInfoList';
  // 解构参数
  // ({ spCode: params.spCode, pageNo: params.pageNo, pageSize: params.pageSize } = query || {});
  ({ spCode: params.spCode } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5427平台商盘口复制接口
 * @param query
 */
export function postSpTplInfoCopy (query) {
  const params = {}
  params.cmd = 'SpTplInfoCopy';
  ({ spTplId: params.spTplId, tplName: params.tplName, spCode: params.spCode } = query || {}) // field error tblName == tplName
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5426修改平台商盘口的名称接口
 * @param query
 */
export function postSpTplInfoUpName (query) {
  const params = {}
  params.cmd = 'SpTplInfoUpName';
  ({ spTplId: params.spTplId, tplName: params.tplName, spCode: params.spCode } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5428平台商已开通彩种类型接口
 * @param query
 */
export function getSpLotteryTypeOpenList (query) {
  const params = {}
  params.cmd = 'SpLotteryTypeOpenList';
  ({ spCode: params.spCode, spTplId: params.spTplId } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5429获得玩法类别接口
 * @param query
 * return promise | res -> playTabList: {playTabId, playTabName}
 */
export function getSpLotteryPlayTabOpenList (query) {
  const params = {}
  params.cmd = 'SpLotteryPlayTabOpenList';
  ({
    spCode: params.spCode,
    lotteryTypeId: params.lotteryTypeId,
    playTabMode: params.playTabMode
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5430平台商盘口的返水设置列表接口
 * @param query
 */
export function getSpTplRebateSetList (query) {
  const params = {}
  params.cmd = 'SpTplRebateSetList';
  ({ spTplId: params.spTplId, spCode: params.spCode } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5431平台商盘口的返水设置接口
 * @param query
 */
export function postSpTplRebateSetUp (query) {
  const params = {}
  params.cmd = 'SpTplRebateSetUp';
  ({
    spTplRebateSetList: params.spTplRebateSetList,
    spCode: params.spCode,
    spTplId: params.spTplId
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5432平台商盘口所有已开通彩种列表接口
 * @param query
 */
export function getSpTplLotterySetList (query) {
  const params = {}
  params.cmd = 'SpTplLotterySetList';
  ({
    spTplId: params.spTplId,
    spCode: params.spCode,
    lotteryTypeId: params.lotteryTypeId,
    lotteryRegionId: params.lotteryRegionId
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5433平台商盘口彩种设置接口
 * @param query
 */
export function postSpTplLotterySetUp (query) {
  const params = {}
  params.cmd = 'SpTplLotterySetUp';
  ({
    spCode: params.spCode,
    spTplId: params.spTplId,
    lotteryTypeId: params.lotteryTypeId,
    spTplLotterySetList: params.spTplLotterySetList
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

export function postSpTplLotteryPlayModeSwitchUp (query) {
  const params = {}
  params.cmd = 'SpTplLotteryPlayModeSwitchUp'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/**
 * IF_5434平台商盘口玩法列表
 * @param query
 */
export function getSpTplPlayList (query) {
  const params = {}
  params.cmd = 'SpTplPlayList';
  ({
    spTplId: params.spTplId,
    spCode: params.spCode,
    playGroupId: params.playGroupId,
    playTypeId: params.playTypeId
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5435平台商盘口玩法设置
 * @param query
 */
export function postSpTplPlaySetUp (query) {
  const params = {}
  params.cmd = 'SpTplPlaySetUp';
  ({
    spTplPlaySetList: params.spTplPlaySetList,
    spCode: params.spCode,
    spTplId: params.spTplId,
    playGroupId: params.playGroupId,
    upPrize: params.upPrize
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5436平台商盘口热门彩种列表接口
 * @param query
 */
export function getSpTplLotteryHotList (query) {
  const params = {}
  params.cmd = 'SpTplLotteryHotList';
  ({
    spTplId: params.spTplId,
    spCode: params.spCode,
    lotteryId: params.lotteryId
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5437平台商盘口热门彩种设置接口
 * @param query
 */
export function postSpTplHotLotterySetUp (query) {
  const params = {}
  params.cmd = 'SpTplHotLotterySetUp';
  ({
    // spTplHotLotterySet = { id: int, hot: int, hotDesc: String}
    spCode: params.spCode,
    spTplId: params.spTplId,
    lotteryId: params.lotteryId,
    spTplHotLotterySetList: params.spTplHotLotterySetList
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5457平台商盘口首页推荐彩种列表
 * @param query
 */
export function getSpTplLotteryRecommendList (query) {
  const params = {}
  params.cmd = 'SpTplLotteryRecommendList';
  ({
    spTplId: params.spTplId,
    spCode: params.spCode
    // lotteryTypeId: params.lotteryTypeId
  } = query || {})
  console.log(params)
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/**
 * IF_5458盘口的首页推荐彩种设置
 * @param query
 */
export function postSpTplRecommendLotterySetUp (query) {
  const params = {}
  params.cmd = 'SpTplRecommendLotterySetUp';
  ({
    // items = { id: int, recommend: 0-否 1-是}
    spTplId: params.spTplId,
    items: params.items,
    spCode: params.spCode
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 代理列表
export function getBandingAgentInfoList (query) {
  const params = {
    cmd: 'BandingAgentInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// export function IF(query) {
//   const params = {};
//   params.cmd = '';
//   ({} = query || {});
//   return fetch({
//     method: 'post',
//     data: Object.assign({},  params)
//   });
// }
export function postSpTplLotteryPlayModeUp (query) {
  const params = {
    cmd: 'SpTplLotteryPlayModeUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postSpTplLotteryCreditModeUp (query) {
  const params = {
    cmd: 'SpTplLotteryCreditModeUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获取定位胆设置彩种列表
export function getDwdLimitList (query) {
  const params = {
    cmd: 'DwdLimitList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 更新定位胆设置彩种接口
export function postDwdLimitSetSave (query) {
  const params = {
    cmd: 'DwdLimitSet'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 厅主盘口指定彩种的玩法列表接口
export function getSpTplLotteryPlayList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SpTplLotteryPlayList', ...query }
  })
}

// 厅主盘口指定彩种的玩法设置保存接口
export function postSpTplLotteryPlaySetUp (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SpTplLotteryPlaySetUp', ...query }
  })
}

// 厅主盘口指定彩种的玩法设置开关接口
export function postSpTplLotteryPlaySwitchUp (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SpTplLotteryPlaySwitchUp', ...query }
  })
}

// 获取盘口中各个彩种中玩法类型默认显示列表
export function getSpTplPlaytypeSetList (query) {
  const params = {
    cmd: 'SpTplPlaytypeSetList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 厅主盘口彩种玩法类型默认设置接口
export function postSpTplPlaytypeDeafultUp (query) {
  const params = {
    cmd: 'SpTplPlaytypeDeafultUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
