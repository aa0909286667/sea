import fetch from 'utils/fetch'

// import { commonParams } from 'api/config'

export function getLoggerApiList (query) {
  const params = {
    cmd: 'LoggerApiList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLoggerServiceList (query) {
  const params = {
    cmd: 'LoggerServiceList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLoggerTrdErrorList (query) {
  const params = {
    cmd: 'LoggerTrdErrorList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getOrderBigAmountList (query) {
  const params = {
    cmd: 'OrderBigAmountList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getOrderExceptionList (query) {
  const params = {
    cmd: 'OrderExceptionList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getOrderCancelExceptionList (query) {
  const params = {
    cmd: 'OrderCancelExceptionList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLoggerOpencodeList (query) {
  const params = {
    cmd: 'LoggerOpencodeList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function LoggerCloudInfoList (query) {
  const params = {
    cmd: 'LoggerCloudInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLoggerHttpErrorList (query) {
  const params = {
    cmd: 'LoggerHttpErrorList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postLoggerHttpErrorReq (query) {
  const params = {
    cmd: 'LoggerHttpErrorReq'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getTaskInfoList (query) {
  const params = {
    cmd: 'taskInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getTaskSubInfoList (query) {
  const params = {
    cmd: 'taskSubInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLoggerBusiList (query) {
  const params = {}
  params.cmd = 'LoggerBusiList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getOrderExceptionProcessor (query) {
  const params = {}
  params.cmd = 'OrderExceptionProcessor'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
