/**
 * Created by Jane on 2017/7/25.
 */
import fetch from 'utils/fetch'

// 5.5.4.2 历史开奖信息查询接口 【LotteryHistoryOpenInfo】
export function getLotteryHistoryOpenInfo (query) {
  const params = {}
  params.cmd = 'LotteryHistoryOpenInfo'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getLotteryLastOpenInfo (query) {
  const params = {}
  params.cmd = 'LotteryLastOpenInfo'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 开奖公告
export function getLotteryResultList (query) {
  const params = {
    cmd: 'LotteryNoticeList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
