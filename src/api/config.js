import { getToken } from 'utils/storage'

import store from 'store/index'

/**
 * 随机数范围[min, max)
 * @param min
 * @param max
 * @returns {*}
 */
export const getRandomInt = function (min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

/**
 * 随机数 时间戳去掉前两位 + 随机七位数
 */
export function genIdentity () {
  // console.log(new Date().getTime().toString())
  return `${(new Date().getTime()).toString()}${getRandomInt(1000000, 10000000).toString()}`
}

export function commonParams () {
  return {
    channel: 'bg',
    clientType: 1,
    identity: genIdentity(),
    version: '1.0',
    token: store.getters.token || getToken() || '',
    ztsLang: 'zh',
    langType: 'zh',
    accountType: 1
  }
}

/* ----二次授权--- */

export const ERR_OK = 0

/* mock 配置 */
export const ISMOCK = process.env.NODE_ENV !== 'production' ? {
  baseURL: 'http://localhost:9527', url: '/lottery-api/api/local'
} : {}
