import fetch from 'utils/fetch'
// import axios from 'axios'
// import { commonParams } from 'api/config'

export function loginByAccount (query) {
  // const url = '/api/loginIn'
  const params = {
    cmd: 'UserLogin'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
  // const data = Object.assign({}, commonParams(), params, query)
  // return axios.post(url, { params: data }).then((res) => {
  //   return Promise.resolve(res.data)
  // })
}

// 验证登录
export function postAdminAuthValidate (token) {
  const params = {
    cmd: 'AdminAuthValidate',
    token
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

export function logout (token) {
  const params = {
    cmd: 'UserLogout',
    token
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

export function getClientIp () {
  const params = {
    cmd: 'ClientIp'
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}
