import fetch from 'utils/fetch'
import { getRandomInt } from '../utils'

/* ajaxData 参数说明
 * dataType: 要请求的数据类型
 10:获得基础数据中所有彩种类型
 11:根据彩种类型获得基础数据中所有玩法分组
 12:根据玩法分组获得基础数据中所有玩法分类
 注：以上可以做联动查询
 13: 根据彩种类型及彩种所属地区获得基础数据中所有彩种
 注：10可以做为13联动查询的条件
 14: 根据彩种类型获得该类型下的所有地区,无地区时返回空列表
 注:该类型必须与10进行联动
 15:根据玩法分组获得该分组下所有彩种
 21: 获得所有正常状态的厅主列表[返回厅主编码和名称]
 22: 获得某个彩种下最近十期彩期
 23:获得所有厅自开彩彩种
 24: 获得所有系统自开彩彩种
 25: 获得所有自开彩彩种[包含厅自开彩及系统自开彩]
 * */
export function getAjaxData (query) {
  const params = {
    cmd: 'AjaxData'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 层级树
export function getAjaxAgentTree (query) {
  const params = {
    cmd: 'AjaxAgentTree'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 刷新服务端缓存
// export function RefreshCache() {
//   const params = {
//     cmd: 'RefreshCache'
//   }
//   return fetch({
//     method: 'post',
//     data: Object.assign({}, commonParams, params)
//   })
// }

export function RefreshLocalCache (query) {
  const { refreshType, refreshValue } = query
  let str = `refreshType=${refreshType}`
  if (refreshValue) str += `&refreshValue=${refreshValue}`
  return fetch({
    method: 'get',
    url: `${window.API_ADMINFRONT_PROD + window.API_F_PATH}/refreshLocalCache?${str}&r=${Math.random() * 956347}`,
    contentType: 'application/json; charset=utf-8',
    responseType: 'json',
    timeout: 3000
  })
}

// 报表导出
export function postReportExport (query) {
  return fetch({
    method: 'post',
    data: { ...query }
  })
}

// 获取服务器时间
export function getServerTime () {
  return fetch({
    // url: window.API_ADMINFRONT_PROD + '/oss/servertime',
    method: 'post',
    data: { cmd: 'ServerTime' }
  })
}

// 获取二维码
export function getQrCode () {
  return fetch({
    method: 'post',
    data: { cmd: 'qrCodeCreate' }
  })
}

// 轮询二维码
export function postScanQrCode (qrText) {
  return fetch({
    method: 'post',
    data: { cmd: 'qrCodeScan', qrText }
  })
}

// 公共二维码返回函数
export function postScanQrCodeQuery (data) {
  return fetch({
    method: 'post',
    data
  })
}

export function getClientIp () {
  return fetch({
    method: 'post',
    data: { cmd: 'ClientIp' }
  })
}

// 通过该接口查询总后台/厅主后台报表的排除厅
export function getReportSpcodeExclude () {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportSpcodeExclude' }
  })
}

// 一键维护
export function getIntercept () {
  return fetch({
    method: 'post',
    url: 'auth/api/',
    data: {
      id: getRandomInt(0, 1000),
      method: 'auth.cms.intercept.get',
      params: {},
      jsonrpc: '2.0'
    }
  })
}

// 用户账号的模糊查询
export function getMemberFuzzyQuery (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'MemberFuzzyQuery', ...query }
  })
}

// 通过厅前缀模糊匹配
export function getSpCodeFuzzyQuery (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SpCodeFuzzyQuery', ...query }
  })
}

// 对指定的彩种获得排程数据
export function getLotteryIssueAjaxQuery (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'LotteryIssueAjaxQuery', ...query }
  })
}
