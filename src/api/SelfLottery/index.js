import fetch from 'utils/fetch'

export function getSelfLotteryInfoList (query) {
  const params = {
    cmd: 'SelfLotteryInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

//   自开彩配置详情接口
export function getSelfLotteryInfo (query) {
  const params = {
    cmd: 'SelfLotteryInfo'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩配置更新接口
export function postSelfLotteryInfoUp (query) {
  const params = {
    cmd: 'SelfLotteryInfoUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSelfLotteryDetailList (query) {
  const params = {
    cmd: 'SelfLotteryDetailList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSelfLotteryCurrIssueList (query) {
  const params = {
    cmd: 'SelfLotteryCurrIssueList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSelfLotteryDetailInfo (query) {
  const params = {
    cmd: 'SelfLotteryDetailInfo'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postSelfLotteryDetailAdd (query) {
  const params = {
    cmd: 'SelfLotteryDetailAdd'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postSelfLotteryDetailUp (query) {
  const params = {
    cmd: 'SelfLotteryDetailUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSelfLotteryOpenCodeList (query) {
  const params = {
    cmd: 'SelfLotteryOpenCodeList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSelfLotteryOrderInfoList (query) {
  const params = {
    cmd: 'SelfLotteryOrderInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getReportSysSelfLottery (query) {
  const params = {
    cmd: 'ReportSysSelfLottery'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全局自开彩风控查询
export function getRiskControlSysSelfLottery (lotteryId) {
  const params = {
    cmd: 'RiskControlSysSelfLottery',
    lotteryId
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 全局自开彩风控设置
export function postUpSysSelfLotteryRiskControl (query) {
  const params = {
    cmd: 'UpSysSelfLotteryRiskControl'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postUpSysSelfLotteryPayoutRate (query) {
  const params = {
    cmd: 'UpSysSelfLotteryPayoutRate'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全局自开彩接口
export function getSelfSysLotteryReport (query) {
  const params = {
    cmd: 'SelfSysLotteryReport'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postSelfSysLotteryClean (lotteryId) {
  return fetch({
    method: 'post',
    data: { cmd: 'SelfSysLotteryClean', lotteryId }
  })
}

export function getSelfSysLotterySettingInfo (lotteryId) {
  const params = {
    cmd: 'SelfSysLotterySettingInfo',
    lotteryId
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

export function postSelfSysLotterySettingSave (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SelfSysLotterySettingSave', ...query }
  })
}

// 根据条件获得自开彩开奖轨迹信息,默认获得当天信息列表
export function postIssueOrbitList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueOrbitList', ...query }
  })
}

// 自开彩全局设置
export function postSelfLotteryRandomGet (query) {
  const params = {
    cmd: 'SelfLotteryRandomGet'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 更新自开彩全局设置
export function postSelfLotteryRandomUp (query) {
  const params = {
    cmd: 'SelfLotteryRandomUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩redis缓存中的预统计数据进行清零设置，并记录清零时间
export function postSelfLotteryClean (query) {
  const params = {
    cmd: 'SelfLotteryClean'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩预设开奖接口查询列表
export function getSelfLotteryDetailSearch (query) {
  return fetch({
    method: 'post',
    data: { ...query, cmd: 'SelfLotteryDetailSearch' }
  })
}

// 自开彩图标配置列表
export function getSelfCustomConfList (query) {
  const params = {
    cmd: 'SelfCustomConfList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩图标彩种名称修改
export function postSelfCustomNameEdit (query) {
  const params = {
    cmd: 'SelfCustomNameEdit'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩图标图标修改
export function postSelfCustomImageEdit (query) {
  const params = {
    cmd: 'SelfCustomImageEdit'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 自开彩图标状态修改
export function postSelfCustomSwitch (query) {
  const params = {
    cmd: 'SelfCustomSwitch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量预设彩期开奖结果
export function postSelfLotteryDetailBatchAdd (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SelfLotteryDetailBatchAdd', ...query }
  })
}

// 自开采名称启用禁用开关
export function postSelfCustomNameSwitch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'SelfCustomNameSwitch', ...query }
  })
}
