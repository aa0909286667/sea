/**
 * Created by xiaow on 2017/7/13.
 */
import fetch from 'utils/fetch'

/* ---角色列表-- */
export function getAuthRoleList () {
  const params = {}
  params.cmd = 'AuthRoleList'
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* 用户角色新增 */
export function postAuthRoleCreate (query) {
  const params = {}
  params.cmd = 'AuthRoleCreate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---权限列表--- */
export function getAuthPermissionList (query) {
  const params = {}
  params.cmd = 'AuthPermissionList'
  params.roleId = query.id
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---权限绑定--- */
export function postAuthPermissionBind (query) {
  const params = {}
  params.cmd = 'AuthPermissionBind'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----- 角色管理 ----- */

/* 用户角色查询 */
export function getMasterRoleList () {
  const params = {}
  params.cmd = 'MasterRoleList'
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* 为用户绑定相应的角色 */
export function postAuthRoleBind (query) {
  const params = {}
  params.cmd = 'AuthRoleBind'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* 系统通用配置列表 */
export function getSysConfigList () {
  const params = {}
  params.cmd = 'SysConfigList'
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* 通用配置更新 */
export function postSysConfigUpdate (query) {
  const params = {}
  params.cmd = 'SysConfigUpdate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----采集日志--- */
export function getLoggerGatherList (query) {
  const params = {}
  params.cmd = 'LoggerGatherList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----操作员管理--- */

// 列表
export function getMasterList (query) {
  const params = {}
  params.cmd = 'MasterList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 状态改变
export function postMasterUpStatus (query) {
  const params = {}
  params.cmd = 'MasterUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 创建角色
export function postMasterCreate (query) {
  const params = {}
  params.cmd = 'MasterCreate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 修改角色
export function postAuthRoleUpdate (query) {
  const params = {}
  params.cmd = 'AuthRoleUpdate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 重置密码
export function postUserRestPassword (query) {
  const params = {}
  params.cmd = 'UserRestPassword'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 修改密码
export function UpdatePassword (query) {
  const params = {}
  params.cmd = 'UpdatePassword'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 刷新redis缓存数据
export function postRefreshRedisCache (query) {
  const params = {}
  params.cmd = 'RefreshRedisCache'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 页面跳转类型列表
export function getPageTypeList (query) {
  const params = {}
  params.cmd = 'PageTypeList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postPageTypeUpdate (query) {
  const params = {}
  params.cmd = 'PageTypeUpdate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 结算配置列表
export function getSettleSettingList (query) {
  const params = {}
  params.cmd = 'SettleSettingList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 结算配置新增
export function postSettleSettingAdd (query) {
  const params = {}
  params.cmd = 'SettleSettingAdd'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 结算配置详情
export function getSettleSettingDetail (query) {
  const params = {}
  params.cmd = 'SettleSettingDetail'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 结算配置保存
export function postSettleSettingSave (query) {
  const params = {}
  params.cmd = 'SettleSettingSave'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 结算维护开关 开启 关闭 操作
export function getSettleHungmant (query) {
  const params = {}
  params.cmd = 'SettleHungmant'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获得结算维护状态0关闭维护 1开启维护
export function getSettleHungmantGet (query) {
  const params = {}
  params.cmd = 'SettleHungmantGet'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获取白名单列表
export function getWhiteList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'WhiteList', ...query }
  })
}

export function postWhiteListEdit (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'WhiteListEdit', ...query }
  })
}

export function postWhiteListSwitch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'WhiteListSwitch', ...query }
  })
}

// 多语言管理
export function getLangList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'LangList', ...query }
  })
}

// 多语言设置
export function postLangUp (items) {
  return fetch({
    method: 'post',
    data: { cmd: 'LangUp', items }
  })
}

/* 币别预警阈值 */

// 币别预警阈值列表
export const GetCurrencyMonitorList = (data) => {
  return fetch({
    method: 'post',
    data: { cmd: 'CurrencyMonitorList', ...data }
  })
}

// 币别预警阈值新增/修改
export const UpdateCurrencyMonitorAmount = (data) => {
  return fetch({
    method: 'post',
    data: { cmd: 'CurrencyMonitorUpdate', ...data }
  })
}

// 币别预警阈值列表启用/禁用
export const UpdateCurrencyMonitorStatus = (data) => {
  return fetch({
    method: 'post',
    data: { cmd: 'CurrencyMonitorUpStatus', ...data }
  })
}
