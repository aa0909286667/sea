import fetch from 'utils/fetch'

// 获取厅列表接口
export function getSpRegList (query) {
  const params = {}
  params.cmd = 'SpRegList'
  return fetch({
    method: 'post',
    data: { ...query, ...params }
  })
}

// 重置默认盘口接口
export function postSpRegClearTpl (query) {
  const params = {}
  params.cmd = 'SpRegClearTpl'
  return fetch({
    method: 'post',
    data: { ...query, ...params }
  })
}

// 获取开厅选择厅列表，已配过数据源
export function getSpRegSpList (query) {
  const params = {}
  params.cmd = 'SpRegSpList'
  return fetch({
    method: 'post',
    data: { ...query, ...params }
  })
}

// 开厅 前提以绑数据源
export function postSpRegInsert (query) {
  const params = {}
  params.cmd = 'SpRegInsert'
  return fetch({
    method: 'post',
    data: { ...query, ...params }
  })
}
