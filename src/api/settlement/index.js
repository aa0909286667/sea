/**
 * Created by Xiaowu on 2019/6/3.
 */
/**
 * Created by xiaow on 2017/7/13.
 */
import fetch from 'utils/fetch'

/* ---冻结日志报表-- */
export function getSettleUserblockList (query) {
  const params = {}
  params.cmd = 'SettleUserblockList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 冻结用户id列表详情

export function postSettleUserblockUserIds (query) {
  const params = {}
  params.cmd = 'SettleUserblockUserIds'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 撤单任务数据列表
export function getTaskRefundList (query) {
  const params = {}
  params.cmd = 'TaskRefundList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 撤单任务失败列表接口
export function postTaskRefundFailedList (query) {
  const params = {}
  params.cmd = 'TaskRefundFailedList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 撤单任务失败数据强制处理接口
export function postTaskRefundFailedForceProcess (query) {
  const params = {}
  params.cmd = 'TaskRefundFailedForceProcess'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 撤单任务失败数据批量处理接口
export function postTaskRefundFailedProcess (query) {
  const params = {}
  params.cmd = 'TaskRefundFailedProcess'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 任务撤单处理接口
export function postOrderCancelByTask (query) {
  const params = {}
  params.cmd = 'OrderCancelByTask'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
