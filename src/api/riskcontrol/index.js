import fetch from 'utils/fetch'

export function getSpRiskControlList () {
  const params = {
    cmd: 'RiskControlList'
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

export function postRiskControlAdd (query) {
  const params = {
    cmd: 'RiskControlAdd'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postRiskControlEdit (query) {
  const params = {
    cmd: 'RiskControlEdit'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postRiskControlDel (query) {
  const params = {
    cmd: 'RiskControlDel'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获取提前开奖列表
export function getRiskBeforeOpenList (query) {
  const params = {
    cmd: 'RiskBeforeOpenList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获取提前开奖风控解除
export function postRiskBeforeOpenRecovery (query) {
  const params = {
    cmd: 'RiskBeforeOpenRecovery'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种维护设置列表
export function getLotteryMaintainList (query) {
  const params = {
    cmd: 'LotteryMaintainList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种维护设置添加
export function postLotteryMaintainAdd (query) {
  const params = {
    cmd: 'LotteryMaintainAdd'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种维护设置解除彩种维护
export function postLotteryMaintainClose (query) {
  const params = {
    cmd: 'LotteryMaintainClose'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
