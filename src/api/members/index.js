import fetch from 'utils/fetch'
// import { commonParams } from 'api/config'

// 会员列表
export function getMemberInfoList (query) {
  const params = {}
  params.cmd = 'MemberInfoList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员详情
export function getMemberDetail (uid) {
  const params = {}
  params.cmd = 'MemberDetail'
  params.uid = uid
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 会员状态变更
export function MemberInfoUpStatus (uid, status) {
  const params = {}
  params.cmd = 'MemberInfoUpStatus'
  params.uid = uid
  params.status = status
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 会员设置
export function MemberSetting () {
  console.log('MemberSetting')
}

// 用户限额列表
export function getMemberLotteryQuotaList (query) {
  const params = {
    cmd: 'MemberLotteryQuotaList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员限额设置列表
export function getMemberQuotaList (query) {
  const params = {
    cmd: 'MemberQuotaList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员限额

export function getMemberQuotaLimitList (query) {
  const params = {
    cmd: 'MemberQuotaLimitList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员限额设置
export function postMemberQuotaLimitCreate (query) {
  const params = {
    cmd: 'MemberQuotaLimitCreate'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 限额状态变更
export function postMemberQuotaLimitStatus (query) {
  const params = {
    cmd: 'MemberQuotaLimitStatus'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员盘口绑定
export function postMemberTplBind (uid, spTplId) {
  const params = {
    cmd: 'MemberTplBind',
    uid,
    spTplId
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 会员状态变更
export function postMemberStatus (uid, spCode, status) {
  const params = {
    cmd: 'MemberStatus',
    uid,
    spCode,
    status
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 开户配置
export function getMemberRegConfDetail (confId) {
  const params = {
    cmd: 'MemberRegConfDetail',
    confId
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 代理列表
export function getAgentInfoList (query) {
  const params = {
    cmd: 'AgentInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 会员登录日志
export function getMemberLoggerMembeList (query) {
  const params = {}
  params.cmd = 'MemberLoggerMembeList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 开户配置查询列表
export function getRebateConfigList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigList', ...query }
  })
}

// 开户配置预配列表查询
export function getRebateConfigPresetList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigPresetList', ...query }
  })
}

// 会员的开户预配列表查询
export function getRebateConfigPresetListByMember (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigPresetListByMember', ...query }
  })
}

// 查询代理的邀请码开户配置
export function getRebateConfigPresetListByInvite (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigPresetListByInvite', ...query }
  })
}

// 预设的开户配置绑定接口
export function postRebateConfigPresetBind (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigPresetBind', ...query }
  })
}

// 邀请码和开户配置绑定接口
export function postRebateConfigAgentCodeBind (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'RebateConfigAgentCodeBind', ...query }
  })
}
