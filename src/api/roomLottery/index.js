import fetch from 'utils/fetch'

// 交互下注彩种列表
export function getRoomLotteryList () {
  const params = {
    cmd: 'roomLotteryList'
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 交互下注彩种开关
export function postRoomLotterySwitch (query) {
  const params = {
    cmd: 'roomLotterySwitch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注彩种绑定
export function postRoomLotteryBindSN (query) {
  const params = {
    cmd: 'roomLotteryBindSN'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注彩种绑定列表
export function getRoomLotteryBindSNList (query) {
  const params = {
    cmd: 'roomLotteryBindSNList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注彩种绑定开关
export function postRoomLotteryBindSNSwitch (query) {
  const params = {
    cmd: 'roomLotteryBindSNSwitch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间等级列表
export function getRoomLevelList (query) {
  const params = {
    cmd: 'roomLevelList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间等级开关
export function postRoomLevelSwitch (query) {
  const params = {
    cmd: 'roomLevelSwitch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注基础玩法赔率列表
export function getRoomPlayList (query) {
  const params = {
    cmd: 'roomPlayList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注基础玩法赔率保存
export function postRoomPlaySave (query) {
  const params = {
    cmd: 'roomPlaySave'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注特殊玩法赔率列表
export function getRoomPlaySpecialList (query) {
  const params = {
    cmd: 'roomPlaySpecialList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 交互下注特殊玩法赔率列表
export function postRoomPlaySpecialSave (query) {
  const params = {
    cmd: 'roomPlaySpecialSave'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间限额设置列表接口
export function getRoomQuotaList (query) {
  const params = {
    cmd: 'roomQuotaList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间限额设置保存
export function postRoomQuotaSave (query) {
  const params = {
    cmd: 'roomQuotaSave'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间条件设置列表
export function getRoomConditionList (query) {
  const params = {
    cmd: 'roomConditionList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 房间条件设置保存
export function postRoomConditionSave (query) {
  const params = {
    cmd: 'roomConditionSave'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
