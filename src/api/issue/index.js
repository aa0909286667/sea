/**
 * 彩期相关接口
 * Created by xiaow on 2017/7/28.
 */
import fetch from 'utils/fetch'

// 彩期规则信息列表
export function getIssueRuleInfo (query) {
  const params = {
    cmd: 'GetIssueRuleInfo'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩期规则设置保存
export function postIssueRuleSaveBatch (query) {
  const params = {
    cmd: 'IssueRuleSaveBatch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩期列表
export function getIssueInfoList (query) {
  const params = {
    cmd: 'IssueInfoList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 特殊彩期列表
// export function getIssueInfoSpecialList(query) {
//   const params = {
//     cmd: 'IssueInfoSpecialList'
//   }
//   return fetch({
//     method: 'post',
//     data: Object.assign({}, params, query)
//   })
// }

// 手动开奖
export function postManualDraw (query) {
  const params = {
    cmd: 'ManualDraw',
    lotteryId: query.lotteryId,
    issue: query.issue,
    openCode: query.openCode
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 开盘
export function postIssueEnabled (query) {
  const params = {
    cmd: 'IssueEnabled',
    lotteryId: query.lotteryId,
    issue: query.issue
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 封盘
export function postIssueDisabled (query) {
  const params = {
    cmd: 'IssueDisabled',
    lotteryId: query.lotteryId,
    issue: query.issue
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 重新结算
export function postReSettlement (query) {
  const params = {
    cmd: 'ReSettlement',
    lotteryId: query.lotteryId,
    issue: query.issue,
    openCode: query.openCode
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 系统撤单
export function postOrderCancelByIssue (query) {
  const params = {
    cmd: 'OrderCancelByIssue',
    lotteryId: query.lotteryId,
    issue: query.issue
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 彩期详情
export function getIssueInfoDetail (query) {
  const params = {
    cmd: 'IssueInfoDetail'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量重置彩期列表
// export function postIssueRuleReset(query) {
//   const params = {
//     cmd: 'IssueRuleReset'
//   }
//   params.lotteryId = query.lotteryId
//   return fetch({
//     method: 'post',
//     data: Object.assign({},  params)
//   })
// }

// 统一微调
// export function postIssueRuleFineTune(query) {
//   const params = {
//     cmd: 'IssueRuleFineTune'
//   }
//   return fetch({
//     method: 'post',
//     data: Object.assign({},  params, query)
//   })
// }

// 特殊彩期添加或编辑
export function postIssueRuleSpecialAdd (query) {
  const params = {
    cmd: 'IssueRuleSpecialAdd'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 特殊彩种的彩期删除
export function postIssueRuleSpecialDel (query) {
  const params = {
    cmd: 'IssueRuleSpecialDel'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 获取彩期规则详情信息
export function getIssueRuleInfoList (query) {
  const params = {
    cmd: 'GetIssueRuleInfo'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩期类别内容重置
export function saveIssueBatchUpdate (query) {
  const params = {
    cmd: 'IssueBatchUpdate'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 厅开奖采集设置
export function getOpenCodeCollectList (query) {
  const params = {
    cmd: 'OpenCodeCollectList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postOpenCodeCollectSave (query) {
  const params = {
    cmd: 'OpenCodeCollectSave'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postOpenCodeCollectUp (query) {
  const params = {
    cmd: 'OpenCodeCollectUp'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function postOpenCodeCollectSwitch (query) {
  const params = {
    cmd: 'OpenCodeCollectSwitch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 新彩期规则相关
export function getIssueProduceRuleList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueProduceRuleList', lotteryId: query.lotteryId }
  })
}

export function postIssueProduceRuleDisabled (lotteryId) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueProduceRuleDisabled', lotteryId }
  })
}

export function postIssueProduceRuleEnabled (lotteryId) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueProduceRuleEnabled', lotteryId }
  })
}

export function postIssueProduceExcludeRuleDisabled (id) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueProduceExcludeRuleDisabled', id }
  })
}

export function postIssueProduceExcludeRuleEnabled (id) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueProduceExcludeRuleEnabled', id }
  })
}

export function postIssueResetByProduceRule (lotteryId) {
  return fetch({
    method: 'post',
    data: { cmd: 'IssueResetByProduceRule', lotteryId }
  })
}
