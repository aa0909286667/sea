import fetch from 'utils/fetch'

// 业务日志查询接口
export function getBusiLoggerList (query) {
  const params = {}
  params.cmd = 'BusiLoggerList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 查询所属模块条件
export function getBusiLoggerModule (query) {
  const params = {}
  params.cmd = 'BusiLoggerModule'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 日志行为查询列表
export function getBusiLoggerActionList (query) {
  const params = {}
  params.cmd = 'BusiLoggerActionList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 业务操作日志行为添加接口
export function postBusiLoggerActionAdd (query) {
  const params = {}
  params.cmd = 'BusiLoggerActionAdd'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 业务操作日志行为状态更新接口/启用和禁用
export function postBusiLoggerActionUp (query) {
  const params = {}
  params.cmd = 'BusiLoggerActionUp'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 	业务操作日志行为删除接口
export function postBusiLoggerActionDel (query) {
  const params = {}
  params.cmd = 'BusiLoggerActionDel'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 业务操作日志行为属性列表接口
export function getBusiLoggerPropertyList (query) {
  const params = {}
  params.cmd = 'BusiLoggerPropertyList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 业务操作日志行为属性添加和更新接口
export function postBusiLoggerPropertyUp (query) {
  const params = {}
  params.cmd = 'BusiLoggerPropertyUp'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
