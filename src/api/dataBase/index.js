/**
 * Created by xiaow on 2017/7/13.
 */
import fetch from 'utils/fetch'

export function getLotteryTypeList (query) {
  const params = {}
  params.cmd = 'LotteryTypeList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---彩种信息列表--- */
export function getLotteryInfoList (query) {
  const params = {}
  params.cmd = 'LotteryInfoList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---彩种分组显示顺序--- */
export function getLotteryTypeUpDisplaySort (query) {
  const params = {}
  params.cmd = 'LotteryTypeUpDisplaySort'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---彩种设置显示顺序--- */
export function getLotteryUpDisplaySort (query) {
  const params = {}
  params.cmd = 'LotteryUpDisplaySort'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---彩种注册(待定)--- */
export function LotteryUpRegister (id, status, authCode) {
  const params = {}
  params.cmd = 'LotteryUpRegister'
  params.id = id
  params.status = status
  params.authCode = authCode
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* ---545彩种类型的关闭或开通--- */
export function postLotteryTypeUpStatus (query) {
  const params = {}
  params.cmd = 'LotteryTypeUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----5412接口玩法关闭或开通--- */
export function postLotteryPlayUpStatus (query) {
  const params = {}
  params.cmd = 'LotteryPlayUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----接口5410玩法分类的关闭或开通--- */
export function postLotteryPlayTypeUpStatus (query) {
  const params = {}
  params.cmd = 'LotteryPlayTypeUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----5415 彩种关闭或开通接口--- */
export function postLotteryUpStatus (query) {
  const params = {}
  params.cmd = 'LotteryUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---玩法设置列表--- */
export function getLotteryPlayList (query) {
  const params = {
    cmd: 'LotteryPlayList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---玩法设置列表排序--- */
export function getLotteryPlayUpDisplaySort (query) {
  const params = {
    cmd: 'LotteryPlayUpDisplaySort'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----修改玩法的注额---- */
export function postLotteryPlayUpBet (query) {
  const params = {}
  params.cmd = 'LotteryPlayUpBet'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 玩法分组列表
export function getLotteryPlayTabList (query) {
  const params = {}
  params.cmd = 'LotteryPlayTabList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----玩法类型的关闭或开通---- */
export function postLotteryPlayTabUpStatus (query) {
  const params = {}
  params.cmd = 'LotteryPlayTabUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----548玩法类型的排序修改---- */
export function LotteryPlayTabUpSort (query) {
  const params = {}
  params.cmd = 'LotteryPlayTabUpSort'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----玩法分类--- */
export function getPlayTypeList (query) {
  const params = {}
  params.cmd = 'LotteryPlayTypeList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----玩法分类排序--- */
export function getLotteryPlayTypeUpDisplaySort (query) {
  const params = {}
  params.cmd = 'LotteryPlayTypeUpDisplaySort'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----彩种分组列表--- */
export function getLotteryGroupList () {
  const params = {}
  params.cmd = 'LotteryGroupList'
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* ----根据分组id显示彩种列表---- */
export function getLotteryGroupRelList (id) {
  const params = {}
  params.cmd = 'LotteryGroupRelList'
  params.groupId = id
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

/* ----批量修改分组信息---- */
export function postLotteryGroupUpdate (query) {
  const params = {}
  params.cmd = 'LotteryGroupUpdate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----添加组彩种---- */
export function postLotteryGroupRelAdd (query) {
  const params = {}
  params.cmd = 'LotteryGroupRelAdd'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ---分组绑定/解绑接口--- */
export function postLotteryGroupRelDel (query) {
  const params = {}
  params.cmd = 'LotteryGroupRelDel'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----分组删除--- */
export function postLotteryGroupDel (query) {
  const params = {}
  params.cmd = 'LotteryGroupDel'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

/* ----玩法规则---- */
export function getGameRuleList (query) {
  return fetch({
    method: 'post',
    data: { ...query, cmd: 'GameRuleList' }
  })
}

export function getGameRuleDetail (query) {
  return fetch({
    method: 'post',
    data: { ...query, cmd: 'GameRuleDetail' }
  })
}

export function postGameRuleSave (query) {
  return fetch({
    method: 'post',
    data: { ...query, cmd: 'GameRuleSave' }
  })
}

// 获取长龙数据
export function getLongdragonConfList (query) {
  return fetch({
    method: 'post',
    data: { ...query, cmd: 'LongdragonConfList' }
  })
}

// 长龙开启|关闭
export function postLongdragonConfSave (query) {
  const params = {}
  params.cmd = 'LongdragonConfSave'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
