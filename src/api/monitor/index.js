/**
 *注单监控和彩期监控相关接口
 */
import fetch from 'utils/fetch'

// 查询异常注单或彩期接口
export function getExceptionList (query) {
  const params = {}
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量处理异常注单或彩期接口
export function handleBatchList (params) {
  return fetch({
    method: 'post',
    data: { cmd: 'MonitorProcess', ...params }
  })
}

// 异常处理
export function getOrderErrorList (query) {
  const params = {}
  params.cmd = 'OrderErrorList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 查看 异常处理
export function getOrderErrorDetail (query) {
  const params = {}
  params.cmd = 'OrderErrorDetail'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 人工处理
export function getOrderErrorHandleProcessor (query) {
  const params = {}
  params.cmd = 'OrderErrorHandleProcessor'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 处理时间段的所有异常
export function postOrderErrorHandleAllProcessor (query) {
  const params = {}
  params.cmd = 'OrderErrorHandleAllProcessor'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 处理日志
export function getOrderErrorHandleSearch (query) {
  const params = {}
  params.cmd = 'OrderErrorHandleSearch'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 数据库主从延迟验证数据
export function getMonitorDB (query) {
  const params = {}
  params.cmd = 'MonitorDB'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 可疑注单列表
export function getOrderDoubtList (query) {
  const params = {}
  params.cmd = 'OrderDoubtList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
