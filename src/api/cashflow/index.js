/**
 * 金流列表
 * Created by xiaow on 2017/7/19.
 */
import fetch from 'utils/fetch'
import { parseTime } from 'utils/index'

export function getMemberCashflowList (query) {
  const params = {}
  params.cmd = 'MemberCashflowList'
  if (query.beginTime && query.endTime) {
    params.beginTime = parseTime(query.beginTime)
    params.endTime = parseTime(query.endTime)
  }
  return fetch({
    method: 'post',
    data: { ...query, ...params }
  })
  /*
   * 分页
   * elements节点
   * spCode	所属平台商
   loginId	用户账号
   cashType	交易类别
   lotteryName	彩种名称
   orderCode	注单号
   historyMoney	原金额
   cashMoney	操作金额
   balanceMoney	剩余金额
   singleMoney	单注金额
   createTime	操作时间
   description	备注
   * */
}
