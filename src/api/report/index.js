// 报表
import fetch from 'utils/fetch'

// 单骑盈亏报表
export function getReportIssueProfitLossList (query) {
  const params = {
    // cmd: 'ReportIssueProfitLossList'
    cmd: 'ReportIssueProfitLossSearch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 用户投注额报表
export function getReportUserBetAmountSearch (query) {
  const params = {
    cmd: 'ReportUserBetAmountSearch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种投注额报表
export function getReportLotteryBetAmountSearch (query) {
  const params = {
    cmd: 'ReportLotteryBetAmountSearch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种输赢报表数据
export function getReportLotteryWinLoseSearch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportLotteryWinLoseSearch', ...query }
  })
}

// 总报表
export function getReportGeneralSearch (query) {
  const params = {
    cmd: 'ReportGeneralSearch'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种报表
export function getReportLotteryList (query) {
  const params = {
    cmd: 'ReportLotteryList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 报表下载列表
export function getReportDownloadList (query) {
  const params = {
    cmd: 'ReportExDownloadList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 杀率排行报表
export function getReportSelfLotteryWinLoseList (query) {
  const params = {
    cmd: 'ReportSelfLotteryWinLoseList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 杀率汇总报表
export function getReportSelfLotteryRatio (query) {
  const params = {
    cmd: 'ReportSelfLotteryRatio'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 预统计操作
export function postReStatistics (query) {
  const params = {
    cmd: 'ReStatistics'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种输赢报表 导出
export function getReportExportLotteryWinLoseList (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportExportLotteryWinLoseList', ...query }
  })
}

// 直播有效投注数据报表
export function getReportLiveSummaryList (query) {
  const params = {
    cmd: 'ReportLiveSummaryList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩种分析报表
export function getReportAnalysisLotterySearch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportAnalysisLotterySearch', ...query }
  })
}

// 用户分析报表
export function getReportAnalysisUserSearch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportAnalysisUserSearch', ...query }
  })
}

// 厅主分析报表
export function getReportAnalysisSnSearch (query) {
  return fetch({
    method: 'post',
    data: { cmd: 'ReportAnalysisSnSearch', ...query }
  })
}
