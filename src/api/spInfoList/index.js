/**
 * Created by caoz on 2017/7/19.
 */
import fetch from 'utils/fetch'

export function getSpLotteryTypeList (query) {
  const params = {}
  params.cmd = 'SpLotteryTypeList';
  ({
    spCode: params.spCode,
    status: params.status
  } = query || {})
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 5.6.29 平台商彩种设置 中奖限额设置
export function postSpLotteryMaxReward (query) {
  const params = {
    cmd: 'SpLotteryMaxReward'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 5.4.17平台商列表
export function getSpInfoList (query) {
  const params = {}
  params.cmd = 'SpInfoList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台商返水率控制设置
export function psotSpInfoUpRebateShow (query) {
  const params = {}
  params.cmd = 'SpInfoUpRebateShow'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台商分红级别设置
export function psotSpInfoUpAbonusLevel (query) {
  const params = {}
  params.cmd = 'SpInfoUpAbonusLevel'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 5.6.28.2 平台商彩种配置列表接口
export function getSpLotteryBetPauseList (query) {
  const params = {
    cmd: 'SpLotteryBetPauseList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

export function getSpLotteryMaxRewardList (query) {
  const params = {
    cmd: 'SpLotteryMaxRewardList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 5.6.30 平台商彩种暂停下注
export function postSpLotteryBetPause (query) {
  const params = {
    cmd: 'SpLotteryBetPause'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 5.4.20 平台商详情
export function getSpInfoDetail (spCode) {
  const params = {}
  params.cmd = 'SpInfoDetail'
  params.spCode = spCode
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 5.4.23.2 平台商彩种类型设置
export function postSpLotteryTypeUp (spCode, query) {
  const params = {
    cmd: 'SpLotteryTypeUp',
    spLotteryTypeList: JSON.stringify(query),
    spCode
  }
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 接口5418平台商用户的启用禁用
export function postSpInfoUpStatus (query) {
  const params = {}
  params.cmd = 'SpInfoUpStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 维护计划
export function getSpMaintainList (query) {
  const params = {}
  params.cmd = 'SpMaintainList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 维护计划编辑
export function postSpMaintainSave (query) {
  const params = {}
  params.cmd = 'SpMaintainSave'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 维护计划状态变更
export function postSpMaintainSatus (query) {
  const params = {}
  params.cmd = 'SpMaintainSatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台商彩种当前彩期列表
export function getSpLotteryCurrIssueList (query) {
  const params = {}
  params.cmd = 'SpLotteryCurrIssueList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台商彩种当前彩期设置开盘
export function postSpLotteryCurrIssueEnabled (query) {
  const params = {}
  params.cmd = 'SpLotteryCurrIssueEnabled'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台商彩种当前彩期设置封盘
export function postSpLotteryCurrIssueDisabled (query) {
  const params = {}
  params.cmd = 'SpLotteryCurrIssueDisabled'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 平台列表
export function getPlatformList () {
  const params = {}
  params.cmd = 'PlatformList'
  return fetch({
    method: 'post',
    data: params
  })
}

export function postSpInfoUpMoneyUnit (query) {
  const params = {}
  params.cmd = 'SpInfoUpMoneyUnit'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 虚拟数据开关
export function postSpInfoUpVrtualDataSwitch (query) {
  const params = {}
  params.cmd = 'SpInfoUpVrtualDataSwitch'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 厅主关闭和开通用户的撤单功能
export function postSpInfoUpCancelStatus (query) {
  const params = {}
  params.cmd = 'SpInfoUpCancelStatus'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 分库设置列表接口
export function getShardingDBList (query) {
  const params = {}
  params.cmd = 'ShardingDBList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 分库设置添加接口
export function getShardingDBInit (query) {
  const params = {}
  params.cmd = 'ShardingDBInit'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 分库设置保存接口
export function getShardingDBSave (query) {
  const params = {}
  params.cmd = 'ShardingDBSave'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 分库数据源列表接口
export function getShardingDatasourceList (query) {
  const params = {}
  params.cmd = 'ShardingDatasourceList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
// 分库数据源列表接口
export function postShardingDatasourceMaintain (query) {
  const params = {}
  params.cmd = 'ShardingDatasourceMaintain'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 厅主币别修改
export function spInfoUpCurrencyCode (query) {
  const params = {}
  params.cmd = 'SpInfoUpCurrencyCode'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
