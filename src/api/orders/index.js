/**
 * 注单列表
 * Created by xiaow on 2017/7/19.
 */
import fetch from 'utils/fetch'

export function getOrderInfoList (query) {
  const params = {}
  params.cmd = 'OrderInfoList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
  /*
   * 分页
   * elements节点
   * orderCode	订单号
   clientType	来源终端类型  1:WEB 2:WAP(H5) 3.Android 4:iOS 5:IPAD
   channel	所属渠道
   spCode	所属平台商
   loginId	用户账号
   lotteryName	彩种名称
   issue	期号
   playName	所属玩法
   status	订单状态  000(0)未结算 001(1)用户撤单  010(2)追号撤单  011(3)系统撤单  100(4)已结算_平局  101(5)已结算_输 110(6)已结算_赢
   ip	IP地址
   createTime	注单时间
   buyNumber	投注数量
   singleMoney	单注金额  单位分
   orderMoney	总金额  单位分
   prize	投注赔率 投注赔率单位分
   rebateRate	投注返水率  返水率[返点率],单位百分比并计算到两位小数
   rebateMoney	返水金额
   openCode	开奖号码
   buyCode	下注号码
   winMoney	中奖金额
   winCount	中奖单数
   profitMoney	盈亏金额
   trace	是否追号注单  0-否 1-是
   winningToStop	中奖后是否停止追号  0-否 1-是
   traceCode	追号注单上级注单编码
   settleNum	结算次数
   settleTime	结算时间
   * */
}

// 注单详情
export function getOrderDetail (query) {
  const params = {
    cmd: 'OrderDetail'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 撤单操作
export function postLotteryUserBetCancel (query) {
  const params = {
    cmd: 'LotteryUserBetCancel'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 注单分组数据
// export function getOrderReportUserGroup(query) {
//   const params = {
//     cmd: 'OrderReportUserGroup'
//   }
//   return fetch({
//     method: 'post',
//     data: Object.assign({}, params, query)
//   })
// }

// 官方即时注单统计列表
export function getOrderCurrReportList (query) {
  const params = {
    cmd: 'OrderCurrReportList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 信用即时注单统计列表
export function getOrderCreditCurrReportList (query) {
  const params = {
    cmd: 'OrderCreditCurrReportList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全厅信用即时注单统计列表
export function getOrderAllSnCreditCurrReportList (query) {
  const params = {
    cmd: 'OrderAllSnCreditCurrReportList'
  }
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 按彩期撤单
export function OrderCancelByIssue (lotteryId, issue) {
  const params = {}
  params.cmd = 'OrderCancelByIssue'
  // 必填参数
  params.lotteryId = lotteryId
  params.issue = issue
  // 可填参数  从store或cookie取
  // params.spCode = '';
  return fetch({
    method: 'post',
    data: { ...params }
  })
}

// 局数据列表查询
export function getOrderGameReportList (query) {
  const params = {}
  params.cmd = 'OrderGameReportList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 派彩
export function postOrderDeal (query) {
  const params = {}
  params.cmd = 'OrderDeal'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 人工结算
export function postOrderSettlement (query) {
  const params = {}
  params.cmd = 'OrderSettlement'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 人工发放代理返点
export function postOrderDealAgentRebate (query) {
  const params = {}
  params.cmd = 'OrderDealAgentRebate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 异常统计
export function getOrderReportList (query) {
  const params = {}
  params.cmd = 'OrderReportList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 注单统计
export function getReportOrderList (query) {
  const params = {}
  params.cmd = 'ReportOrderList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 彩期已开奖未结算的注单列表
export function getReportUnSettleOrderList (query) {
  const params = {}
  params.cmd = 'ReportUnSettleOrderList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 已结算未派彩的注单列表
export function getReportUnPayoutOrderList (query) {
  const params = {}
  params.cmd = 'ReportUnPayoutOrderList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 有代理返点未派彩的注单列表
export function getReportAgentRebateOrderList (query) {
  const params = {}
  params.cmd = 'ReportAgentRebateOrderList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量处理或单个处理未派彩代理返点的注单接口
export function postBatchDealAgentRebate (query) {
  const params = {}
  params.cmd = 'BatchDealAgentRebate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量处理或单个未派彩已结算的注单
export function postBatchDeal (query) {
  const params = {}
  params.cmd = 'BatchDeal'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 批量处理或单个处理彩期已开奖未结算的注单
export function postBatchSettlementOrder (query) {
  const params = {}
  params.cmd = 'BatchSettlementOrder'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 根据条件查询补差记录列表明细信息
export function postOrderReverseInfoList (query) {
  const params = {}
  params.cmd = 'OrderReverseInfoList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 根据注单号调用平台扣款接口重新进行补录操作
export function postUpOrderReverseInfo (query) {
  const params = {}
  params.cmd = 'UpOrderReverseInfo'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 追号方案
export function getOrderTraceDetail (query) {
  const params = {}
  params.cmd = 'OrderTraceDetail'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全局发放代理返点
export function postGlobalDealAgentRebate (query) {
  const params = {}
  params.cmd = 'GlobalDealAgentRebate'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全局结算已开奖未结算的注单
export function postGlobalOrderSettlement (query) {
  const params = {}
  params.cmd = 'GlobalOrderSettlement'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 全局派彩奖金及投注返点
export function postGlobalPayOutReward (query) {
  const params = {}
  params.cmd = 'GlobalPayOutReward'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 注单报表导出
export function postExportOrderList (query) {
  const params = {}
  params.cmd = 'ExportOrderList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}

// 追号查询
export function getOrderTraceList (query) {
  const params = {}
  params.cmd = 'OrderTraceList'
  return fetch({
    method: 'post',
    data: { ...params, ...query }
  })
}
