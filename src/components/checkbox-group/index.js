import XwCheckboxGroup from '../checkbox/src/checkbox-group.vue'

XwCheckboxGroup.install = function (Vue) {
  Vue.component(XwCheckboxGroup.name, XwCheckboxGroup)
}

export default XwCheckboxGroup
