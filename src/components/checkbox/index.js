import XwCheckbox from './src/checkbox'

XwCheckbox.install = function (Vue) {
  Vue.component(XwCheckbox.name, XwCheckbox)
}

export default XwCheckbox
