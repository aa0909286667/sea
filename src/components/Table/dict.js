/**
 * Created by Xiaowu on 2018/12/28.
 */
import Calendar from 'utils/Calendar'

export const txtColors = {
  txt_red: ['01', '02', '07', '08', '12', '13', '18', '19', '23', '24', '29', '30', '34', '35', '40', '45', '46'],
  txt_blue: ['03', '04', '09', '10', '14', '15', '20', '25', '26', '31', '36', '37', '41', '42', '47', '48'],
  txt_green: ['05', '06', '11', '16', '17', '21', '22', '27', '28', '32', '33', '38', '39', '43', '44', '49']
}

const Animals = ['鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪']
const codes = {
  0: ['01', '13', '25', '37', '49'],
  1: ['12', '24', '36', '48'],
  2: ['11', '23', '35', '47'],
  3: ['10', '22', '34', '46'],
  4: ['09', '21', '33', '45'],
  5: ['08', '20', '32', '44'],
  6: ['07', '19', '31', '43'],
  7: ['06', '18', '30', '42'],
  8: ['05', '17', '29', '41'],
  9: ['04', '16', '28', '40'],
  10: ['03', '15', '27', '39'],
  11: ['02', '14', '26', '38']
}
const Jiaqin = ['牛', '马', '羊', '鸡', '狗', '猪']

function animalHandle () {
  const now = new Date()
  const { year, month, day } = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  // console.log({ year: year, month: month, day: day })
  const { Animal } = Calendar.solar2lunar(year, month, day)
  // console.log(Animal)
  let index = 0
  const pre = []
  for (let i = 0; i < Animals.length; i++) {
    if (Animal === Animals[i]) {
      index = i
      break
    } else {
      pre.push(Animals[i])
    }
  }
  let handleAnimals = []
  let restAnimals = []
  if (!pre && !index) {
    handleAnimals = Animals
  } else {
    restAnimals = Animals.slice(index)
    handleAnimals = restAnimals.concat(pre)
  }
  const obj = {
    家禽: [],
    野兽: []
  }
  for (let i = 0; i < handleAnimals.length; i++) {
    obj[handleAnimals[i]] = codes[i]
    if (Jiaqin.includes(handleAnimals[i])) {
      obj['家禽'] = obj['家禽'].concat(codes[i])
    } else {
      obj['野兽'] = obj['野兽'].concat(codes[i])
    }
  }
  return obj
}

const animalCodes = animalHandle()
// console.log(animalCodes)
export const blockCombinations = {
  first: {
    styleWidth: '315px',
    list: [
      {
        name: '单',
        value: 'dan',
        ranges: ['01', '03', '05', '07', '09', '11', '13', '15', '17', '19', '21', '23', '25', '27', '29', '31', '33', '35', '37', '39', '41', '43', '45', '47', '49']
      },
      {
        name: '大',
        value: 'da',
        ranges: ['25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48']
      },
      {
        name: '大单',
        value: 'dadan',
        ranges: ['25', '27', '29', '31', '33', '35', '37', '39', '41', '43', '45', '47', '49']
      },
      {
        name: '小单',
        value: 'xiaodan',
        ranges: ['01', '03', '05', '07', '09', '11', '13', '15', '17', '19', '21', '23']
      },
      {
        name: '合单',
        value: 'hedan',
        ranges: ['01', '03', '05', '07', '09', '10', '12', '14', '16', '18', '21', '23', '25', '27', '29', '30', '32', '34', '36', '38', '41', '43', '45', '47']
      },
      {
        name: '特尾大',
        value: 'teweida',
        ranges: ['05', '06', '07', '08', '09', '15', '16', '17', '18', '19', '25', '26', '27', '28', '29', '35', '36', '37', '38', '39', '45', '46', '47', '48']
      },
      {
        name: '合尾大',
        value: 'heweida',
        ranges: ['05', '06', '07', '08', '09', '14', '15', '16', '17', '18', '23', '24', '26', '27', '32', '33', '34', '35', '36', '41', '42', '43', '44', '45']
      },
      {
        name: '双',
        value: 'shuang',
        ranges: ['02', '04', '06', '08', '10', '12', '14', '16', '18', '20', '22', '24', '26', '28', '30', '32', '34', '36', '38', '40', '42', '44', '46', '48']
      },
      {
        name: '小',
        value: 'xiao',
        ranges: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
      },
      {
        name: '大双',
        value: 'dashuang',
        ranges: ['26', '28', '30', '32', '34', '36', '38', '40', '42', '44', '46', '48']
      },
      {
        name: '小双',
        value: 'xiaoshuang',
        ranges: ['02', '04', '06', '08', '10', '12', '14', '16', '18', '20', '22', '24']
      },
      {
        value: 'heshuang',
        name: '合双',
        ranges: ['02', '04', '06', '08', '11', '13', '15', '17', '19', '20', '22', '24', '26', '28', '31', '33', '35', '37', '39', '40', '42', '44', '46', '48']
      },
      {
        name: '特尾小',
        value: 'teweixiao',
        ranges: ['01', '02', '03', '04', '10', '11', '12', '13', '14', '20', '21', '22', '23', '24', '30', '31', '32', '33', '34', '40', '41', '42', '43', '44']
      },
      {
        name: '合尾小',
        value: 'heweixiao',
        ranges: ['01', '02', '03', '04', '10', '11', '12', '13', '19', '20', '21', '22', '28', '29', '30', '31', '37', '38', '39', '40', '46', '47', '48', '49']
      }
    ]
  },
  second: {
    styleWidth: '270px',
    list: [
      {
        name: '红单',
        value: 'hongdan',
        ranges: ['01', '07', '13', '19', '23', '29', '35', '45']
      },
      {
        value: 'lvdan',
        name: '绿单',
        ranges: ['05', '11', '17', '21', '27', '33', '39', '43']
      },
      {
        name: '蓝单',
        value: 'landan',
        ranges: ['03', '09', '15', '25', '31', '37', '41', '47']
      },
      {
        name: '红大',
        value: 'hongda',
        ranges: ['29', '30', '34', '35', '40', '45', '46']
      },
      {
        name: '绿大',
        value: 'lvda',
        ranges: ['27', '28', '32', '33', '38', '39', '43', '44']
      },
      {
        name: '蓝大',
        value: 'landa',
        ranges: ['25', '26', '31', '36', '37', '41', '42', '47', '48']
      },
      {
        name: '红双',
        value: 'hongshuang',
        ranges: ['02', '08', '12', '18', '24', '30', '34', '40', '46']
      },
      {
        name: '绿双',
        value: 'lvshuang',
        ranges: ['06', '16', '22', '28', '32', '38', '44']
      },
      {
        name: '蓝双',
        value: 'lanshuang',
        ranges: ['04', '10', '14', '20', '26', '36', '42', '48']
      },
      {
        name: '红小',
        value: 'hongxiao',
        ranges: ['01', '02', '07', '08', '12', '13', '18', '19', '23', '24']
      },
      {
        name: '绿小',
        value: 'lvxiao',
        ranges: ['05', '06', '11', '16', '17', '21', '22']
      },
      {
        name: '蓝小',
        value: 'lanxiao',
        ranges: ['03', '04', '09', '10', '14', '15', '20']
      }
    ]
  },
  third: {
    styleWidth: '242px',
    list: [
      {
        name: '家禽',
        value: 'jiaqin',
        // ranges: ['01', '02', '03', '05', '06', '11', '13', '14', '15', '17', '18', '23', '25', '26', '27', '29', '30', '35', '37', '38', '39', '41', '42', '47']
        ranges: animalCodes['家禽']
      },
      {
        name: '牛',
        value: 'niu',
        // ranges: ['11', '23', '35', '47']
        ranges: animalCodes['牛']
      },
      {
        value: 'ma',
        name: '马',
        // ranges: ['06', '18', '30', '42']
        ranges: animalCodes['马']
      },
      {
        value: 'yang',
        name: '羊',
        // ranges: ['05', '17', '29', '41']
        ranges: animalCodes['羊']
      },
      {
        name: '鸡',
        value: 'ji',
        // ranges: ['03', '15', '27', '39']
        ranges: animalCodes['鸡']
      },
      {
        name: '狗',
        value: 'gou',
        // ranges: ['02', '14', '26', '38']
        ranges: animalCodes['狗']
      },
      {
        name: '猪',
        value: 'zhu',
        // ranges: ['01', '13', '25', '37', '49']
        ranges: animalCodes['猪']
      },
      {
        name: '野兽',
        value: 'yeshou',
        // ranges: ['04', '07', '08', '09', '10', '12', '16', '19', '20', '21', '22', '24', '28', '31', '32', '33', '34', '36', '40', '43', '44', '45', '46', '48']
        ranges: animalCodes['野兽']
      },
      {
        name: '猴',
        value: 'hou',
        // ranges: ['04', '16', '28', '40']
        ranges: animalCodes['猴']
      },
      {
        name: '鼠',
        value: 'shu',
        // ranges: ['12', '24', '36', '48']
        ranges: animalCodes['鼠']
      },
      {
        name: '虎',
        value: 'hu',
        ranges: animalCodes['虎']
        // ranges: ['10', '22', '34', '46']
      },
      {
        name: '兔',
        value: 'tu',
        ranges: animalCodes['兔']
        // ranges: ['09', '21', '33', '45']
      },
      {
        name: '龙',
        value: 'long',
        // ranges: ['08', '20', '32', '44']
        ranges: animalCodes['龙']
      },
      {
        name: '蛇',
        value: 'she',
        ranges: animalCodes['蛇']
        // ranges: ['07', '19', '31', '43']
      }
    ]
  },
  fourth: {
    styleWidth: '200px',
    list: [
      {
        name: '0尾',
        value: 'wei0',
        ranges: ['10', '20', '30', '40']
      },
      {
        name: '1尾',
        value: 'wei1',
        ranges: ['01', '11', '21', '31', '41']
      },
      {
        name: '2尾',
        value: 'wei2',
        ranges: ['02', '12', '22', '32', '42']
      },
      {
        name: '3尾',
        value: 'wei3',
        ranges: ['03', '13', '23', '33', '43']
      },
      {
        name: '4尾',
        value: 'wei4',
        ranges: ['04', '14', '24', '34', '44']
      },
      {
        name: '5尾',
        value: 'wei5',
        ranges: ['05', '15', '25', '35', '45']
      },
      {
        name: '6尾',
        value: 'wei6',
        ranges: ['06', '16', '26', '36', '46']
      },
      {
        name: '7尾',
        value: 'wei7',
        ranges: ['07', '17', '27', '37', '47']
      },
      {
        name: '8尾',
        value: 'wei8',
        ranges: ['08', '18', '28', '38', '48']
      },
      {
        name: '9尾',
        value: 'wei9',
        ranges: ['09', '19', '29', '39', '49']
      }
    ]
  },
  fifth: {
    styleWidth: '179px',
    list: [
      {
        name: '红',
        value: 'hong',
        ranges: ['01', '02', '07', '08', '12', '13', '18', '19', '23', '24', '29', '30', '34', '35', '40', '45', '46']
      },
      {
        name: '蓝',
        value: 'lan',
        ranges: ['03', '04', '09', '10', '14', '15', '20', '25', '26', '31', '36', '37', '41', '42', '47', '48']
      },
      {
        name: '绿',
        value: 'lv',
        ranges: ['05', '06', '11', '16', '17', '21', '22', '27', '28', '32', '33', '38', '39', '43', '44', '49']
      },
      {
        name: '前5',
        value: 'qian5',
        ranges: ['01', '02', '03', '04', '05']
      },
      {
        name: '前10',
        value: 'qian10',
        ranges: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']
      },
      {
        name: '全部',
        value: 'all',
        ranges: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49']
      },
      {
        name: '取消',
        value: 'cancel',
        ranges: []
      }
    ]
  }
}
