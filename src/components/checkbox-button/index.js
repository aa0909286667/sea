import XwCheckboxButton from '../checkbox/src/checkbox-button'

XwCheckboxButton.install = function (Vue) {
  Vue.component(XwCheckboxButton.name, XwCheckboxButton)
}
export default XwCheckboxButton
