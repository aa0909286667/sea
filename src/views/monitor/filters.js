export function typeFilter (type) {
  if (type === '-') {
    return type
  }
  const typeMap = {
    1: '投注异常',
    2: '撤单异常',
    3: '已开奖未结算',
    4: '已结算未派彩',
    5: '已撤单并派彩',
    6: '校验异常',
    7: '金流异常',
    8: '大额下注',
    9: '大额中奖',
    10: '接口调用异常'
  }
  return typeMap[type]
}

export function typeEXFilter (type) {
  if (type === '-') {
    return type
  }
  const typeMap = {
    1: '结算任务不存在',
    2: '未开奖',
    3: '结算超时',
    4: '冻结用户',
    5: '两期开奖结果相同系统撤单',
    6: '提前开奖'
  }
  return typeMap[type]
}

export function selfTypeFilter (type) {
  if (type === '-') {
    return type
  }
  const selfTypeMap = {
    0: '传统彩票',
    1: '系统自开彩',
    2: '厅自开彩'
  }
  return selfTypeMap[type]
}

export function emptyFilter (data) {
  if (data === undefined) {
    return '-'
  }
  return data
}

// 服务监控过滤
export function handleStatusFilter (type) {
  if (type === '-') {
    return type
  }
  const handleStatus = {
    0: '待处理',
    1: '处理成功',
    2: '处理失败'
  }
  return handleStatus[type]
}

export const typeEXs = [
  { orderType: 1, label: '投注异常', num: 0 },
  { orderType: 2, label: '撤单异常', num: 0 },
  { orderType: 3, label: '已开奖未结算', num: 0 },
  { orderType: 4, label: '已结算未派彩', num: 0 },
  { orderType: 5, label: '已撤单并派彩', num: 0 },
  { orderType: 6, label: '校验异常', num: 0 },
  { orderType: 7, label: '金流异常', num: 0 },
  { orderType: 8, label: '大额下注', num: 0 },
  { orderType: 9, label: '大额中奖', num: 0 },
  { orderType: 10, label: '接口调用异常', num: 0 }
]
export const typeEXBig = [
  { orderType: 1, label: '结算任务不存在', num: 0 },
  { orderType: 2, label: '未开奖', num: 0 },
  {
    orderType: 3, label: '结算超时', num: 0
  }
]
export const typeLottery = [
  { orderType: 0, label: '传统彩票', num: 0 },
  { orderType: 1, label: '系统自开彩', num: 0 },
  { orderType: 2, label: '厅自开彩', num: 0 }
]

// 服务监控类型的过滤
export function serviceFilter (type) {
  if (type === '-') {
    return type
  }
  const handleStatus = {
    81: 'DB同步延迟异常',
    88: '常规接口延迟',
    89: '金流接口延迟',
    // 82: '接口延迟',
    83: '采集中心服务异常',
    84: '采集源开奖号码不一致异常',
    85: '采集源提前开奖',
    86: '彩种维护通知'
  }
  return handleStatus[type]
}
