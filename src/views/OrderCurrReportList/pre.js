export const tabMode = [
  {
    id: 22,
    label: '3D系列',
    type: 0,
    name: '3D',
    playGroupId: 1,
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 12101,
        label: '二数',
        type: 0,
        name: 'zh',
        isShowSCode: false,
        playList: [1210121, 1210122, 1210111]
      },
      {
        id: 13101,
        label: '三数',
        type: 0,
        name: 'ss',
        isShowSCode: false,
        playList: [1310121, 1310122]
      },
      {
        label: '跑数',
        id: 11101,
        type: 0,
        name: 'lhd',
        isShowSCode: false,
        playList: [1110123]
      }
    ]
  },
  {
    id: 23,
    playGroupId: 2,
    label: '4D系列',
    type: 0,
    name: '4D',
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  },
  {
    id: 66,
    label: '泰国彩',
    type: 0,
    name: 'six',
    playGroupId: 1,
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 12101,
        label: '二数',
        type: 0,
        name: 'zh',
        isShowSCode: false,
        playList: [1210121, 1210122, 1210111]
      },
      {
        id: 13101,
        label: '三数',
        type: 0,
        name: 'ss',
        isShowSCode: false,
        playList: [1310121, 1310122]
      },
      {
        label: '跑数',
        id: 11101,
        type: 0,
        name: 'lhd',
        isShowSCode: false,
        playList: [1110123]
      }
    ]
  },
  {
    id: 84,
    label: '越南彩',
    type: 0,
    playGroupId: 4,
    name: 'k3',
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  },
  {
    id: 856,
    label: '老挝彩',
    type: 0,
    playGroupId: 2,
    name: 'elevenx5',
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  },
  {
    id: 60,
    label: '马来西亚',
    type: 0,
    name: 'pcdd',
    playGroupId: 2,
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  },
  {
    id: 65,
    label: '新加坡',
    type: 0,
    name: 'kl10',
    playGroupId: 2,
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  },
  {
    id: 21,
    label: '快乐8',
    type: 0,
    playGroupId: 3,
    name: 'kl8',
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 10202,
        label: '龙虎',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1020202, 1020203, 1020201]
      },
      {
        label: '大小', id: 10201, type: 0, playList: [1020102, 1020103, 1020101], name: 'gyjh', isShowSCode: false
      },
      {
        label: '单双', id: 11101, type: 0, playList: [1020512, 1020511, 1020522, 1020521, 1020502, 1020501], name: 'gyzh',
        isShowSCode: false
      },
      {
        id: 10204,
        playList: [1020402, 1020403, 1020401],
        label: '奇偶',
        type: 0,
        name: 'jo',
        isShowSCode: false
      },
      {
        id: 10203,
        playList: [1020301, 1020303, 1020302],
        label: '上下',
        type: 0,
        name: 'sx',
        isShowSCode: false
      },
      {
        id: 10206,
        playList: [1020601, 1020602, 1020603, 1020604, 1020605],
        label: '五行',
        type: 0,
        name: 'wx',
        isShowSCode: false
      }
    ]
  },
  {
    id: 886,
    label: '台湾',
    name: 'fc3d',
    type: 0,
    playGroupId: 2,
    childList: [
      // {
      //   id: 0, label: '官方玩法', type: 1, playList: [], name: 'OfficePlay'
      // },
      {
        id: 14101,
        label: '四数',
        type: 0,
        name: 'lm',
        isShowSCode: false,
        playList: [1410121, 1410122]
      },
      {
        label: '三数', id: 13101, type: 0, playList: [1310121, 1310122], name: 'gyjh', isShowSCode: false
      },
      {
        label: '跑数', id: 11101, type: 0, playList: [1110112, 1110122], name: 'gyzh', isShowSCode: false
      },
      {
        id: 12101,
        playList: [1210111, 1210121],
        label: '二数',
        type: 0,
        name: 'lhd',
        isShowSCode: false
      }
    ]
  }
]

export const TYPE_NAME = {
  10: 'ssc',
  // 时时彩
  11: 'elevenx5',
  // 11选5
  12: 'k3',
  // 快三
  13: 'kl10',
  // 快乐十分
  14: 'pk10',
  // 北京pk拾
  15: 'pcdd',
  // pc蛋蛋
  16: 'ssl',
  // 时时乐
  17: 'qxc',
  // 七星彩
  18: 'fc3d',
  // 福彩3D
  19: 'pl3',
  // 排列三
  20: 'six',
  // 六合彩
  21: 'kl8'
  // 快乐8
}
