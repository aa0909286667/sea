import {
  getReportUnSettleOrderList,
  getReportUnPayoutOrderList,
  getReportAgentRebateOrderList,
  postBatchDealAgentRebate,
  postBatchDeal,
  postBatchSettlementOrder
} from 'api/orders'

const unsettleOrder = {
  type: 1,
  listRequest: getReportUnSettleOrderList,
  dealRequest: postBatchSettlementOrder,
  dealPermission: 'BatchSettlementOrder',
  name: '未结算的注单',
  actionName: '人工结算'
}
const unPayoutOrdertleOrder = {
  type: 2,
  listRequest: getReportUnPayoutOrderList,
  dealRequest: postBatchDeal,
  dealPermission: 'BatchDeal',
  name: '已结算未派彩的注单',
  actionName: '手动派彩'
}
const agentRebateOrder = {
  type: 3,
  listRequest: getReportAgentRebateOrderList,
  dealRequest: postBatchDealAgentRebate,
  dealPermission: 'BatchDealAgentRebate',
  name: '有代理返点未派彩的注单',
  actionName: '手动返点'
}
export const exceptionTypes = [
  unsettleOrder,
  unPayoutOrdertleOrder,
  agentRebateOrder
]

export function getException (type) {
  const exception = exceptionTypes.find((order) => parseInt(order.type) === parseInt(type))
  return exception
}

