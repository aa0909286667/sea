export const statusFilter = (status) => {
  const statusMap = {
    0: '等待开奖',
    1: '用户撤单',
    2: '追号撤单',
    3: '系统撤单',
    4: '输',
    5: '平局',
    6: '赢',
    7: ''
  }
  return statusMap[status]
}

// export const statusFilter = (status) => {
//   const statusMap = {
//     0: '等待开奖',
//     1: '已撤单',
//     2: '已撤单',
//     3: '已撤单',
//     4: '未中奖',
//     5: '未中奖',
//     6: '已中奖',
//     7: ''
//   }
//   return statusMap[status]
// }

export const dealStatusFilter = (dealStatus) => {
  const statusMap = {
    0: '未派彩',
    1: '已派彩'
  }
  return statusMap[dealStatus]
}

export const prizeFilter = (prize) => {
  const arr1 = prize.split(',')
  const arr2 = []
  arr1.forEach((v) => {
    if (v.indexOf('|') > -1) {
      arr2.push(v.split('|')[1])
    } else {
      arr2.push(v)
    }
  })
  const strArr = arr2.map((ele) => `${(ele / 1000).toFixed(3)},`)
  const result = ''.concat(...strArr)
  return result.slice(0, result.length - 1)
}

export const rebateStatusFilter = (status) => {
  const statusMap = {
    0: '等待开奖',
    1: '已结算未发放',
    2: '已发放'
  }
  return statusMap[status]
}

export const TimeFilter = (time) => {
  if (!time) return '暂无'
  const t = new Date('2000-01-01 00:00:00').getTime()
  const tt = new Date(time).getTime()
  return tt > t ? time : '暂无'
}
