const revsTypes = [
  {
    label: '奖金+投注返点',
    value: 1
  },
  {
    label: '代理返点',
    value: 2
  }
]
const revsStatuses = [
  {
    label: '待发放',
    value: 0
  },
  {
    label: '发放成功',
    value: 1
  },
  {
    label: '发放失败',
    value: 2
  }
]

const setteleNums = []
setteleNums.push({ label: '全部', value: '-1' })
for (let i = 0; i < 10; i++) {
  const num = {
    label: `重新结算${i + 1}次`,
    value: i + 1
  }
  setteleNums.push(num)
}

function revsTypeFilter (type) {
  const revesType = revsTypes.find((revsType) => revsType.value === type)
  // console.log(revesType)
  return revesType ? revesType.label : ''
}

function revsStatusFilter (status) {
  const revesStatus = revsStatuses.find((revsStatus) => revsStatus.value === status)
  return revesStatus ? revesStatus.label : ''
}

export {
  revsTypes,
  revsStatuses,
  revsTypeFilter,
  revsStatusFilter,
  setteleNums
}
