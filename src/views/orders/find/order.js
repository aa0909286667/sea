/**
 * Created by PaulChou on 2018/11/20.
 */
export function getAgentRebateStatus (row) {
  if (row.status === 0) {
    return '等待开奖'
  } if (row.agentRebateNeedGrant === 0 || row.status === 1 || row.status === 2 || row.status === 3 || row.status === 5) {
    return '不需要发放'
  }
  if (row.agentRebateNeedGrant === 1) {
    // 输赢发放
    if (row.agentRebateStatus === 2) {
      return '已发放'
    }
    // 未发放或发放异常
    return '未发放'
  }
  if (row.agentRebateNeedGrant === 2) {
    // 输发放
    if (row.status === 4) {
      if (row.agentRebateStatus === 2) {
        return '已发放'
      }
      // 未发放或发放异常
      return '未发放'
    }
    return '不需要发放'
  } if (row.agentRebateNeedGrant === 3) {
    // 赢发放
    if (row.status === 6) {
      if (row.agentRebateStatus === 2) {
        return '已发放'
      }
      // 未发放或发放异常
      return '未发放'
    }
    return '不需要发放'
  }
}

export function getAgentStatusClass (row) {
  const status = this.getAgentRebateStatus(row)
  switch (status) {
    case '等待开奖':
      return 'yellow'
    case '不需要发放':
      return 'black'
    case '未发放':
      return 'black'
    case '已发放':
      return 'green'
  }
}

export function getStatusClass (status) {
  switch (status) {
    case 0:
      return 'yellow'
    case 4:
      return 'red'
    case 5:
      return 'blue'
    case 6:
      return 'green'
    default:
      return 'black'
  }
}
