/**
 * Created by fx on 2017/7/19.
 */
import { ERR_OK } from 'api/config'
// import { getAjaxData } from 'api/common'
import { getSpTplInfoList, postSpTplInfoCopy, postSpTplInfoUpName } from 'api/spltpl/'
import fuzzyQuerySearch from 'components/fuzzyQuerySearch/index'
import SetList from './SetList'
import AgentList from './agentList'

const inputPattern = /^[^!@#$%&'*+/=?^`{|} ~]+$/
export default {
  data () {
    return {
      loaded: true,
      getStatu: false,
      // spCodes: [],
      spTplId: null, // 平台商唯一ID
      spCodeInput: '',
      tableData: [],
      cacheResData: [],
      cacheTplsIdName: {}, // 缓存编辑框值
      showPagination: false,
      total: 0,
      query: {
        pageNo: 1,
        pageSize: 10
      },
      timer: null,
      timeMin: 200,
      timeOut: 5000
    }
  },
  components: { SetList, AgentList, fuzzyQuerySearch },
  methods: {
    getSpTplInfo () {
      if (this.spCodeInput === '') return
      this.getStatu = true
      const query = { spCode: this.spCodeInput }
      // 判断是否超时
      setTimeout(() => {
        this.getStatu = false
      }, this.timeOut)
      getSpTplInfoList(query).then((res) => {
        // console.warn('getSpTplInfoList->', res);
        // 200ms内 获取到数据 不显示loading
        clearTimeout(this.timer)
        this.timer = setTimeout(() => {
          this.getStatu = false
        }, this.timeMin)
        if (res.data.code === ERR_OK) {
          const _data = res.data.data.spTplInfoList
          this.cacheResData = _data.map((v) => {
            v.edit = false
            v.show = Boolean(v.status)
            return v
          })
          if (this.cacheResData.length > this.query.pageSize) {
            this.showPagination = true
            this.tableData = this.pagination(this.query.pageNo, this.query.pageSize, this.cacheResData)
          } else {
            this.showPagination = false
            this.tableData = this.cacheResData
          }
          this.total = this.cacheResData.length
        } else {
          throw new Error(`code: ${res.data.code}`)
        }
      }).catch((err) => {
        console.log(err)
      })
    },
    dealChange (op) {
      this.spCodeInput = op.spCode
    },
    // getSpInfo () {
    //   getAjaxData({ dataType: 21 }).then(res => {
    //     if (res.data.code === ERR_OK) {
    //       this.spCodes = res.data.data.items.map(v => {
    //         const t = {
    //           value: v.id,
    //           label: v.name
    //         }
    //         return t
    //       })
    //       this.loaded = true
    //     }
    //   })
    // },
    // querySearch(queryString, cb) {
    //   const spCodes = this.spCodes
    //   const res = queryString ? spCodes.filter(this.createFilter(queryString)) : spCodes
    //   // 返回建议列表的数据
    //   cb(res)
    // },
    // createFilter(queryString) {
    //   // filter第一个形参为funtion function里第一个形参为current value
    //   const _fn = function (currentValue) {
    //     return currentValue.value.toLowerCase().indexOf(queryString.toLowerCase()) > -1
    //   }
    //   return _fn
    // },
    // handleSelect(item) {
    //   this.spTplId = item.id
    // },
    handleChange () {
      // 切换spCode pageNo复位
      if (this.query.pageNo !== 1) this.query.pageNo = 1
    },
    // 复制盘口
    spTplInfoCopy (row) {
      const LANG_NAME = '盘口名'
      this.$prompt(`请输入新${LANG_NAME}：`, '提示', {
        confirmButtonLoading: false,
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputErrorMessage: '盘口名只能是中英文，数字',
        inputPlaceholder: row.tplName,
        inputValidator (val) {
          if (val === '') return `${LANG_NAME}不能为空`
          return inputPattern.test(val)
        },
        beforeClose: (action, instance, done) => {
          if (action === 'confirm') {
            if (instance.inputValue === null) {
              this.$message({
                type: 'info',
                message: `${LANG_NAME}不能为空`
              })
              return
            }
            if (instance.inputValue.length > 20) {
              this.$message({
                type: 'info',
                message: '长度不得大于20个字符'
              })
              return
            }
            instance.confirmButtonLoading = true
            instance.confirmButtonText = '复制中...'
            const query = { spTplId: row.id, tplName: instance.inputValue, spCode: this.spCodeInput }
            postSpTplInfoCopy(query).then((res) => {
              // const {code, desc} = res.data; // code:0-succ
              if (res.data.code === ERR_OK) {
                this.getSpTplInfo()
                done()
                setTimeout(() => {
                  instance.confirmButtonLoading = false
                }, 300)
              } else {
                instance.confirmButtonLoading = false
                instance.confirmButtonText = '确定'
              }
            }).catch(() => {
              instance.confirmButtonLoading = false
              instance.confirmButtonText = '确定'
            })
          } else {
            done()
          }
        }
      }).then(({ value }) => {
        this.$message({
          type: 'success',
          message: `新${LANG_NAME}为: ${value}`,
          duration: 2000
        })
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '取消复制'
        })
      })
    },
    // 设置盘口
    spTplSetList (row) {
      this.$store.dispatch('sptpl/spTplSpCode', this.spCodeInput)
      this.$store.dispatch('sptpl/spTplRowId', row.id)
      this.$store.dispatch('sptpl/spTplHidePreview')
    },
    // 跳转代理列表
    spTplAgentList (row) {
      this.$store.dispatch('sptpl/spTplSpCode', this.spCodeInput)
      this.$store.dispatch('sptpl/spTplRowId', row.id)
      this.$store.dispatch('sptpl/spTplShowAgent', true)
    },
    // 代理列表返回厅主列表
    spTplHideAgent () {
      this.$store.dispatch('sptpl/spTplSpCode')
      this.$store.dispatch('sptpl/spTplRowId')
      this.$store.dispatch('sptpl/spTplShowAgent', false)
    },
    // 重命名盘口
    spTplReName (row, flag) {
      row.edit = flag // true 修改 false 完成
      // 点‘完成’
      if (!flag) {
        // 重置文本框的值
        const resetRowInput = () => {
          row.tplName = this.cacheTplsIdName[row.id]
          delete this.cacheTplsIdName[row.id]
        }
        // 未修改 不提示
        if (this.cacheTplsIdName[row.id] === row.tplName) {
          delete this.cacheTplsIdName[row.id]
          return
        }
        // 验证修改格式
        if (!inputPattern.test(row.tplName)) {
          this.$message({
            type: 'error',
            message: '盘口名不能包含空格和特殊字符',
            duration: 2000,
            onClose () {
              resetRowInput()
            }
          })
          return
        }
        // 最大长度
        if (row.tplName.length > 25) {
          this.$message({
            type: 'info',
            message: '盘口名长度不得大于25字符',
            duration: 2000,
            onClose () {
              resetRowInput()
            }
          })
          return
        }
        // 格式正确请求服务器
        const query = { spTplId: row.id, tplName: row.tplName, spCode: row.spCode }
        postSpTplInfoUpName(query).then((res) => {
          if (res.data.code === ERR_OK) {
            this.$message({
              type: 'success',
              message: '盘口名修改成功'
            })
            delete this.cacheTplsIdName[row.id]
          } else {
            resetRowInput()
          }
        })
      } else {
        if (Object.prototype.hasOwnProperty.call(!this.cacheTplsIdName, row.id)) this.cacheTplsIdName[row.id] = row.tplName // 存编辑框值
      }
    },
    // 分页
    handleSizeChange (val) {
      this.query.pageSize = val
      this.tableData = this.pagination(this.query.pageNo, this.query.pageSize, this.cacheResData)
    },
    handleCurrentChange (val) {
      this.query.pageNo = val
      this.tableData = this.pagination(this.query.pageNo, this.query.pageSize, this.cacheResData)
    },
    pagination (pageNo, pageSize, array) {
      const offset = (pageNo - 1) * pageSize
      return offset + pageSize >= array.length ? array.slice(offset, array.length) : array.slice(offset, offset + pageSize)
    }
  },
  computed: {
    showPreview () {
      return this.$store.state.sptpl.showPreview
    },
    showAgent () {
      return this.$store.state.sptpl.showAgent
    }
  },
  created () {
    // this.getSpInfo()
    this.$store.dispatch('sptpl/spTplShowPreview')
  }
}
