/**
 * Created by fx on 2017/7/21.
 */
export const tabNav = [
  // {
  //   label: '返点设置', key: 'rebate', data: null, cmd: 'SpTplRebateSetList'
  // },
  {
    label: '彩种开关', key: 'SpTplLotterySetList', data: null, cmd: 'SpTplLotterySetList'
  },
  {
    label: '玩法赔率设置', key: 'SpTplPlayList', data: null, cmd: 'SpTplPlayList'
  },
  {
    label: '热门彩种', key: 'SpTplLotteryHotList', data: null, cmd: 'SpTplLotteryHotList'
  },
  {
    label: '玩法分类', key: 'SpTplPlaytypeSetList', data: null, cmd: 'SpTplPlaytypeSetList'
  }
  // {
  //   label: '首页推荐', key: 'recmdlottery', data: null, cmd: 'SpTplLotteryRecommendList'
  // }, // 首页推荐
  // {
  //   label: '球数限制', key: 'dwdSetting', data: null, cmd: 'DwdLimitList'
  // } // 球数限制
]
