export function _setModelKey (modelKey, props, val) {
  const [parm1, parm2, parm3] = props
  if (props.length === 1) {
    this[modelKey][parm1] = val
  } else if (props.length === 2) {
    this[modelKey][parm1][parm2] = val
  } else {
    this[modelKey][parm1][parm2][parm3] = val
  }
}

function clearNoNum (v) {
  // 先把非数字的都替换掉，除了数字和.
  v = v.replace(/[^\d.]/g, '')
  // 保证只有出现一个.而没有多个.
  v = v.replace(/\.{2,}/g, '.')
  // 必须保证第一个为数字而不是.
  v = v.replace(/^\./g, '')
  // 保证.只出现一次，而不能出现两次以上
  v = v.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.')
  // 只能输入最多三位小数
  return v.replace(/^(-)*(\d+)\.(\d\d\d).*$/, '$1$2.$3')
}

export function limitNumberRange (modelKey, props, e, flag, valMin = 0, valMax = 9999999.99) {
  console.log('modelKey', modelKey)
  console.log('props', props)
  const tFlag = props.includes('prize')
  console.log(tFlag)
  let lenText = '两'
  const value = modelKey === 'limitInfo' ? Math.floor(clearNoNum(e.target.value)) : clearNoNum(e.target.value) // 格式化输入的值
  let re = modelKey === 'limitInfo' ? /^\d+$/ : /^\d+(\.\d{0,2})?$/
  const min = modelKey === 'limitInfo' ? 1 : valMin
  let max = modelKey === 'limitInfo' ? 9999999 : valMax
  let conText = `金额请输入${min} - ${max}之间的数值`
  if (tFlag) {
    lenText = '三'
    re = /^\d+(\.\d{0,3})?$/
  }
  if (flag) {
    max = 100000
    conText = `倍数请输入值0或者1 - ${max}之间的数值,保留小数点后面${lenText}位`
  }
  if (re.test(value)) {
    if (parseFloat(value) < min || parseFloat(value) > max) {
      this.$nextTick(() => {
        if (value < min) {
          e.target.value = min
          this._setModelKey(modelKey, props, min)
        }
        if (value > max) {
          e.target.value = max
          this._setModelKey(modelKey, props, max)
        }
      })
      this.$message({
        type: 'warning',
        message: conText
      })
    } else if (flag) {
      if (min < parseFloat(value) && parseFloat(value) < 1) {
        if (parseFloat(value) - 0.5 >= 0) {
          e.target.value = 1
          this._setModelKey(modelKey, props, 1)
          this.$message({
            type: 'warning',
            message: conText
          })
        } else {
          e.target.value = min
          this._setModelKey(modelKey, props, min)
          this.$message({
            type: 'warning',
            message: conText
          })
        }
      } else {
        e.target.value = value
        this._setModelKey(modelKey, props, value)
      }
    } else {
      e.target.value = value
      this._setModelKey(modelKey, props, value)
    }
  } else {
    e.target.value = min
    const tcontent = modelKey === 'limitInfo' ? '输入的数值不能为空或格式不正确,只允许整数' : `输入的数值不能为空或格式不正确,只允许正整数或正${lenText}位小数`
    this.$message({
      type: 'warning',
      message: `${tcontent}`
    })
    this._setModelKey(modelKey, props, min)
  }
}

