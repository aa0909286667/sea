/**
 * Created by Xiaowu on 2018/12/11.
 */
export default [
  {
    type_config: '注单配置',
    isShow: true,
    elements: [
      {
        name: '陪玩或试玩用户的单注下注限额',
        mission: '陪玩或试玩用户投注单注最大金额,0表示不限制',
        configKey: 'ORDER_PW_MAX_BET',
        configValue: '0',
        configDesc: '陪玩或试玩用户的单注下注限额，单位元',
        type: 'input',
        unit: '元',
        array: []
      },
      {
        name: '陪玩或试玩用户的单笔下注限额',
        mission: '陪玩或试玩用户投注单笔最大金额,0表示不限制',
        configKey: 'ORDER_PW_MAX_BET_ALL',
        configValue: '0',
        configDesc: '陪玩或试玩用户的单笔下注限额，单位元',
        type: 'input',
        unit: '元',
        array: []
      },
      {
        name: '陪玩或试玩用户的最大中奖限额',
        mission: '陪玩或试玩用户最大中奖金额,0表示不限制',
        configKey: 'ORDER_PW_MAX_WIN_MONEY',
        configValue: '0',
        configDesc: '陪玩或试玩用户的最大中奖限额，单位元',
        type: 'input',
        unit: '元',
        array: []
      },
      {
        name: '注单的派彩延迟时间',
        mission: '',
        configKey: 'ORDER_DEAL_REWARD_DELAY_TIME',
        configValue: '30',
        configDesc: '注单的派彩延迟时间,单位分钟',
        type: 'input',
        unit: '分钟',
        array: []
      },
      {
        name: '注单在缓存中保存的时间',
        mission: '',
        configKey: 'REDIS_ORDER_DETAIL_DAYS',
        configValue: '2',
        configDesc: '注单在缓存中保存的天数，单位天',
        type: 'input',
        unit: '天',
        array: []
      },
      {
        name: '注单导出最大导出数量',
        mission: '*超过该数量时将给出预警',
        configKey: 'REPORT_EXPORT_ORDER_NUM',
        configValue: '100000',
        configDesc: '注单导出最大导出数量，单位条，超过该数量时将给出预警',
        type: 'input',
        unit: '条',
        array: []
      },
      {
        name: '限制注单导出时间段(美东时间)',
        mission: '*限制注单导出时间段,请输入格式为"01|23",00|00表示不限制',
        configKey: 'ORDER_EXPORT_LIMIT_TIME',
        configValue: '00|23',
        configDesc: '限制注单导出时间段,如:00|23表示从00:00:00到23:00:00',
        type: 'input',
        unit: '',
        array: []
      }
      // {
      //   name: '下注使用版本',
      //   mission: '*投注使用版本[1.使用新版本注单服务 0.使用老版本注单服务]',
      //   configKey: 'ORDER_LOTTERY_BET_VERSION',
      //   configValue: '1',
      //   configDesc: '投注使用版本[1.使用新版本注单服务 0.使用老版本注单服务]',
      //   type: 'radio',
      //   unit: '',
      //   array: {
      //     on: {
      //       value: '1',
      //       label: '新版本'
      //     },
      //     off: {
      //       value: '0',
      //       label: '老版本'
      //     }
      //   }
      // }
    ]
  },
  {
    type_config: '结算配置',
    isShow: true,
    elements: [
      {
        name: '手动结算允许时间',
        mission: '',
        configKey: 'SETTLEMENT_MANUAL_TIME',
        configValue: '1200',
        configDesc: '手动结算允许时间，单位分钟',
        type: 'input',
        unit: '分钟',
        array: []
      },
      {
        name: '重新结算允许时间',
        mission: '',
        configKey: 'SETTLEMENT_RESET_TIME',
        configValue: '1200',
        configDesc: '重新结算允许时间，单位分钟',
        type: 'input',
        unit: '分钟',
        array: []
      },
      {
        name: '结算时大额中奖阈值',
        mission: '*超过该阈值时将给出预警',
        configKey: 'SYS_MAX_WIN_MONEY',
        configValue: '1000',
        configDesc: '结算时大额中奖阀值，单位元，超过该阀值时将给出预警',
        type: 'input',
        unit: '元',
        array: []
      }
    ]
  },
  {
    type_config: '盘口配置',
    isShow: true,
    elements: [
      {
        name: '定位胆下注限制开关',
        mission: '',
        configKey: 'PLATFORM_BUY_LIMIT_SWITCH',
        configValue: 'off',
        configDesc: '购买限制开关设置(即定位胆下注限制开关)on:表示打开 off:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: 'on',
            label: '开启'
          },
          off: {
            value: 'off',
            label: '关闭'
          }
        }
      },
      {
        name: '用户可下注的最高返点限制',
        mission: '*大于该值时不允许投注',
        configKey: 'PLATFORM_MAX_REBATE_RATIO',
        configValue: '10',
        configDesc: '用户可下注的最高返点限制,单位百分比,即用户最大返点大于该值时不允许投注',
        type: 'input',
        unit: '%',
        array: []
      },
      {
        name: '六合彩用户可下注的最高返点限制',
        mission: '*大于该值时不允许投注',
        configKey: 'PLATFORM_SIX_MAX_REBATE_RATIO',
        configValue: '13',
        configDesc: '六合彩用户可下注的最高返点限制,单位百分比,即用户最大返点大于该值时不允许投注',
        type: 'input',
        unit: '%',
        array: []
      },
      {
        name: '下注时大额下注阈值',
        mission: '*超过该阈值时将给出预警',
        configKey: 'SYS_MAX_ORDER_MONEY',
        configValue: '100',
        configDesc: '下注时大额下注阀值，单位元，超过该阀值时将给出预警',
        type: 'input',
        unit: '元',
        array: []
      },
      {
        name: '厅主允许设置最高赔率的万分比控制',
        mission: '',
        configKey: 'SP_PROFIT_RATE',
        configValue: '20',
        configDesc: '厅主允许设置最高赔率的百分比控制',
        type: 'input',
        unit: '‱',
        array: []
      },
      {
        name: '一次性批量入库的最大条数',
        mission: '*请根据实际情况调整',
        configKey: 'BATCH_TO_DB_COUNT',
        configValue: '500',
        configDesc: '一次性批量入库的最大条数,请根据实际情况调整',
        type: 'input',
        unit: '条',
        array: []
      },
      {
        name: '最多批量更新彩期的遗漏值条数',
        mission: '*设置的时间为北京时间',
        configKey: 'BATCH_TO_UPDATE_MISS_DATAS',
        configValue: '1000',
        configDesc: '最多批量更新彩期的遗漏值条数',
        type: 'input',
        unit: '条',
        array: []
      }
    ]
  },
  {
    type_config: '系统配置',
    isShow: true,
    elements: [
      {
        name: '黑白名单控制开关',
        mission: '',
        configKey: 'PLATFORM_WHITE_LIST_SWITCH',
        configValue: '0',
        configDesc: '黑白名单控制开关1:表示打开限制 0:表示关闭限制',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '二维码扫描验证开关',
        mission: '',
        configKey: 'QRCODE_SWITCH',
        configValue: '0',
        configDesc: '二维码扫描验证开关,0表示不开启二维码验证;1表示开启二维码验证',
        type: 'radio',
        unit: '',
        array: [
          {
            value: '0',
            label: '不开启二维码验证'
          },
          {
            value: '1',
            label: '开启二维码验证'
          }
        ]
      },
      {
        name: '二维码扫描验证缓存有效时间',
        mission: '*二维码扫描验证缓存有效时间',
        configKey: 'QRCODE_CACHE_TIME',
        configValue: '10',
        configDesc: '二维码扫描验证缓存有效时间，单位分钟',
        type: 'input',
        unit: '分钟',
        array: []
      },
      {
        name: '中奖快讯的条数',
        mission: '*默认为30条',
        configKey: 'WINRANKING_LATEST_WINNER_NUM',
        configValue: '30',
        configDesc: '中奖快讯的条数',
        type: 'input',
        unit: '条',
        array: []
      },
      {
        name: '中奖快讯缓存的时间',
        mission: '*默认为5分钟',
        configKey: 'WINRANKING_LATEST_WINNER_CACHE_TIME',
        configValue: '5',
        configDesc: '中奖快讯缓存的时间',
        type: 'input',
        unit: '分钟',
        array: []
      },
      {
        name: '彩种维护安全验证码',
        mission: '*平台进行彩种维护推送时的安全验证码',
        configKey: 'PLATFORM_LOTTERY_MAINTAIN_SECRETKEY',
        configValue: '',
        configDesc: '平台进行彩种维护推送时的安全验证码',
        type: 'input',
        unit: '',
        array: []
      }

    ]
  },
  {
    type_config: '报表配置',
    isShow: true,
    elements: [
      {
        name: '报表时间查询的跨度',
        mission: '',
        configKey: 'HISTORY_SEARCH_MONTHS',
        configValue: '3',
        configDesc: '报表时间查询的跨度，默认为3个月',
        type: 'input',
        unit: '月',
        array: []
      },
      {
        name: '从系统配置中获得报表查询的最早时间',
        mission: '',
        configKey: 'REPORT_EARLIEST_DATE',
        configValue: '2017-11-01',
        configDesc: '从系统配置中获得报表查询的最早时间为2017-11-01，格式必须为yyyy-MM-dd',
        type: 'date',
        unit: '',
        array: []
      },
      {
        name: '预统计报表的重新跑批统计操作地址',
        mission: '',
        configKey: 'REPORT_RESTATISTICS',
        configValue: 'http://10.28.177.202:9241/lottery-statistics/statistics/summary',
        configDesc: '预统计报表的重新跑批统计操作地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '报表导出分页大小',
        mission: '',
        configKey: 'REPORT_EXPORT_PAGE_SIZE',
        configValue: '1000',
        configDesc: '报表导出分页大小',
        type: 'input',
        unit: '',
        array: []
      }
    ]
  },
  {
    type_config: '监控配置',
    isShow: true,
    elements: [
      {
        name: '监控-彩期-结算超时时间',
        mission: '',
        configKey: 'MONITOR_ISSUE_SETTLE_TIME_OUT',
        configValue: '600000',
        configDesc: '监控-彩期-结算超时时间，单位毫秒',
        type: 'input',
        unit: '毫秒',
        array: []
      },
      {
        name: '监控-DB同步监控时间阈值',
        mission: '',
        configKey: 'MONITOR_DB_SYNC_THRESHOLD_MS',
        configValue: '600000',
        configDesc: '监控-DB同步监控时间阈值，单位毫秒',
        type: 'input',
        unit: '毫秒',
        array: []
      },
      {
        name: '监控-监控采集中心',
        mission: '',
        configKey: 'MONITOR_COLLECTOR_URLS',
        configValue: '600000',
        configDesc: '监控-监控采集中心服务',
        type: 'textarea',
        unit: '',
        array: []
      },
      {
        name: '平台其它接口监控延迟时间阈值',
        mission: '',
        configKey: 'MONITOR_API_OTHER_DELAY_THRESHOLD_MS',
        configValue: '600000',
        configDesc: '平台其它接口监控延迟时间阈值，单位毫秒',
        type: 'input',
        unit: '毫秒',
        array: []
      },
      {
        name: '平台金流接口监控延迟时间阈值',
        mission: '',
        configKey: 'MONITOR_API_FLOW_DELAY_THRESHOLD_MS',
        configValue: '600000',
        configDesc: '平台金流接口监控延迟时间阈值，单位毫秒',
        type: 'input',
        unit: '毫秒',
        array: []
      }
    ]
  },
  {
    type_config: '自开彩配置',
    isShow: true,
    elements: [
      {
        name: '全局自开彩统计需要过滤的厅',
        mission: '*多个厅以,分隔',
        configKey: 'REPORT_EXCLUDE_SN_LIST',
        configValue: 'sd00,sc00,sb00,mn00,mm00,lo00,pw00,ll00,lj00,li00,lh00,jc00,iy00,hs00,hr00,hq00,ho00,hn00,hi00,hh00,hg00,hf00,ha00,gv00,gi00,eq00,ep00,ej00,eh00,cm00,ar00,ap00,am00,aj00,ai00,ah00,af00,ad00,ac00,ab00,aa00,SP02,BG00,DEMO,SP01,TEST01',
        configDesc: '全局自开彩统计需要过滤的厅',
        type: 'textarea',
        unit: '',
        array: []
      },
      {
        name: '系统自开彩开关',
        mission: '',
        configKey: 'SELF_LOTTERY_PUBLIC_SWITCH',
        configValue: 'true',
        configDesc: '系统自开彩开关设置on:表示打开 off:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: 'on',
            label: '开启'
          },
          off: {
            value: 'off',
            label: '关闭'
          }
        }
      },
      {
        name: '厅自开彩全局随机算法百分比',
        mission: '*默认60%',
        configKey: 'SELF_LOTTERY_RANDOM_VALUE',
        configValue: '70',
        configDesc: '厅自开彩全局随机算法百分比，默认60%',
        type: 'input',
        unit: '%',
        array: []
      },
      {
        name: '自开彩服务HTTP访问地址',
        mission: '',
        configKey: 'SELF_LOTTERY_SERVER_URL',
        configValue: 'http://10.28.177.202:19007/pre_settlement',
        configDesc: '自开彩服务HTTP访问地址',
        type: 'input',
        unit: '',
        array: []
      }
    ]
  },
  {
    type_config: '平台配置',
    isShow: true,
    elements: [
      {
        name: '彩票平台访问直播服务的地址',
        mission: '',
        configKey: 'LIVE_SERVER_URL',
        configValue: 'http://47.90.38.0:9009/live-api',
        configDesc: '彩票平台访问直播服务的地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '平台auth接口访问地址',
        mission: '',
        configKey: 'PLATFORM_AUTH_API',
        configValue: 'http://sn.bgbet1.com/auth/api/',
        configDesc: '平台auth接口访问地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '平台金流方面操作接口',
        mission: '',
        configKey: 'PLATFORM_CASHFLOW_API',
        configValue: 'http://web.bg1207.com/ltgw/api/',
        configDesc: '平台金流方面操作接口',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '平台cloud接口访问地址',
        mission: '',
        configKey: 'PLATFORM_CLOUD_API',
        configValue: 'http://web.bg1207.com/ltgw/api/',
        configDesc: '平台cloud接口访问地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '总平台4号服务类型的API地址',
        mission: '',
        configKey: 'PLATFORM_OTHER_API',
        configValue: 'http://10.113.1.116/mt-cloud/api/',
        configDesc: '总平台4号服务类型的API地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '平台中使用的测试厅编码',
        mission: '',
        configKey: 'PLATFORM_TEST_SP_CODE',
        configValue: 'ae00',
        configDesc: '平台中使用的测试厅编码',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '派彩调用平台接口方式',
        mission: '',
        configKey: 'SINGLE_DEAL',
        configValue: '1',
        configDesc: '派彩是否单条调用平台接口',
        type: 'radio',
        unit: '',
        array: [
          {
            value: 'true',
            label: '单条'
          },
          {
            value: 'false',
            label: '批量'
          }
        ]
      },
      {
        name: '平台商维护计划在缓存中保存的时间',
        mission: '',
        configKey: 'SP_MAINTAIN_DAYS',
        configValue: '7',
        configDesc: '平台商维护计划在缓存中保存的天数，单位天',
        type: 'input',
        unit: '天',
        array: []
      },
      {
        name: '是否走平台api',
        mission: '',
        configKey: 'REAL_VERSION',
        configValue: 'true',
        configDesc: '是否走平台api true 走 false 不走',
        type: 'radio',
        unit: '',
        array: [
          {
            value: 'true',
            label: '是'
          },
          {
            value: 'false',
            label: '否'
          }
        ]
      }
    ]
  },
  {
    type_config: '服务端配置',
    isShow: true,
    elements: [
      {
        name: '彩票采集服务HTTP访问地址',
        mission: '*该地址必须以 / 符号结束',
        configKey: 'COLLERTOR_SERVER_URL',
        configValue: 'http://lotv2-admin.bgbet3.com/lottery-collector/',
        configDesc: '采集服务HTTP访问地址,该地址必须以 / 符号结束',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '开奖号码推送到第三方服务地址',
        mission: '*多个地址以英文逗号分隔,空表示无需推',
        configKey: 'COLLECTOR_OPENCODE_PUSHTOTHIRD_URLS',
        configValue: '',
        configDesc: '开奖号码推送到第三方服务地址配置，多个地址以英文逗号分隔',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '异常统计任务地址',
        mission: '',
        configKey: 'EXCEPTION_JOB_URL',
        configValue: 'http://10.26.138.237:9241/lottry-logs-job/logs/opencodemsg',
        configDesc: '异常统计任务地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: 'IM服务器API访问URL地址',
        mission: '*该地址必须以 / 符号结束',
        configKey: 'IM_API',
        configValue: 'http://10.26.138.237:9011/lottery-im/api/',
        configDesc: 'IM服务器API访问URL地址,该地址必须以 / 符号结束',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '在JOB中产生彩期的间隔时间',
        mission: '',
        configKey: 'JOB_ISSUE_PROD_DAYS',
        configValue: '1',
        configDesc: '在JOB中产生彩期的间隔天数，单位天',
        type: 'input',
        unit: '天',
        array: []
      },
      {
        name: '加拿大开奖信息服务的地址',
        mission: '',
        configKey: 'PLATFORM_CANADA_RESULT_URL',
        configValue: 'http://47.75.78.32:8080/result/canadaResult',
        configDesc: '加拿大开奖信息服务的地址',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '所有厅单个账期中的信用授权阈值',
        mission: '',
        configKey: 'PLATFORM_CREDIT_AUTH_THRESHOLD',
        configValue: '1000000',
        configDesc: '所有厅单个帐期中的信用授权阀值,单位百分比',
        type: 'input',
        unit: '%',
        array: []
      },
      {
        name: '彩期在缓存中保存的时间',
        mission: '',
        configKey: 'REDIS_ISSUE_DETAIL_DAYS',
        configValue: '4',
        configDesc: '彩期在缓存中保存的天数，单位天',
        type: 'input',
        unit: '天',
        array: []
      },
      {
        name: '临时数据在缓存中存在的有效时间',
        mission: '',
        configKey: 'REDIS_TEMPORARY_TIMES',
        configValue: '1',
        configDesc: '临时数据在缓存中存在的有效时间，单位分钟',
        type: 'input',
        unit: '分钟',
        array: []
      },
      // {
      //   name: '新版本预统计报表服务开关',
      //   mission: '',
      //   configKey: 'REPORT_PRE_STATISTICS_SWITCH',
      //   configValue: 'on',
      //   configDesc: '新版本预统计报表服务开关,off表示使用老版统计功能,on表示使用新版本统计功能',
      //   type: 'radio',
      //   unit: '',
      //   array: [
      //     {
      //       value: 'off',
      //       label: '使用老版统计功能'
      //     },
      //     {
      //       value: 'on',
      //       label: '使用新版本统计功能'
      //     }
      //   ]
      // },
      {
        name: 'cdn服务器地址',
        mission: '*以//开头',
        configKey: 'RESOURCE_CDN_URL',
        configValue: '//lotv2cdn.bgbet3.com/cp',
        configDesc: 'cdn服务器地址，以//开头.',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '资源服务远程访问URL地址',
        mission: '*该地址必须以 / 符号结束',
        configKey: 'RESOURCE_REMOTE_URL',
        configValue: 'http://10.28.177.202:9231/lottery-resource/rest/',
        configDesc: '资源服务远程访问URL地址,该地址必须以 / 符号结束',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '交互下注的彩种id和名称',
        mission: '*彩种与编号以|分隔，彩种之间以,分隔',
        configKey: 'ROOM_LOTTERY_INFO',
        configValue: '1561|北京幸运28,1562|加拿大幸运28',
        configDesc: '交互下注的彩种id和名称',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '服务器设置的时区',
        mission: '',
        configKey: 'SERVER_TIME_ZONE',
        configValue: '2',
        configDesc: '服务器设置的时区 1.北京时区 2.美东时区，没有配置该参数时，系统默认使用北京时区',
        type: 'radio',
        unit: '',
        array: [
          {
            value: '1',
            label: '美东时区'
          },
          {
            value: '2',
            label: '北京时区'
          }]
      }
    ]
  },
  {
    type_config: 'redis配置',
    isShow: true,
    elements: [
      {
        name: 'cloud模块收集',
        mission: '',
        configKey: 'REDIS_CAPTURE_CLOUD',
        configValue: '1',
        configDesc: 'cloud模块收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '采集收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_COLLECTOR',
        configValue: '1',
        configDesc: '采集收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '后台服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_CONSOLE',
        configValue: '1',
        configDesc: '后台服务收集redis的开关 1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '监控服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_MONITOR',
        configValue: '1',
        configDesc: '监控服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '注单服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_ORDER',
        configValue: '1',
        configDesc: '注单服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '交互式下注服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_ORDERSRV',
        configValue: '1',
        configDesc: '交互式下注服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: 'portal模块收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_PORTAL',
        configValue: '1',
        configDesc: 'portal模块收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '报表服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_REPORT',
        configValue: '1',
        configDesc: '报表服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '调度服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_SCHEDULE',
        configValue: '1',
        configDesc: '调度服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '结算服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_SETTLE',
        configValue: '1',
        configDesc: '结算服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '预统计服务收集redis',
        mission: '',
        configKey: 'REDIS_CAPTURE_STATS',
        configValue: '1',
        configDesc: '预统计服务收集redis的开关1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      }
    ]
  },
  {
    type_config: '异常模拟采集(临时)',
    isShow: process.env.NODE_ENV === 'development' ? true : (!!(window.environment && window.environment.toLowerCase() === 'uat')),
    elements: [
      {
        name: '金流入库异常模拟',
        mission: '',
        configKey: 'ANALOG_CASHFLOW_DB_EXCEPTION',
        configValue: '0',
        configDesc: '金流入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '下注入库异常模拟',
        mission: '',
        configKey: 'ANALOG_BET_ORDER_DB_EXCEPTION',
        configValue: '0',
        configDesc: '下注入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '撤单入库异常模拟',
        mission: '',
        configKey: 'ANALOG_REFUND_ORDER_DB_EXCEPTION',
        configValue: '0',
        configDesc: '撤单入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '下注调接口失败时异常模拟',
        mission: '',
        configKey: 'ANALOG_BET_ORDER_CALLAPI_FILD_EXCEPTION',
        configValue: '0',
        configDesc: '下注调接口失败时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '下注调接口成功时异常模拟',
        mission: '',
        configKey: 'ANALOG_BET_ORDER_CALLAPI_SUCC_EXCEPTION',
        configValue: '0',
        configDesc: '下注调接口成功时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '撤单调接口失败时异常模拟',
        mission: '',
        configKey: 'ANALOG_REFUND_ORDER_CALLAPI_FILD_EXCEPTION',
        configValue: '0',
        configDesc: '撤单调接口失败时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '撤单调接口成功时异常模拟',
        mission: '',
        configKey: 'ANALOG_REFUND_ORDER_CALLAPI_SUCC_EXCEPTION',
        configValue: '0',
        configDesc: '撤单调接口成功时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '派彩时金流入库异常模拟',
        mission: '',
        configKey: 'ANALOG_PAYOUT_CASHFLOW_DB_EXCEPTION',
        configValue: '0',
        configDesc: '派彩时金流入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '派彩调用接口失败时异常模拟',
        mission: '',
        configKey: 'ANALOG_PAYOUT_CALLAPI_FILD_EXCEPTION',
        configValue: '0',
        configDesc: '派彩调用接口失败时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '派彩调用接口成功时异常模拟',
        mission: '',
        configKey: 'ANALOG_PAYOUT_CALLAPI_SUCC_EXCEPTION',
        configValue: '0',
        configDesc: '派彩调用接口成功时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '派彩时注单更新入库异常模拟',
        mission: '',
        configKey: 'ANALOG_PAYOUT_ORDER_DB_EXCEPTION',
        configValue: '0',
        configDesc: '派彩时注单更新入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '代理返点派彩时金流入库异常模拟',
        mission: '',
        configKey: 'ANALOG_AGENTREBATE_PAYOUT_CASHFLOW_DB_EXCEPTION',
        configValue: '0',
        configDesc: '代理返点派彩时金流入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '代理返点派彩时注单更新入库异常模拟',
        mission: '',
        configKey: 'ANALOG_AGENTREBATE_PAYOUT_ORDER_DB_EXCEPTION',
        configValue: '0',
        configDesc: '代理返点派彩时注单更新入库异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '代理返点派彩调用接口失败时异常模拟',
        mission: '',
        configKey: 'ANALOG_AGENTREBATE_PAYOUT_CALLAPI_FILD_EXCEPTION',
        configValue: '0',
        configDesc: '代理返点派彩调用接口失败时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      },
      {
        name: '代理返点派彩调用接口成功时异常模拟',
        mission: '',
        configKey: 'ANALOG_AGENTREBATE_PAYOUT_CALLAPI_SUCC_EXCEPTION',
        configValue: '0',
        configDesc: '代理返点派彩调用接口成功时异常模拟,1:表示打开 0:表示关闭',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: '1',
            label: '开启'
          },
          off: {
            value: '0',
            label: '关闭'
          }
        }
      }
    ]
  },
  {
    type_config: '其他配置',
    isShow: true,
    elements: [
      // {
      //   name: '自开彩服务HTTP访问地址',
      //   mission: '',
      //   configKey: 'SELF_LOTTERY_SERVER_URL1',
      //   configValue: 'http://10.172.4.242:19007/pre_settlement',
      //   configDesc: '自开彩服务HTTP访问地址',
      //   type: 'input',
      //   unit: '',
      //   array: []
      // },
      {
        name: '可疑注单收集开关',
        mission: '*示例:code|name|状态,状态栏0表示禁用,1表示启用',
        configKey: 'ORDER_DOUBT_ERROR_CODE',
        configValue: '2407|批量资金流水入库操作中前后金额不一致|1,2408|批量资金流水入库操作中提交记录数与操作成功记录数不一致|1,2417|资金存款申请金额需要为正数|1,2474|金流操作的bizId已存在|1,2504|余额不足|1,2512|注单已存在(注单ID已存在)|1',
        configDesc: '可疑注单错误编码',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '连续点击次数',
        mission: '',
        configKey: 'ACCESS_LIMIT_COUNT',
        configValue: '2',
        configDesc: '连续点击次数为3',
        type: 'input',
        unit: '次',
        array: []
      },
      {
        name: '限制频率',
        mission: '',
        configKey: 'ACCESS_LIMIT_FREQ',
        configValue: '3',
        configDesc: '限制频率，1:秒 2:分钟 3:小时 4:天，未匹配到该配置时默认为秒',
        type: 'radio',
        unit: '',
        array: [
          {
            value: '1',
            label: '秒'
          },
          {
            value: '2',
            label: '分钟'
          },
          {
            value: '3',
            label: '小时'
          },
          {
            value: '4',
            label: '天'
          }
        ]
      },
      {
        name: '访问次数限制开关',
        mission: '',
        configKey: 'ACCESS_LIMIT_OPEN',
        configValue: 'false',
        configDesc: '访问次数限制开关on:表示打开限制 off:表示关闭限制',
        type: 'switch',
        unit: '',
        array: {
          on: {
            value: 'on',
            label: '开启'
          },
          off: {
            value: 'off',
            label: '关闭'
          }
        }
      },
      {
        name: '序列号生成服务器的访问地址,如订单号等通过该地址获得',
        mission: '*多个地址以,分隔',
        configKey: 'SERIAL_NUMBER_URL',
        configValue: 'http://120.77.148.64:8080/',
        configDesc: '序列号生成服务器的访问地址,如订单号等通过该地址获得,多个地址以逗号分隔',
        type: 'input',
        unit: '',
        array: []
      },
      {
        name: '冷热走势统计期数',
        mission: '默认120期,如果大于该值,业务中将使用默认值',
        configKey: 'HOT_COLD_ISSUE_PERIODS',
        configValue: '120',
        configDesc: '冷热走势默认获取120期计算',
        type: 'input',
        unit: '期',
        array: []
      }
    ]
  }
]
