const openTypes = [
  {
    label: '随机开奖',
    value: 1
  },
  {
    label: '杀率控制',
    value: 2
  },
  {
    label: '预设结果',
    value: 3
  }
]

function openTypeFilter (type) {
  const openType = openTypes.find((openType) => openType.value === type)
  return openType ? openType.label : ''
}

export {
  openTypes,
  openTypeFilter
}
