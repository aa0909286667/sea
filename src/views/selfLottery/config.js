import { getAjaxData } from 'api/common'
import { ERR_OK } from 'api/config'

async function getSpSelfLottery (flag) {
  let arr = []
  const res = await getAjaxData({ dataType: 23 })
  if (res.data.code === ERR_OK) {
    if (flag) {
      arr = res.data.data.items
    } else {
      res.data.data.items.forEach((m) => {
        arr.push(m.id)
      })
    }
  }
  return arr
}

export const spSelfLottery = async () => await getSpSelfLottery(true)

async function getSysSelfLottery (flag) {
  let arr = []
  const res = await getAjaxData({ dataType: 24 })
  if (res.data.code === ERR_OK) {
    if (flag) {
      arr = res.data.data.items
    } else {
      res.data.data.items.forEach((m) => {
        arr.push(m.id)
      })
    }
  }
  return arr
}

export const sysSelfLottery = async () => await getSysSelfLottery(true)

async function getAllSelfLottery (flag) {
  let arr = []
  const res = await getAjaxData({ dataType: 25 })
  if (res.data.code === ERR_OK) {
    if (flag) {
      arr = res.data.data.items
    } else {
      res.data.data.items.forEach((m) => {
        arr.push(m.id)
      })
    }
  }
  return arr
}

export async function getSelfControl (v) {
  const arr = await getSpSelfLottery()
  return arr.indexOf(v) > -1
}

export async function getSelfGlobalControl (v) {
  const arr = await getSysSelfLottery()
  return arr.indexOf(v) > -1
}

export async function selfLotterys () {
  return await getAllSelfLottery(true)
}

export function typeFilter (type) {
  const typeMap = {
    1: '随机',
    2: '杀率控制',
    // 3: '混合'
    3: '预设'
  }
  return typeMap[type]
}
