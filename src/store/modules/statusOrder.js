const SET_SPCODE = 'SET_SPCODE'
const SET_LOTTERYID = 'SET_LOTTERYID'
const SET_ISSUE = 'SET_ISSUE'
const SET_PLAYTYPEIDS = 'SET_PLAYTYPEIDS'
const SET_CASHDATA = 'SET_CASHDATA'
const SET_TABMODENAME = 'SET_TABMODENAME'

const app = {
  state: {
    spCode: '',
    lotteryId: '',
    issue: '',
    playTypeIds: '',
    cashData: [],
    tabModeName: ''
  },
  mutations: {
    [SET_ISSUE]: (state, issue) => {
      state.issue = issue
    },
    [SET_TABMODENAME]: (state, name) => {
      state.tabModeName = name
    },
    [SET_SPCODE]: (state, spCode) => {
      state.spCode = spCode
    },
    [SET_LOTTERYID]: (state, id) => {
      state.lotteryId = id
    },
    [SET_PLAYTYPEIDS]: (state, ids) => {
      state.playTypeIds = ids
    },
    [SET_CASHDATA]: (state, data) => {
      state.cashData = data
    }
  },
  actions: {
    setData: ({ commit }, data) => {
      commit(SET_CASHDATA, data)
    },
    setSpCode: ({ commit }, code) => {
      commit(SET_SPCODE, code)
    },
    setTabModeName: ({ commit }, name) => {
      commit(SET_TABMODENAME, name)
    },
    setPlayTypeIds: ({ commit }, ids) => {
      commit(SET_PLAYTYPEIDS, ids)
    },
    setIssue: ({ commit }, issue) => {
      commit(SET_ISSUE, issue)
    },
    setLotteryId: ({ commit }, id) => {
      commit(SET_LOTTERYID, id)
    },
    resetObj: ({ commit }) => {
      commit(SET_SPCODE, '')
      commit(SET_LOTTERYID, '')
      commit(SET_CASHDATA, [])
      commit(SET_TABMODENAME, '')
      commit(SET_PLAYTYPEIDS, '')
      commit(SET_ISSUE, '')
    }
  }
}
export default app
