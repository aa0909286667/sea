import { ERR_OK } from 'api/config'
import {
  GetCurrencyMonitorList,
  UpdateCurrencyMonitorAmount,
  UpdateCurrencyMonitorStatus
} from 'api/system'

const state = {
  result: [],
  tableIsLoading: false
}

const getters = {
  getResult (state) {
    return state.result
  },
  getTableLoading (state) {
    return state.tableIsLoading
  }
}

const mutations = {
  SET_RESULT (state, payload) {
    state.result = payload
  },
  SET_STATUS (state, payload) {
    console.log('###SET STATUS: ', payload)
    state.result[payload.index].status = payload.status
  },
  SET_TABLE_LOADING (state, payload) {
    state.tableIsLoading = payload
  }
}

const actions = {
  async getCurrencyMonitorList (context, payload) {
    context.commit('SET_TABLE_LOADING', true)
    console.log('####ACTIONS Get currency monitor list POST: ', payload)
    const res = await GetCurrencyMonitorList(payload)
    if (res.data.code === ERR_OK) {
      console.log('####ACTIONS Get currency monitor list: ', res)
      context.commit('SET_TABLE_LOADING', false)
      context.commit('SET_RESULT', res.data.data)
    }
  },

  async updateCurrencyMonitorAmount (context, payload) {
    console.log('####ACTIONS Update currency monitor amount POST: ', context)
    const res = await UpdateCurrencyMonitorAmount(payload)
    if (res.data.code === ERR_OK) {
      console.log('####ACTIONS Get currency monitor amount: ', res)
      context.dispatch('getCurrencyMonitorList', {})
      return res.data.code === ERR_OK
    }
  },

  async updateCurrencyMonitorStatus (context, payload) {
    console.log('####ACTIONS Get currency monitor update status POST: ', context)
    const res = await UpdateCurrencyMonitorStatus(payload)
    if (res.data.code === ERR_OK) {
      console.log('####ACTIONS Get currency monitor update status: ', res)
      context.dispatch('getCurrencyMonitorList', {})
      return res.data.code === ERR_OK
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
