/**
 * Created by Xiaowu on 2019/4/10.
 */
const state = {
  info: {},
  show: false,
  spCode: '',
  loginId: ''
}
const mutations = {
  MEMBER_CASHINFO: (state, obj) => {
    state.info = obj
  },
  MEMBER_CASHINFO_SHOW: (state) => {
    state.show = false
  },
  MEMBER_CASHINFO_SPCODE: (state, spCode) => {
    state.spCode = spCode
  },
  MEMBER_CASHINFO_LOGINID: (state, loginId) => {
    state.loginId = loginId
  }
}
const actions = {
  setHide: ({ commit }) => {
    commit('MEMBER_CASHINFO_SHOW')
  },
  setSpCode: ({ commit }, spCode) => {
    commit('MEMBER_CASHINFO_SPCODE', spCode)
  },
  setLoginID: ({ commit }, loginId) => {
    commit('MEMBER_CASHINFO_LOGINID', loginId)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
