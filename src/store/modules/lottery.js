/**
 * Created by Xiaowu on 2019/4/10.
 */
const state = {
  group: {},
  show: false
}
const mutations = {
  LOTTERY_GROUP: (state, obj) => {
    state.group = obj
  },
  LOTTERY_SHOW: (state) => {
    state.show = false
  }
}
const actions = {
  setHide: ({ commit }) => {
    commit('LOTTERY_SHOW')
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
