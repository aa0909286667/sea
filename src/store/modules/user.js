import { loginByAccount, logout } from 'api/login'
import { ERR_OK } from 'api/config'
import * as storage from 'utils/storage'
import Message from 'core/packages/message/'
import md5 from 'blueimp-md5'
import { resetRouter } from '@/router'

const state = {
  userinfo: storage.getUserInfo() || {},
  token: storage.getToken() || '',
  permission: storage.getPermissionInfo() || [],
  role: storage.getUserRole() || '',
  noPermissionList: storage.getNoPermissionInfo() || []
  // captchaKey: parseInt(Math.random() * 149545454),
  // captchaTime: Date.now(),
  // captchaUrl: ''
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ROLE: (state, role) => {
    state.role = role
  },
  // SET_CAPTCHAKEY: (state, key) => {
  //   state.captchaKey = key
  // },
  // SET_CAPTCHATIME: (state, time) => {
  //   state.captchaTime = time
  // },
  SET_USERINFO: (state, obj) => {
    // storage.setUserInfo(obj)
    state.userinfo = obj
  },
  SET_PERMISSION: (state, arr) => {
    // storage.setPermissionInfo(arr)
    state.permission = arr
  },
  SET_MENUS: (state, arr) => {
    state.menus = arr
  },
  SET_NOPERMISSIONLIST: (state, arr) => {
    // storage.setNoPermissionInfo(arr)
    state.noPermissionList = arr
  }
  // SET_CAPTCHAURL: (state, url) => {
  //   state.captchaUrl = url
  // }
}

const actions = {
  // 账号登录
  LoginByAccount (options, userInfo) {
    // console.log(options)
    const { commit, dispatch } = options
    return new Promise((resolve, reject) => {
      userInfo.password = md5(userInfo.password)
      loginByAccount(userInfo).then((response) => {
        if (ERR_OK === response.data.code) {
          // console.log(response.data)
          storage.setToken(response.data.data.userInfo.token)
          storage.setPermissionInfo(JSON.stringify(response.data.data.permission))
          storage.setUserInfo(response.data.data.userInfo)
          storage.setRole(response.data.data.role)
          commit('SET_TOKEN', response.data.data.userInfo.token)
          commit('SET_ROLE', response.data.data.role)
          commit('SET_USERINFO', response.data.data.userInfo)
          commit('SET_PERMISSION', response.data.data.permission)
          dispatch('tagsView/delAllViews', null, { root: true })
          Message({
            type: 'success',
            message: '登录成功,正在跳转，请稍候...'
          })
          resolve()
          // } else {
          //   commit('SET_CAPTCHAKEY', parseInt(Math.random() * 149545454))
          //   commit('SET_CAPTCHATIME', Date.now())
        }
      }).catch((error) => {
        reject(error)
      })
    })
  },
  // 前端 登出
  async FedLogOut ({ commit }) {
    // return new Promise((resolve, reject) => {
    //   if (storage.getToken()) {
    //     logout(storage.getToken()).then(() => {
    //       storage.removeAll()
    //       commit('SET_TOKEN', '')
    //       commit('SET_ROLE', '')
    //       commit('SET_USERINFO', {})
    //       commit('SET_PERMISSION', [])
    //       resolve()
    //       // window.location.reload()
    //     }).catch((error) => {
    //       reject(error)
    //     })
    //   } else {
    //     storage.removeAll()
    //     commit('SET_TOKEN', '')
    //     commit('SET_ROLE', '')
    //     commit('SET_USERINFO', {})
    //     commit('SET_PERMISSION', [])
    //     resolve()
    //     // window.location.reload()
    //   }
    // })
    if (storage.getToken()) {
      await logout(storage.getToken())
      storage.removeAll()
    }
    commit('SET_TOKEN', '')
    commit('SET_ROLE', '')
    commit('SET_USERINFO', {})
    commit('SET_PERMISSION', [])
    await resetRouter()
  },
  SetNoPermissionList ({ commit }, Array) {
    commit('SET_NOPERMISSIONLIST', Array)
  },
  // resetCode ({ commit }) {
  //   commit('SET_CAPTCHAKEY', parseInt(Math.random() * 149545454))
  //   commit('SET_CAPTCHATIME', Date.now())
  // },
  resetToken ({ commit }) {
    return new Promise((resolve) => {
      storage.removeAll()
      commit('SET_TOKEN', '')
      commit('SET_ROLE', '')
      commit('SET_USERINFO', {})
      commit('SET_PERMISSION', [])
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
