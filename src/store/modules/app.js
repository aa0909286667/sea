import Cookies from 'js-cookie'
import { getLanguage } from 'lang/index'

const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  device: 'desktop',
  language: getLanguage(),
  size: Cookies.get('size') || 'mini',
  qrHash: 0,
  redirect: Cookies.get('redirectUrl') || '/'
}

const mutations = {
  SET_REDIRECT: (state, url) => {
    state.redirect = url
    Cookies.set('redirectUrl', url)
  },
  QRHASHKEY: (state, num) => {
    state.qrHash += num
  },
  TOGGLE_SIDEBAR: (state) => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_LANGUAGE: (state, language) => {
    state.language = language
    Cookies.set('language', language)
  },
  SET_SIZE: (state, size) => {
    state.size = size
    Cookies.set('size', size)
  },
  TOGGLE_THEME: (state, theme) => {
    state.theme = theme
  }
}

const actions = {
  setRedirect ({ commit }, url) {
    commit('SET_REDIRECT', url)
  },
  toggleSideBar ({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  IncreaseQrHashKey ({ commit }, num) {
    commit('QRHASHKEY', num)
  },
  closeSideBar ({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice ({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setLanguage ({ commit }, language) {
    commit('SET_LANGUAGE', language)
  },
  setSize ({ commit }, size) {
    commit('SET_SIZE', size)
  },
  setTheme ({ commit }, theme) {
    commit('TOGGLE_THEME', theme)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
