import Message from 'core/packages/message/'
import { ERR_OK } from 'api/config'
import * as API from 'api/spltpl/'
import { getNoPermissionInfo } from 'utils/storage'
import { console } from 'vuedraggable/src/util/helper'

const hasPermissionOrNot = (v) => {
  const PermissionList = getNoPermissionInfo()
  return !!(PermissionList && PermissionList.length && PermissionList.includes(v))
}
const LANG = {
  succ: '设置成功',
  info: '提交成功，但数据没有变化'
}

const state = {
  showPreview: true,
  showAgent: false,
  spCode: '', // 平台商spCode
  rowId: '', // 盘口Id
  tableData: [], // 表格数据
  allData: [], // 表格数据
  lotteryTypeId: '',
  lotteryId: '0',
  playGroupId: '',
  cmd: ''
  // lotteryTypeList: [] // 已开通彩种类型列表
}
const mutations = {
  SPTPL_SHOW_AGENT: (state, status) => {
    state.showAgent = status
  },
  SPTPL_SHOW_PREVIEW: (state) => {
    state.showPreview = true
  },
  SPTPL_HIDE_PREVIEW: (state) => {
    state.showPreview = false
  },
  SPTPL_PLAYGROUPID: (state, id) => {
    state.playGroupId = id
  },
  SPTPL_LOTTERYTYPEID: (state, id) => {
    state.lotteryTypeId = id
  },
  SPTPL_SPCODE: (state, code) => {
    state.spCode = code
  },
  SPTPL_ROWID: (state, id) => {
    state.rowId = id
  },
  SPTPL_SAVESET: (state) => {
    console.log('saveLen ', state.tableData.length)
  },
  SPTPL_SET_TABLEDATA: (state, data) => {
    state.tableData = data
  },
  SPTPL_SET_ALLDATA: (state, data) => {
    state.allData = data
  },
  SPTPL_SET_LOTTERY_TYPE_LIST: (state, data) => {
    state.lotteryTypeList = data
  },
  SPTPL_SET_LOTTERYID: (state, id) => {
    state.lotteryId = id
  },
  SPTPL_SET_CMD: (state, cmd) => {
    state.cmd = cmd
  }
}
const actions = {
  spTplShowAgent: ({ commit }, status) => {
    commit('SPTPL_SHOW_AGENT', status)
  },
  spTplShowPreview: ({ commit }) => {
    commit('SPTPL_SHOW_PREVIEW')
  },
  spTplHidePreview: ({ commit }) => {
    commit('SPTPL_HIDE_PREVIEW')
  },
  spTplSpCode: ({ commit }, code) => {
    commit('SPTPL_SPCODE', code)
  },
  spTplRowId: ({ commit }, id) => {
    commit('SPTPL_ROWID', id)
  },
  spTplSaveSet2: ({ commit, state }, Array) => {
    let CMD = 'SpTplPlaySetUp'
    if (state.lotteryId !== '0') CMD = 'SpTplLotteryPlaySetUp'
    if (hasPermissionOrNot(CMD)) {
      Message({
        type: 'error',
        message: '没有权限进行保存操作'
      })
      return
    }
    const tempData = []
    const serializeData = Array
    const upPrizeArr = []
    // let n = 0
    for (let i = 0; i < serializeData.length; i++) {
      if (serializeData[i].isEditRow) {
        // n++
        const node = {}
        // node.minPrize = Math.round(parseFloat(serializeData[i].maxPrize) * 1000 * 1000) / 1000
        node.minPrize = serializeData[i].minPrize
        node.maxPrize = Math.round(parseFloat(serializeData[i].maxPrize) * 1000 * 1000) / 1000
        node.itemMaxBet = Math.round(parseFloat(serializeData[i].itemMaxBet) * 100 * 100) / 100
        node.eachMaxBet = Math.round(parseFloat(serializeData[i].eachMaxBet) * 100 * 100) / 100
        node.eachMinBet = Math.round(parseFloat(serializeData[i].eachMinBet) * 100 * 100) / 100
        node.eachMaxCount = Math.round(parseFloat(serializeData[i].eachMaxCount) * 100) / 100
        node.itemMaxCount = Math.round(parseFloat(serializeData[i].itemMaxCount) * 100) / 100
        node.id = serializeData[i].id
        node.playId = serializeData[i].playId
        node.status = Number(serializeData[i].status)
        node.isEdit = 1
        // 记录日志
        // 格式:"upPrize":"101131014[原:898200/998000,新:898201/998001],101131015[原:898200/998000,新:898201/998001]"
        if (node.maxPrize !== serializeData[i].maxPrize__) {
          upPrizeArr.push(`${node.playId}[原:${node.minPrize}/${serializeData[i].maxPrize__},新:${node.minPrize}/${node.maxPrize}]`)
        }
        if (serializeData[i].childList) {
          const { childList } = serializeData[i]
          for (let j = 0; j < childList.length; j++) {
            const nodeChild = {}
            // nodeChild.minPrize = Math.round(parseFloat(childList[j].minPrize) * 1000 * 1000) / 1000
            nodeChild.minPrize = childList[j].minPrize
            nodeChild.maxPrize = Math.round(parseFloat(childList[j].maxPrize) * 1000 * 1000) / 1000
            nodeChild.itemMaxBet = node.itemMaxBet
            nodeChild.eachMaxBet = node.eachMaxBet
            nodeChild.eachMinBet = node.eachMinBet
            nodeChild.itemMaxCount = node.itemMaxCount
            nodeChild.eachMaxCount = node.eachMaxCount
            nodeChild.id = childList[j].id
            nodeChild.playId = childList[j].playId
            nodeChild.status = Number(childList[j].status)
            if (nodeChild.maxPrize !== childList[j].maxPrize__) {
              upPrizeArr.push(`${nodeChild.playId}[原:${nodeChild.minPrize}/${childList[j].maxPrize__},新:${nodeChild.minPrize}/${nodeChild.maxPrize}]`)
            }
            if (Number(nodeChild.status) === 1) node.status = 1
            if (serializeData[i].isExtenalEdit) {
              nodeChild.isEdit = 1
            } else {
              if (childList[j].isEditRow) {
                nodeChild.isEdit = 1
                node.isEdit = 1
              }
            }
            tempData.push(nodeChild)
          }
        }
        tempData.push(node)
      }
    }
    if (!tempData.length) {
      Message({
        type: 'success',
        message: LANG.info,
        duration: 2000
      })
      return
    }
    const query = {}
    query.spCode = state.spCode
    query.spTplId = state.rowId
    query.upPrize = upPrizeArr.join(',')
    if (state.lotteryId !== '0') {
      query.lotteryId = state.lotteryId
    } else {
      query.playGroupId = state.playGroupId
    }
    query.spTplPlaySetList = JSON.stringify(tempData)
    return new Promise((resolve, reject) => {
      API[`post${CMD}`](query).then((res) => {
        const { code } = res.data
        if (code === ERR_OK) {
          Message({
            type: 'success',
            message: `${tempData.length}条数据${LANG.succ}`,
            duration: 2000
          })
          resolve()
        } else {
          reject(res)
        }
      })
    }).catch((err) => {
      console.log(err)
    })
  },
  spTplSaveSet: ({ commit, state }) => {
    commit('SPTPL_SAVESET')
    // if (state.tabIndex === 0) return
    if (!state.tableData.length) return
    const query = {}
    const filedCon = { filed: '', subField: [] }
    let iFace = ''
    let CMD = ''
    // todo 待优化 改为递归赋值 ↓
    // 匹配当前tab上传接口
    switch (state.cmd) {
      // case 0:
      //   iFace = 'postSpTplRebateSetUp'
      //   filedCon.filed = 'spTplRebateSetList'
      //   filedCon.subField = ['id', 'lotteryTypeRate', 'rebateRate']
      //   CMD = 'SpTplRebateSetUp'
      //   break
      case 'SpTplLotterySetList':
        iFace = 'postSpTplLotterySetUp'
        filedCon.filed = 'spTplLotterySetList'
        filedCon.subField = ['id', 'spTplId', 'lotteryTypeId', 'lotteryId', 'status']
        CMD = 'SpTplLotterySetUp'
        break
      case 'SpTplPlayList':
        iFace = 'postSpTplPlaySetUp'
        filedCon.filed = 'spTplPlaySetList'
        // filedCon.subField = ['id', 'playId', 'isExtenalEdit', 'minPrize', 'maxPrize', 'itemMaxBet', 'eachMaxBet', 'eachMinBet', 'status', 'isEditRow', 'childList']
        filedCon.subField = ['id', 'playId', 'isExtenalEdit', 'maxPrize', 'itemMaxBet', 'eachMaxBet', 'eachMinBet', 'eachMaxCount', 'itemMaxCount', 'status', 'isEditRow', 'childList', 'minPrize_', 'maxPrize__', 'minPrize']
        CMD = 'SpTplPlaySetUp'
        break
      case 'SpTplLotteryHotList':
        iFace = 'postSpTplHotLotterySetUp'
        filedCon.filed = 'spTplHotLotterySetList'
        filedCon.subField = ['id', 'hotSort', 'hot', 'hotDesc']
        CMD = 'SpTplHotLotterySetUp'
        break
      // case 4:
      //   iFace = 'postSpTplRecommendLotterySetUp'
      //   filedCon.filed = 'items'
      //   filedCon.subField = ['id', 'recommend', 'recSort']
      //   CMD = 'SpTplRecommendLotterySetUp'
      //   break
      default:
        console.log('未匹配当前tab上传接口')
    }
    if (hasPermissionOrNot(CMD)) {
      Message({
        type: 'error',
        message: '没有权限进行保存操作'
      })
      return
    }
    // 根据不同tab下的配置 从tableData里取需要上传给接口的字段
    const serializeData = []
    // let regBoolean = false
    // let lenText = ''
    for (const item of state.tableData) {
      const obj = {}
      for (const v of filedCon.subField) {
        // 上传前还原布尔值为Number
        if (v === 'status' || v === 'hot' || v === 'recommend') {
          obj[v] = Number(item[v])
          // } else if (v === 'rebateRate' || v === 'eachMinBet' || v === 'eachMaxBet' || v === 'itemMaxBet' || v === 'maxPrize') {
          //   const re = (v === 'maxPrize') ? /^\d+(\.\d{0,3})?$/ : /^\d+(\.\d{0,2})?$/
          //   lenText = (v === 'maxPrize') ? '最高精确到三位小数' : '最高精确到两位小数'
          //   console.log(item[v])
          //   if (!re.test(item[v])) {
          //     regBoolean = true
          //     break
          //   } else {
          //     obj[v] = item[v]
          //   }
        } else {
          obj[v] = item[v]
        }
      }
      serializeData.push(obj)
    }
    // if (regBoolean) {
    //   Message({
    //     type: 'error',
    //     message: `输入的数值有误，${lenText}`
    //   })
    //   return
    // }
    // let flag = false
    // let bigRebate = false
    // if (state.tabIndex === 0) {
    //   for (let i = 0; i < serializeData.length; i++) {
    //     if (serializeData[i].rebateRate < 0) {
    //       flag = true
    //       break
    //     }
    //     serializeData[i].rebateRate = Math.round(parseFloat(serializeData[i].rebateRate) * 100 * 100) / 100
    //     if (serializeData[i].rebateRate > 999999) {
    //       bigRebate = true
    //       break
    //     }
    //     if (serializeData[i].rebateRate > 999999) {
    //       bigRebate = true
    //       break
    //     }
    //     serializeData[i].lotteryTypeRate = Math.round(parseFloat(serializeData[i].lotteryTypeRate) * 100 * 100) / 100
    //   }
    // }
    let hotSort = false
    let bigSort = false
    if (state.cmd === 'SpTplLotteryHotList') {
      for (let i = 0; i < serializeData.length; i++) {
        if (serializeData[i].hotSort < 0) {
          hotSort = true
          break
        }
        if (serializeData[i].hotSort > 10000) {
          bigSort = true
          break
        }
      }
      if (hotSort) {
        Message({
          type: 'error',
          message: '排序不能为负值'
        })
        return
      }
      if (bigSort) {
        Message({
          type: 'error',
          message: '排序数值过大'
        })
        return
      }
    }
    let counter = 0
    if (state.cmd === 'SpTplLotteryHotList') {
      for (let i = 0; i < serializeData.length; i++) {
        if (serializeData[i].hot) {
          counter++
        }
      }
      if (counter < 12) {
        Message({
          type: 'error',
          message: '热门推荐彩种不能低于12种'
        })
        return
      }
    }
    // if (flag) {
    //   Message({
    //     type: 'error',
    //     message: '返点不能为负'
    //   })
    //   return
    // }
    // if (bigRebate) {
    //   Message({
    //     type: 'error',
    //     message: '填写的值超出范围'
    //   })
    //   return
    // }
    const tempData = []
    const upPrizeArr = []
    if (state.cmd === 'SpTplPlayList') {
      for (let i = 0; i < serializeData.length; i++) {
        if (serializeData[i].isEditRow) {
          const node = {}
          // node.minPrize = Math.round(parseFloat(serializeData[i].maxPrize) * 1000 * 1000) / 1000
          node.minPrize = serializeData[i].minPrize
          node.maxPrize = Math.round(parseFloat(serializeData[i].maxPrize) * 1000 * 1000) / 1000
          node.itemMaxBet = Math.round(parseFloat(serializeData[i].itemMaxBet) * 100 * 100) / 100
          node.eachMaxBet = Math.round(parseFloat(serializeData[i].eachMaxBet) * 100 * 100) / 100
          node.eachMinBet = Math.round(parseFloat(serializeData[i].eachMinBet) * 100 * 100) / 100
          node.eachMaxCount = Math.round(parseFloat(serializeData[i].eachMaxCount) * 100) / 100
          node.itemMaxCount = Math.round(parseFloat(serializeData[i].itemMaxCount) * 100) / 100
          node.id = serializeData[i].id
          node.playId = serializeData[i].playId
          node.status = Number(serializeData[i].status)
          node.isEdit = 1
          // 记录日志
          // 格式:"upPrize":"101131014[原:898200/998000,新:898201/998001],101131015[原:898200/998000,新:898201/998001]"
          if (node.maxPrize !== serializeData[i].maxPrize__) {
            upPrizeArr.push(`${node.playId}[原:${node.minPrize}/${serializeData[i].maxPrize__},新:${node.minPrize}/${node.maxPrize}]`)
          }
          if (serializeData[i].childList) {
            const { childList } = serializeData[i]
            for (let j = 0; j < childList.length; j++) {
              const nodeChild = {}
              // nodeChild.minPrize = Math.round(parseFloat(childList[j].minPrize) * 1000 * 1000) / 1000
              nodeChild.minPrize = childList[j].minPrize
              nodeChild.maxPrize = Math.round(parseFloat(childList[j].maxPrize) * 1000 * 1000) / 1000
              // nodeChild.itemMaxBet = Math.round(parseFloat(childList[j].itemMaxBet) * 100 * 100) / 100
              // nodeChild.eachMaxBet = Math.round(parseFloat(childList[j].eachMaxBet) * 100 * 100) / 100
              // nodeChild.eachMinBet = Math.round(parseFloat(childList[j].eachMinBet) * 100 * 100) / 100
              nodeChild.itemMaxBet = node.itemMaxBet
              nodeChild.eachMaxBet = node.eachMaxBet
              nodeChild.eachMinBet = node.eachMinBet
              nodeChild.itemMaxCount = node.itemMaxCount
              nodeChild.eachMaxCount = node.eachMaxCount
              nodeChild.id = childList[j].id
              nodeChild.playId = childList[j].playId
              nodeChild.status = Number(childList[j].status)
              if (nodeChild.maxPrize !== childList[j].maxPrize__) {
                upPrizeArr.push(`${nodeChild.playId}[原:${nodeChild.minPrize}/${childList[j].maxPrize__},新:${nodeChild.minPrize}/${nodeChild.maxPrize}]`)
              }
              if (Number(nodeChild.status) === 1) node.status = 1
              if (serializeData[i].isExtenalEdit) {
                nodeChild.isEdit = 1
              } else if (childList[j].isEditRow) {
                nodeChild.isEdit = 1
                node.isEdit = 1
              }
              tempData.push(nodeChild)
            }
          }
          tempData.push(node)
        }
      }
      if (!tempData.length) {
        Message({
          type: 'success',
          message: LANG.info,
          duration: 2000
        })
        return
      }
    }
    if (state.cmd === 'SpTplPlayList') {
      query[filedCon.filed] = JSON.stringify(tempData)
      query.upPrize = upPrizeArr.join(',')
    } else {
      query[filedCon.filed] = JSON.stringify(serializeData)
    }
    query.spCode = state.spCode
    query.spTplId = state.rowId
    // query.playGroupId = state.playGroupId
    if (state.lotteryId !== '0' && state.cmd === 'SpTplPlayList') {
      iFace = 'postSpTplLotteryPlaySetUp'
      query.lotteryId = state.lotteryId
      // query.playGroupId = state.playGroupId
    }
    if (state.lotteryId === '0' && state.cmd === 'SpTplPlayList') {
      iFace = 'postSpTplPlaySetUp'
      query.playGroupId = state.playGroupId
    }
    return new Promise((resolve, reject) => {
      API[iFace](query).then((res) => {
        const { code } = res.data
        if (code === ERR_OK) {
          Message({
            type: 'success',
            message: LANG.succ,
            duration: 2000
          })
          resolve()
        } else {
          reject(res)
        }
      }).catch((err) => {
        console.log(err)
      })
    })
  },
  spTplSetTableData: ({ commit }, obj) => {
    // obj 在vue文件传 index, data两个参数
    commit('SPTPL_SET_CMD', obj.cmd)
    commit('SPTPL_SET_TABLEDATA', obj.data)
  },
  spTplSetAllData: ({ commit }, data) => {
    commit('SPTPL_SET_ALLDATA', data)
  },
  spTplSetLotteryTypeList: ({ commit }, data) => {
    commit('SPTPL_SET_LOTTERY_TYPE_LIST', data)
  },
  spTplSetLotteryId: ({ commit }, id) => {
    commit('SPTPL_SET_LOTTERYID', id)
  },
  spTplSetLotteryTypeId: ({ commit }, id) => {
    commit('SPTPL_LOTTERYTYPEID', id)
  },
  spTplSetPlayGroupId: ({ commit }, id) => {
    commit('SPTPL_PLAYGROUPID', id)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
