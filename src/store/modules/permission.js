import { constantRouterMap } from 'router'
import { getRoutesStorage, setRoutesStorage } from 'utils/storage'

const state = {
  routers: getRoutesStorage() || constantRouterMap,
  addRouters: []
}

const mutations = {
  SET_ROUTERS: (state, routers) => {
    state.addRouters = routers
    state.routers = routers
  }
}

const actions = {
  GenerateRoutes ({ commit }, data) {
    console.log(data)
    setRoutesStorage(data)
    commit('SET_ROUTERS', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
