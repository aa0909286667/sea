// import defaultSettings from '@/settings'
import Cookies from 'js-cookie'
import variables from 'styles/element-variables.scss'

// const { showSettings, tagsView, fixedHeader, sidebarLogo } = defaultSettings

const state = {
  theme: Cookies.get('theme') || variables.theme,
  showSettings: true,
  tagsView: Cookies.get('tagsView') === 'true' || true,
  fixedHeader: Cookies.get('fixedHeader') === 'true' || true,
  sidebarLogo: Cookies.get('sidebarLogo') === 'true' || true,
  headerTheme: Cookies.get('headerTheme') || 'blue',
  sidebarTheme: Cookies.get('sidebarTheme') || 'dark'
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    if (Object.prototype.hasOwnProperty.call(state, key)) {
      state[key] = value
      Cookies.set(key, value)
    }
  },
  SET_HEADERTHEME: (state, skin) => {
    state.headerTheme = skin
  }
}

const actions = {
  changeSetting ({ commit }, data) {
    commit('CHANGE_SETTING', data)
  },
  setHeaderTheme ({ commit }, skin) {
    commit('SET_HEADERTHEME', skin)
    Cookies.set('headerTheme', skin)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

