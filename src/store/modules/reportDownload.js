/**
 * Created by Xiaowu on 2019/4/10.
 */
const state = {
  downloadlist: [],
  type: ''
}
const mutations = {
  SET_DOWNLOADLIST: (state, arr) => {
    state.downloadlist = arr
  },
  SET_TYPE: (state, id) => {
    state.type = id
  }
}
const actions = {
  setDownloadData ({ commit }, data) {
    commit('SET_DOWNLOADLIST', data)
  },
  setType ({ commit }, id) {
    commit('SET_TYPE', id)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
