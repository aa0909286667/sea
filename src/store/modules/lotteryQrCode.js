/**
 * Created by Xiaowu on 2019/4/10.
 */
import { getQrInfo } from 'utils/storage'
import { postScanQrCodeQuery } from 'api/common'
import { ERR_OK } from 'api/config'
import Message from 'core/packages/message/'

const state = {
  qrText: getQrInfo().qrText || '',
  base64: getQrInfo().qrImgText || '',
  isOpenQrDialog: false,
  showCodeButton: true,
  callOptions: {},
  qrIdentity: '',
  qrErrInfo: ''
}
const mutations = {
  SET_QRTEXT: (state, qrText) => {
    state.qrText = qrText
  },
  SET_BASE64: (state, base64) => {
    state.base64 = base64
  },
  SET_ISOPEN: (state, flag) => {
    state.isOpenQrDialog = flag
  },
  SET_SHOWCODE: (state, flag) => {
    state.showCodeButton = flag
  },
  SET_CALLOPTIONS: (state, options) => {
    state.callOptions = { ...options }
  },
  SET_QRCODEIDENTITY: (state, identity) => {
    state.qrIdentity = identity
  },
  SET_QRERRINFO: (state, info) => {
    state.qrErrInfo = info
  }
}
const actions = {
  getQrText: ({ commit }, obj) => {
    // console.log(obj)
    commit('SET_BASE64', obj.qrImgText)
    commit('SET_QRTEXT', obj.qrText)
  },
  setIsOpen: ({ commit }, flag) => {
    // console.log(1111)
    commit('SET_ISOPEN', flag)
  },
  setShowCode: ({ commit }, flag) => {
    commit('SET_SHOWCODE', flag)
  },
  setCallOptions: (options, obj) => {
    const { commit } = options
    commit('SET_CALLOPTIONS', obj)
  },
  postScanQuery: ({ commit, state }, data) => new Promise((resolve, reject) => {
    postScanQrCodeQuery({ ...data, qrText: state.qrText }).then((res) => {
      const codeType = res.data.code === ERR_OK ? 'success' : 'error'
      Message({
        type: codeType,
        message: res.data.desc
      })
      if (res.data.code === ERR_OK) {
        resolve()
      }
    }).catch((error) => {
      reject(error)
    })
  }),
  setQrIdentity: ({ commit }, qrIdentity) => {
    commit('SET_QRCODEIDENTITY', qrIdentity)
  },
  setQrErrInfo: ({ commit }, info) => {
    commit('SET_QRERRINFO', info)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
