import { getIntercept } from 'api/common'
import { removeAll } from 'utils/storage'

export default {
  props: {},
  data () {
    return {
      mantainTimer: null
    }
  },
  computed: {},
  watch: {},
  methods: {
    getMantainStatus () {
      this.mantainTimer = null
      clearTimeout(this.mantainTimer)
      getIntercept().then((r) => {
        if (r.data.result === 1) {
          removeAll()
          if (this.$route.fullPath !== '/maintenance') this.$router.push('/maintenance')
        } else if (this.$route.fullPath === '/maintenance') {
          this.$router.push(`/login`)
        }
      })
      this.mantainTimer = setTimeout(() => {
        this.getMantainStatus()
      }, 10 * 1000)
    }
  },
  beforeCreate () {},
  created () {
    if (process.env.NODE_ENV === 'production') this.getMantainStatus()
    // const noRedirectUrls = ['/login', '/401', '/404', '/maintenance']
    // console.log(noRedirectUrls)
    // console.log(this.$route)
  },
  beforeMount () {},
  mounted () {},
  beforeUpdate () {},
  updated () {},
  activated () {},
  deactivated () {},
  beforeDestroy () {
    this.mantainTimer = null
    clearTimeout(this.mantainTimer)
  },
  destroyed () {},
  filters: {}
}
