import Vue from 'vue'
import Router from 'vue-router'
import store from 'store'
import { getPermissionInfo, setNoPermissionInfo } from 'utils/storage'

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import system from './modules/system'
import addSpManage from './modules/addSpManage'
import monitor from './modules/monitor'
import logs from './modules/logs'
import reports from './modules/reports'
import issue from './modules/issue'
import selfLottery from './modules/selfLottery'
import openCode from './modules/openCode'
// import busiLogger from './modules/busiLogger'
import memberList from './modules/members'
import orderList from './modules/orderList'
// import cashFlow from './modules/cashFlow'
import riskControl from './modules/riskControl'
import currentReport from './modules/OrderCurrReportList'
import spInfoList from './modules/spInfoList'
// import roomLottery from './modules/roomLottery'
import dataBase from './modules/dataBase'
import settlement from './modules/settlement'

Vue.use(Router)

/**
 * Note: sub-menu only appear when route children.length >= 1
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * isBlank:true                   新建窗口打开
 * meta : {
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
    NoTag:true                    不显示在tags-views上面
    noActive:true                 菜单不高亮
  }
 */

export const constantRouterMap = [
  {
    path: '/login',
    component: () => import('views/login/login'),
    hidden: true
  },
  {
    path: '/maintenance',
    component: () => import('views/maintenance/index'),
    hidden: true
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('views/errorPage/401'),
    hidden: true
  },
  {
    path: '/icons',
    component: Layout,
    hidden: process.env.NODE_ENV !== 'development',
    redirect: '/icons/index',
    children: [
      {
        path: 'index',
        component: () => import('views/icons/index'),
        name: 'Icons',
        meta: { title: 'Icons', icon: 'fas fa-icons', noCache: true }
      }
    ]
  },
  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    hidden: false,
    children: [
      {
        path: 'dashboard',
        component: () => import('views/dashboard/index'),
        name: 'Dashboard',
        meta: {
          title: '首页概览', icon: 'fas fa-home', noCache: true, affix: true
        }
      }
    ]
  },
  {
    path: '/monitor/content',
    name: 'monitorContent',
    hidden: true,
    notag: true,
    component: () => import('views/monitor/OrderMonitor/Content')
  }
]

/**
 * asyncRouterMap
 * 权限菜单
 */
export const asyncRouterMap = [
  dataBase,
  // roomLottery,
  addSpManage,
  spInfoList,
  issue,
  openCode,
  memberList,
  // cashFlow,
  orderList,
  currentReport,
  selfLottery,
  riskControl,
  reports,
  settlement,
  logs,
  // busiLogger,
  monitor,
  system,
  { path: '*', redirect: '/404', hidden: true }
]

function filterAsyncRouter (asyncRouter, menus) {
  // 保存无权限CMD数组
  const btnArr = []
  // 遍历路由筛选
  const accessedRouters = asyncRouter.map((x) => {
    let count = 0
    if (x.meta && x.meta.permission) {
      x.hidden = false
      x.children = x.children.map((y) => {
        if (y.meta && y.meta.onlyMasterCanSee) {
          y.hidden = true
          return y
        }
        const { permission } = y.meta
        for (const pop in permission) {
          const t = menus.indexOf(pop)
          const btns = permission[pop]
          if (btns && btns.length) {
            for (let i = 0; i < btns.length; i++) {
              const s = menus.indexOf(btns[i])
              if (s === -1) btnArr.push(btns[i])
            }
          }
          if (t > -1) {
            y.hidden = false
            count++
          } else {
            y.hidden = true
          }
        }
        return y
      })
      if (count === 0) x.hidden = true
    } else {
      x.hidden = true
    }
    return x
  })
  if (btnArr.length) {
    setNoPermissionInfo(JSON.stringify(btnArr))
    store.dispatch('user/SetNoPermissionList', btnArr)
  }
  return accessedRouters
}

function FilterAdminMenus (menus = []) {
  return menus.filter((m) => !(m.meta && m.meta.onlyMasterCanSee))
}

// console.log(store)

function generateRoutesFromPermission (menus = []) {
  const asyncRouter = asyncRouterMap
  if (store.getters.role === 'master') {
    if (store.getters.userinfo.account !== 'admin') {
      store.dispatch('permission/GenerateRoutes', FilterAdminMenus(asyncRouter))
      return FilterAdminMenus(asyncRouter)
    }
    store.dispatch('permission/GenerateRoutes', asyncRouter)
    return asyncRouter
  }
  store.dispatch('permission/GenerateRoutes', filterAsyncRouter(asyncRouter, menus))
  return filterAsyncRouter(asyncRouter, menus)
}

// export default new Router({
//   // mode: 'history', // 后端支持可开
//   scrollBehavior: () => ({ y: 0 }),
//   routes: constantRouterMap.concat(generateRoutesFromPermission(getPermissionInfo()))
// })
const router = new Router({
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap.concat(generateRoutesFromPermission(getPermissionInfo()))
})

export function resetRouter () {
  router.matcher = new Router({
    scrollBehavior: () => ({
      y: 0
    }),
    routes: constantRouterMap
  }).matcher
}

export default router
