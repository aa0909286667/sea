/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

const system = {
  path: '/system',
  component: Layout,
  name: 'system',
  redirect: '/system/MasterList',
  meta: { permission: '#', title: '系统管理', icon: 'set' },
  children: [
    {
      path: 'MasterList',
      meta: {
        permission: {
          MasterList: [
            'MasterList',
            'MasterCreate',
            'MasterUpStatus',
            'UserRestPassword',
            'AuthRoleBind'
          ]
        },
        title: '操作员管理'
      },
      component: () => import('views/system/masterlist/masterlist'),
      name: 'langList'
    },
    {
      path: 'AuthRoleList',
      meta: {
        permission: { AuthRoleList: ['AuthRoleList', 'AuthRoleCreate', 'AuthPermissionBind', 'AuthRoleUpdate', 'AuthPermissionList'] },
        title: '角色管理'
      },
      component: () => import('views/system/authrole/authrole'),
      name: 'AuthRoleList'
    },
    {
      path: 'langList',
      meta: {
        permission: {
          LangList: [
            'LangList', 'LangUp'
          ]
        },
        title: '多语言管理'
      },
      component: () => import('views/system/langList/langList'),
      name: 'langList'
    },

    // {
    //   path: 'SettleSettingList', component: () => import('views/system/settleManage/settleManage'),
    //   name: 'SettleSettingList',
    //   meta: {
    //     permission: { SettleSettingList: ['SettleSettingList', 'SettleSettingDetail', 'SettleSettingSave'] },
    //     title: '结算配置'
    //   }
    // },
    {
      path: 'SysConfigList',
      component: () => import('views/system/configlist/configlist'),
      name: 'SysConfigList',
      meta: {
        title: '通用配置', permission: { SysConfigList: ['SysConfigList', 'SysConfigUpdate'] }
      }
    },
    // {
    //   path: 'LoggerBusiList', component: () => import('views/system/Operationlog/Operationlog'),
    //   name: 'LoggerBusiList',
    //   meta: {
    //     title: '操作日志',
    //     permission: { LoggerBusiList: ['LoggerBusiList'] }
    //   }
    // },
    {
      path: 'BusiLoggerList',
      meta: {
        permission: { BusiLoggerList: ['BusiLoggerList'] }, title: '业务操作日志'
      },
      component: () => import('views/busiLogger/BusiLoggerList/BusiLoggerList'),
      name: 'BusiLoggerList'
    },
    {
      path: 'BusiLoggerActionList',
      meta: {
        permission: { BusiLoggerActionList: ['BusiLoggerActionList', 'BusiLoggerActionAdd', 'BusiLoggerActionUp', 'BusiLoggerPropertyList', 'BusiLoggerPropertyUp'] },
        title: '操作日志行为设置'
      },
      component: () => import('views/busiLogger/BusiLoggerActionList/BusiLoggerActionList'),
      name: 'BusiLoggerActionList'
    },
    {
      path: 'RefreshRedisCache',
      component: () => import('views/system/cache/index'),
      name: 'RefreshRedisCache',
      meta: {
        permission: { RefreshRedisCache: ['RefreshRedisCache'], '/refreshLocalCache': ['/refreshLocalCache'] },
        title: '缓存管理'
      }
    },
    // {
    //   path: 'PageTypeList',
    //   component: () => import('views/system/PageLink/PageLink'),
    //   name: 'PageTypeList',
    //   meta: {
    //     permission: { PageTypeList: ['PageTypeList', 'PageTypeUpdate'] }, title: '跳转配置'
    //   }
    // },
    {
      path: 'WhiteList',
      component: () => import('views/system/WhiteList/index'),
      name: 'WhiteList',
      meta: {
        permission: { WhiteList: ['WhiteList', 'WhiteListEdit', 'WhiteListSwitch'] }, title: 'IP白名单'
      }
    },
    {
      path: 'CurrencyMonitorList',
      component: () => import('views/system/CurrencyMonitorList/index'),
      name: 'CurrencyMonitorList',
      meta: {
        permission: { CurrencyMonitorList: ['CurrencyMonitorList', 'CurrencyMonitorUpdate', 'CurrencyMonitorUpStatus'] }, title: '币别预警阈值'
      }
    }

  ]
}
export default system
