/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/dataBase',
  component: Layout,
  redirect: '/dataBase/typelist',
  meta: { permission: '#', title: '基础配置', icon: 'decoration_fill' },
  name: 'decoration_fill',
  children: [
    {
      path: 'LotteryTypeList',
      meta: {
        permission: {
          LotteryTypeList: [
            'LotteryTypeList',
            'LotteryTypeUpStatus',
            'LotteryTypeUpDisplaySort'
          ]
        },
        title: '彩种分组'
      },
      component: () => import('views/dataBase/typelist/typelist'),
      name: 'LotteryTypeList'
    },
    {
      path: 'LotteryInfoList',
      meta: {
        permission: {
          LotteryInfoList: [
            'LotteryInfoList',
            'LotteryUpStatus',
            'LotteryUpDisplaySort'
          ]
        },
        title: '彩种设置'
      },
      component: () => import('views/dataBase/infolist/infolist'),
      name: 'LotteryInfoList'
    },
    {
      path: 'LotteryPlayTabList',
      meta: {
        permission: { LotteryPlayTabList: ['LotteryPlayTabList'] },
        title: '玩法分组'
      },
      component: () => import('views/dataBase/LotteryPlayTabList/index'),
      name: 'LotteryPlayTabList'
    },
    {
      path: 'LotteryPlayTypeList',
      meta: {
        permission: {
          LotteryPlayTypeList: [
            'LotteryPlayTypeList',
            'LotteryPlayTypeUpDisplaySort',
            'LotteryPlayTypeUpStatus'
          ]
        },
        title: '玩法分类'
      },
      component: () => import('views/dataBase/LotteryPlayTypeList/index'),
      name: 'LotteryPlayTypeList'
    },
    {
      path: 'LotteryPlayList',
      meta: {
        permission: {
          LotteryPlayList: [
            'LotteryPlayList',
            'LotteryPlayUpStatus',
            'LotteryPlayUpBet',
            'LotteryPlayUpDisplaySort'
          ]
        },
        title: '玩法设置'
      },
      component: () => import('views/dataBase/playlist/playlist'),
      name: 'LotteryPlayList'
    },
    {
      path: 'GameRuleList',
      meta: {
        permission: {
          GameRuleList: ['GameRuleList', 'GameRuleDetail', 'GameRuleSave']
        },
        title: '开奖说明'
      },
      component: () => import('views/dataBase/gameRuleList/index'),
      name: 'GameRuleList'
    }
  ]
}
