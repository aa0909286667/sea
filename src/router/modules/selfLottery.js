/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/selfLottery',
  component: Layout,
  redirect: '/selfLottery/SelfLotteryInfoList',
  name: 'selfLottery',
  meta: {
    permission: '#',
    // onlyMasterCanSee: true,
    title: '自开彩控制',
    icon: 'manage_fill'
  },
  children: [
    {
      path: 'SelfCustomConfList',
      meta: {
        permission: { SelfCustomConfList: ['SelfCustomConfList', 'SelfCustomNameEdit', 'SelfCustomNameSwitch', 'SelfCustomImageEdit', 'SelfCustomSwitch'] },
        title: '厅自开彩自定义配置'
      },
      component: () => import('views/selfLottery/SelfCustomConfList/index'),
      name: 'SelfCustomConfList'
    },
    {
      path: 'SelfLotteryInfoList',
      meta: {
        permission: {
          SelfLotteryInfoList: ['SelfLotteryInfoList', 'SelfLotteryInfo', 'SelfLotteryInfoUp']
        },
        // onlyMasterCanSee: true,
        title: '厅自开彩杀率设置 '
      },
      component: () => import('views/selfLottery/SelfLotteryInfoList/index'),
      name: 'SelfLotteryInfoList '
    },
    {
      path: 'SelfLotteryDetailList',
      meta: {
        permission: {
          SelfLotteryDetailList: ['SelfLotteryDetailList', 'SelfLotteryDetailAdd']
        },
        // onlyMasterCanSee: true,
        title: '预设开奖结果'
      },
      component: () => import('views/selfLottery/SelfLotteryDetailList/index'),
      name: 'SelfLotteryDetailList'
    },
    {
      path: 'SelfLotteryCurrIssueList',
      meta: {
        permission: {
          SelfLotteryCurrIssueList: ['SelfLotteryCurrIssueList', 'SelfLotteryDetailInfo', 'SelfLotteryDetailUp']
        },
        // onlyMasterCanSee: true,
        title: '手动开奖结果'
      },
      component:
        () => import('views/selfLottery/SelfLotteryCurrIssueList/index'),
      name: 'SelfLotteryCurrIssueList'
    },
    {
      path: 'SelfLotteryDetailSearch',
      meta:
        {
          permission: {
            SelfLotteryDetailSearch: ['SelfLotteryDetailSearch']
          },
          // onlyMasterCanSee: true,
          title: '预设结果查询'
        },
      component: () => import('views/selfLottery/SelfLotteryDetailSearch/index'),
      name: 'SelfLotteryDetailSearch'
    },
    {
      path: 'SelfLotteryOpenCodeList',
      meta:
        {
          permission: {
            SelfLotteryOpenCodeList: ['SelfLotteryOpenCodeList']
          },
          // onlyMasterCanSee: true,
          title: '开奖结果 '
        },
      component: () => import('views/selfLottery/SelfLotteryOpenCodeList/index'),
      name: 'SelfLotteryOpenCodeList'
    },
    // {
    //   path: 'orbitList',
    //   meta: {
    //     permission: {
    //       IssueOrbitList: ['IssueOrbitList']
    //     },
    //     onlyMasterCanSee: true
    //   },
    //   component: () => import('views/selfLottery/IssueList/index'),
    //   name: '开奖轨迹'
    // },
    {
      path: 'SelfLotteryOrderInfoList',
      meta:
        {
          permission: {
            SelfLotteryOrderInfoList: ['SelfLotteryOrderInfoList']
          },
          // onlyMasterCanSee: true,
          title: '大额即时注单 '
        },
      component: () => import('views/selfLottery/SelfLotteryOrderInfoList/SelfLotteryOrderInfoList'),
      name: 'SelfLotteryOrderInfoList '
    },
    {
      path: 'SelfSysLotteryReport',
      meta:
        {
          permission: {
            SelfSysLotteryReport: [
              'SelfSysLotteryReport',
              'SelfSysLotteryClean',
              'SelfSysLotterySettingInfo',
              'SelfSysLotterySettingSave'
            ]
          },
          // onlyMasterCanSee: true,
          title: '系统彩杀率设置 '
        },
      component: () => import('views/selfLottery/SelfSysLotteryReport/index'),
      name: 'SelfSysLotteryReport '
    }
  ]
}
