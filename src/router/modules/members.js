/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/members',
  component: Layout,
  redirect: '/members/MemberInfoList',
  name: 'members',
  meta: { permission: '#', title: '会员管理', icon: 'theme' },
  children: [
    {
      path: 'MemberInfoList',
      meta: {
        permission: {
          MemberInfoList: [
            'MemberInfoList',
            'MemberStatus',
            'MemberQuotaLimitCreate',
            'MemberTplBind',
            'MemberCashflowList',
            'MemberDetail'
          ]
        },
        title: '会员管理 '
      },
      component: () => import('views/members/manage/manage'),
      name: 'MemberInfoList '
    },
    {
      path: 'agentinfoList',
      meta: {
        permission: { AgentInfoList: ['AgentInfoList'] },
        title: '代理列表'
      },
      component: () => import('views/members/agentinfoList/agentinfoList'),
      name: 'agentinfoList'
    },
    {
      path: 'MemberQuotaLimitList',
      meta: {
        permission: { MemberQuotaLimitList: ['MemberQuotaLimitList', 'MemberQuotaLimitStatus', 'MemberLoggerMembeList'] },
        title: '会员限额 '
      },
      component: () => import('views/members/quotalimit/quotalimit'),
      name: 'MemberQuotaLimitList'
      // },
      // {
      //   path: 'Logger',
      //   meta: {
      //     permission: { 'MemberLoggerMembeList': ['MemberLoggerMembeList'] }
      //   },
      //   component: MemberLoggerMembeList,
      //   id: 100404,
      //   name: '登录日志'
    }
    // {
    //   path: '/AccountConfiguration',
    //   meta: {
    //     permission: {
    //       RebateConfigList: ['RebateConfigList'],
    //       RebateConfigPresetList: ['RebateConfigPresetList', 'RebateConfigPresetListByInvite', 'RebateConfigPresetBind'],
    //       RebateConfigPresetListByMember: ['RebateConfigPresetListByMember', 'RebateConfigPresetBind']
    //     },
    //     title: '开户配置管理'
    //   },
    //   component: () => import('views/members/RebateConfigList/index'),
    //   name: 'AccountConfiguration'
    // }
  ]
}
