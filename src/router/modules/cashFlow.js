/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/cashFlow',
  component: Layout,
  redirect: '/cashFlow/fund',
  meta: {
    permission: '#',
    title: '金流',
    icon: 'financial_fill'
  },
  name: 'financial_fill',
  noDropdown: true,
  children: [
    {
      path: 'fund',
      meta: {
        permission: { MemberCashflowList: ['MemberCashflowList'] },
        title: '金流管理'
      },
      component: () => import('views/cashFlow/fund'),
      name: 'MemberCashflowList'
    }
  ]
}
