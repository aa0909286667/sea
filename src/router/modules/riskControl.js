/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  redirect: '/riskcontrol/RiskControlList',
  path: '/riskcontrol',
  component: Layout,
  meta: {
    permission: '#',
    title: '风控管理',
    icon: 'table'
  },
  name: 'riskcontrol',
  // noDropdown: true,
  children: [
    {
      path: 'RiskControlList',
      meta: {
        permission: { RiskControlList: ['RiskControlList', 'RiskControlAdd', 'RiskControlEdit', 'RiskControlDel'] },
        title: '风控管理设置'
      },
      component: () => import('views/riskControl/index'),
      name: 'RiskControlList'
    },
    {
      path: 'RiskBeforeOpenList',
      meta: {
        permission: { RiskBeforeOpenList: ['RiskBeforeOpenList', 'RiskBeforeOpenRecovery'] },
        title: '提前开奖风控'
      },
      component: () => import('views/riskControl/RiskBeforeOpenList/index'),
      name: 'RiskBeforeOpenList'
    },
    {
      path: 'maintainLottery',
      meta: {
        permission: { RiskBeforeOpenList: ['LotteryMaintainList', 'LotteryMaintainAdd', 'LotteryMaintainClose	'] },
        title: '彩种维护设置'
      },
      component: () => import('views/riskControl/maintainLottery/index'),
      name: 'LotteryMaintainList'
    }
  ]
}
