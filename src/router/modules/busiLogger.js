/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/busiLogger',
  component: Layout,
  redirect: '/busiLogger/BusiLoggerList',
  meta: { permission: '#', title: '业务日志管理', icon: 'flag_fill' },
  name: 'busiLogger',
  children: [
    {
      path: 'BusiLoggerList',
      meta: {
        permission: { BusiLoggerList: ['BusiLoggerList', 'BusiLoggerModule'] }, title: '业务日志查询'
      },
      component: () => import('views/busiLogger/BusiLoggerList/BusiLoggerList'),
      name: 'BusiLoggerList'
    },
    {
      path: 'BusiLoggerActionList',
      meta: {
        permission: { BusiLoggerActionList: ['BusiLoggerActionList', 'BusiLoggerActionAdd', 'BusiLoggerActionUp', 'BusiLoggerPropertyList', 'BusiLoggerPropertyUp'] },
        title: '日志行为设置'
      },
      component: () => import('views/busiLogger/BusiLoggerList/BusiLoggerList'),
      name: 'BusiLoggerActionList'
    }
  ]
}
