/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/reports',
  component: Layout,
  redirect: '/reports/ReportIssueProfitLossSearch',
  name: 'reports',
  meta: {
    permission: '#',
    title: '报表管理',
    icon: 'EXCEL'
  },
  children: [
    {
      path: 'ReportIssueProfitLossSearch',
      component: () => import('views/reports/ReportIssueProfitLossList/index'),
      name: 'ReportIssueProfitLossSearch ',
      meta: {
        permission: {
          ReportIssueProfitLossSearch: [
            'ReportIssueProfitLossSearch',
            'ReportIssueProfitLossExport'
            // 'ReportDownloadList'
          ]
        },
        title: '单期盈亏报表 '
      }
    },
    // {
    //   path: 'ReportIssueProfitLossSearch', component: () => import('views/reports/ReportIssueProfitLossList/index'),
    //   name: 'ReportIssueProfitLossSearch ',
    //   meta: {
    //     permission: {
    //       ReportIssueProfitLossSearch: [
    //         'ReportIssueProfitLossSearch',
    //         'ReportIssueProfitLossExport'
    //         // 'ReportDownloadList'
    //       ]
    //     }, title: '单期盈亏报表 '
    //   }
    // },
    {
      path: 'ReportUserBetAmountSearch',
      component: () => import('views/reports/ReportUserBetAmountSearch/index'),
      name: 'ReportUserBetAmountSearch',
      meta: {
        permission: {
          ReportUserBetAmountSearch: [
            'ReportUserBetAmountSearch',
            'ReportUserBetAmountExport'
            // 'ReportDownloadList'
          ]
        },
        title: '用户投注额报表 '
      }
    },
    {
      path: 'ReportLotteryBetAmountSearch',
      meta: {
        permission: { ReportLotteryBetAmountSearch: ['ReportLotteryBetAmountSearch', 'ReportLotteryBetAmountExport'] },
        title: '彩种投注额报表'
      },
      component: () => import('views/reports/ReportLotteryBetAmountSearch/index'),
      name: 'ReportLotteryBetAmountSearch'
    },
    // {
    //   path: 'ReportWinLoseList', component: () => import('views/reports/ReportWinLoseList/index'),
    //   name: 'ReportWinLoseList ',
    //   meta: {
    //     permission: {
    //       ReportWinLoseList: [
    //         'ReportWinLoseList',
    //         'ReportExWinLoseList',
    //         'OrderInfoList'
    //         // 'ReportDownloadList'
    //       ]
    //     }, title: '输赢排名报表 '
    //   }
    // },
    {
      path: 'ReportLotteryWinLoseSearch',
      meta: {
        permission: { ReportLotteryWinLoseSearch: ['ReportLotteryWinLoseSearch', 'ReportLotteryWinLoseExport'] },
        title: '彩种输赢报表'
      },
      component: () => import('views/reports/ReportLotteryWinLoseSearch/index'),
      name: 'ReportLotteryWinLoseSearch'
    },
    // {
    //   path: 'ReportLotteryList', component: () => import('views/reports/ReportLotteryList/index'),
    //   name: 'ReportLotteryList',
    //   meta: {
    //     permission: {
    //       ReportLotteryList: [
    //         'ReportLotteryList',
    //         'ReportExLotteryList'
    //         // 'ReportDownloadList'
    //       ]
    //     }, title: '彩种报表'
    //   }
    // },
    {
      path: 'ReportGeneralSearch',
      component: () => import('views/reports/ReportGeneralSearch/index'),
      name: 'ReportGeneralSearch ',
      meta: {
        title: '汇总统计报表',
        permission: {
          ReportGeneralSearch:
            [
              'ReportGeneralSearch',
              'ReportGeneralExport',
              'ReStatistics'
              // 'ReportDownloadList'
            ]
        }
      }
    },
    // {
    //   path: 'ReportOptAnalysisList',
    //   component: () => import('views/reports/ReportOptAnalysisList/index'),
    //   name: 'ReportOptAnalysisList ',
    //   meta: {
    //     title: '运营分析报表',
    //     permission: {
    //       ReportOptAnalysisList:
    //         [
    //           'ReportOptAnalysisList'
    //         ]
    //     }
    //   }
    // },
    {
      path: 'ReportAnalysisLotterySearch',
      component: () => import('views/reports/ReportAnalysisLotterySearch/index'),
      name: 'ReportAnalysisLotterySearch ',
      meta: {
        title: '彩种分析报表',
        permission: {
          ReportAnalysisLotterySearch:
            [
              'ReportAnalysisLotterySearch',
              'ReportAnalysisLotteryExport'
            ]
        }
      }
    },
    {
      path: 'ReportAnalysisUserSearch',
      component: () => import('views/reports/ReportAnalysisUserSearch/index'),
      name: 'ReportAnalysisUserSearch ',
      meta: {
        title: '用户分析报表',
        permission: {
          ReportAnalysisUserSearch:
            [
              'ReportAnalysisUserSearch',
              'ReportAnalysisUserExport'
            ]
        }
      }
    },
    {
      path: 'ReportAnalysisSnSearch',
      component: () => import('views/reports/ReportAnalysisSnSearch/index'),
      name: 'ReportAnalysisSnSearch ',
      meta: {
        title: '厅主分析报表',
        permission: {
          ReportAnalysisSnSearch:
            [
              'ReportAnalysisSnSearch',
              'ReportAnalysisSnExport'
            ]
        }
      }
    },
    // {
    //   path: 'ReportLiveSummaryList',
    //   component: () => import('views/reports/ReportLiveSummaryList/ReportLiveSummaryList'),
    //   name: 'ReportLiveSummaryList ',
    //   meta: {
    //     title: '直播有效投注报表 ',
    //     permission: {
    //       ReportLiveSummaryList:
    //         [
    //           'ReportLiveSummaryList'
    //         ]
    //     }
    //   }
    // },
    {
      path: 'IssueOrbitList',
      meta: {
        permission: {
          IssueOrbitList: ['IssueOrbitList']
        },
        title: '自开彩开奖轨迹'
      },
      component: () => import('views/selfLottery/IssueOrbitList/index'),
      name: 'IssueOrbitList'
    },
    {
      path: 'ReportSelfLotteryRatio',
      component: () => import('views/reports/ReportSelfLotteryRatio/index'),
      name: 'ReportSelfLotteryRatio ',
      meta: {
        title: '杀率汇总报表 ',
        permission: {
          ReportSelfLotteryRatio:
            [
              'ReportSelfLotteryRatio'
            ]
        }
      }
    },
    {
      path: 'ReportSelfLotteryWinLoseList',
      component: () => import('views/reports/ReportSelfLotteryWinLoseList/index'),
      name: 'ReportSelfLotteryWinLoseList',
      meta: {
        title: '杀率排行报表',
        permission: {
          ReportSelfLotteryWinLoseList:
            [
              'ReportSelfLotteryWinLoseList'
            ]
        }
      }
    },
    {
      path: 'ReportDownloadList',
      meta: {
        title: '报表下载列表',
        permission: { ReportExDownloadList: ['ReportExDownloadList', 'ReportExDownload'] }
      },
      component: () => import('views/reports/ReportExDownloadList/index'),
      name: 'ReportDownloadList'
    }
  ]
}
