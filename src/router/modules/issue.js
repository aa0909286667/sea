/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/issue',
  component: Layout,
  redirect: '/issue/common',
  name: 'issue',
  meta: { permission: '#', icon: 'zonghe', title: '排程管理' },
  // noDropdown: true,
  children: [
    {
      path: 'IssueInfoList',
      meta: {
        permission: {
          IssueInfoList:
            [
              'IssueInfoList',
              //  'IssueRuleFineTune',
              'OrderCancelByIssue',
              'ReSettlement',
              'ManualDraw',
              'IssueRuleSpecialAdd',
              'IssueRuleSpecialDel'
            ]
        },
        title: '排程管理'
      },
      component: () => import('views/issue/common/common'),
      name: 'IssueInfoList'
    },
    // {
    //   path: 'GetIssueRuleInfo',
    //   meta: {
    //     permission: {
    //       GetIssueRuleInfo:
    //         [
    //           'GetIssueRuleInfo',
    //           //  'IssueRuleFineTune',
    //           'OrderCancelByIssue',
    //           'ReSettlement',
    //           'ManualDraw',
    //           'IssueRuleSpecialAdd',
    //           'IssueRuleSpecialDel'
    //         ]
    //     },
    //     title: '彩期规则'
    //   },
    //   component: () => import('views/issue/rule/index'),
    //   name: 'GetIssueRuleInfo'
    // },
    {
      path: 'IssueProduceRuleList',
      meta: {
        permission: {
          IssueProduceRuleList:
            [
              'IssueProduceRuleList',
              'IssueProduceRuleDisabled',
              'IssueProduceRuleEnabled',
              'IssueProduceExcludeRuleDisabled',
              'IssueProduceExcludeRuleEnabled',
              'IssueResetByProduceRule'
            ]
        },
        title: '彩期规则'
      },
      component: () => import('views/issue/IssueProduceRuleList/index'),
      name: 'IssueProduceRuleList'
    }
    // {
    //   path: 'lotteryCommon',
    //   meta: {
    //     // permission: {
    //     //   IssueInfoList:
    //     //     [
    //     //       'IssueInfoList',
    //     //       //  'IssueRuleFineTune',
    //     //       'OrderCancelByIssue',
    //     //       'ReSettlement',
    //     //       'ManualDraw',
    //     //       'IssueRuleSpecialAdd',
    //     //       'IssueRuleSpecialDel'
    //     //     ]
    //     // }
    //   },
    //   component: () => import('views/issue/lotteryCommon/lotteryCommon'),
    //   name: '通用彩期'
    // },
    // {
    //   path: 'lotterySpecial',
    //   meta: {
    //     // permission: {
    //     //   IssueInfoList:
    //     //     [
    //     //       'IssueInfoList',
    //     //       //  'IssueRuleFineTune',
    //     //       'OrderCancelByIssue',
    //     //       'ReSettlement',
    //     //       'ManualDraw',
    //     //       'IssueRuleSpecialAdd',
    //     //       'IssueRuleSpecialDel'
    //     //     ]
    //     // }
    //   },
    //   component: () => import('views/issue/lotterySpecial/lotterySpecial'),
    //   name: '特殊彩期'
    // }
  ]
}
