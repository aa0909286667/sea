/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/addSpManage',
  component: Layout,
  redirect: '/addSpManage/ShardingDBList',
  name: 'addSpManage',
  meta: { permission: '#', icon: 'add', title: '开厅管理' },
  children: [
    {
      path: 'ShardingDBList',
      meta: {
        permission: {
          ShardingDBList: ['ShardingDBList', 'ShardingDBSave']
        },
        title: '分库数据源设置'
      },
      component: () => import('views/addSpManage/branLibrarySet/index'),
      name: 'ShardingDBList'
    },
    {
      path: 'ShardingDatasourceList',
      meta: {
        permission: {
          ShardingDatasourceList: ['ShardingDatasourceList', 'ShardingDatasourceMaintain']
        },
        title: '分库数据源列表'
      },
      component: () => import('views/addSpManage/shardingDatasourceList/index'),
      name: 'ShardingDatasourceList'
    },

    {
      path: 'addSpList',
      meta: {
        permission: { SpRegList: ['SpRegList', 'SpRegClearTpl', 'SpRegInsert', 'SpRegSpList'] },
        title: '开厅列表'
      },
      component: () => import('views/addSpManage/addSpList/index'),
      name: 'addSpList'
    }
  ]
}
