/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/openCode',
  component: Layout,
  name: 'openCode',
  meta: {
    permission: '#', icon: 'tubiao', title: '开奖结果'
  },
  children: [
    {
      path: 'LotteryNoticeList',
      meta: {
        permission: { LotteryNoticeList: ['LotteryNoticeList'] },
        title: '近期开奖'
      },
      component: () => import('views/openCode/recent/recent'),
      name: 'LotteryNoticeList'
    }
  ]
}
