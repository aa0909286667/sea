/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/logs',
  component: Layout,
  meta: { permission: '#', icon: 'bug', title: '日志管理' },
  name: 'logs',
  children: [
    {
      path: 'LoggerApiList',
      meta: {
        permission: { LoggerApiList: ['LoggerApiList'] }, title: '接口访问日志'
      },
      component: () => import('views/logs/LoggerApi/LoggerApi'),
      name: 'LoggerApiList'
    },
    {
      path: 'LoggerServiceList',
      meta: {
        permission: { LoggerServiceList: ['LoggerServiceList'] }, title: '服务访问日志'
      },
      component: () => import('views/logs/LoggerService/LoggerService'),
      name: 'LoggerServiceList'
    },
    {
      path: 'LoggerTrdErrorList',
      meta: {
        permission: { LoggerTrdErrorList: ['LoggerTrdErrorList'] }, title: '平台交互日志'
      },
      component: () => import('views/logs/LoggerTrdError/LoggerTrdError'),
      name: 'LoggerTrdErrorList'
    },
    {
      path: 'LoggerHttpErrorList',
      meta: {
        permission: { LoggerHttpErrorList: ['LoggerHttpErrorList', 'LoggerHttpErrorReq'] }, title: '异常重发日志'
      },
      component: () => import('views/logs/LoggerHttpError/LoggerHttpError'),
      name: 'LoggerHttpErrorList'
    },
    {
      path: 'LoggerGatherList',
      component: () => import('views/logs/GatherLog/gatherlog'),
      name: 'LoggerGatherList',
      meta: {
        title: '开奖采集日志',
        permission: { LoggerGatherList: ['LoggerGatherList'] }
      }
    },
    // {
    //   path: 'OrderBigAmountList',
    //   meta: {
    //     permission: { 'OrderBigAmountList': ['OrderBigAmountList'] }
    //   },
    //   component: OrderBigAmountList, name: '大额注单'
    // },
    {
      path: 'OrderExceptionList',
      meta: {
        permission: { OrderExceptionList: ['OrderExceptionList'] }, title: '异常注单日志'
      },
      component: () => import('views/logs/OrderException/OrderException'),
      name: 'OrderExceptionList'
    },
    {
      path: 'OrderCancelExceptionList',
      meta: {
        permission: { OrderCancelExceptionList: ['OrderCancelExceptionList'] }, title: '异常撤单日志'
      },
      component: () => import('views/logs/OrderCancelException/OrderCancelException'),
      name: 'OrderCancelExceptionList'
    },
    {
      path: 'LoggerOpencodeList',
      meta: {
        permission: { LoggerOpencodeList: ['LoggerOpencodeList'] }, title: '人工开奖日志'
      },
      component: () => import('views/logs/LoggerOpencode/LoggerOpencode'),
      name: 'LoggerOpencodeList'
    },
    {
      path: 'LoggerCloudInfoList',
      meta: {
        permission: { LoggerCloudInfoList: ['LoggerCloudInfoList'] }, title: '云服务访问日志'
      },
      component: () => import('views/logs/LoggerList/loggerList'),
      name: 'LoggerCloudInfoList'
    }
    // {
    //   path: 'taskInfoList',
    //   meta: {
    //     permission: { taskInfoList: ['taskInfoList', 'taskSubInfoList'] },
    //     title: '结算任务列表'
    //   },
    //   component: () => import('views/logs/taskInfo/taskInfo'), name: 'taskInfoList'
    // }
  ]
}
