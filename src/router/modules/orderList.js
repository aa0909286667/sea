/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/orders',
  component: Layout,
  redirect: '/orders/find',
  name: 'orders',
  meta: {
    permission: '#',
    title: '注单管理',
    icon: 'eit'
  },
  children: [
    {
      path: 'find',
      meta: {
        permission: { OrderInfoList: ['OrderDetail', 'OrderTraceList', 'OrderTraceDetail', 'OrderInfoList', 'LotteryUserBetCancel', 'OrderDeal', 'OrderDealAgentRebate', 'OrderSettlement'] },
        title: '注单查询'
      },
      component: () => import('views/orders/find/find'),
      name: 'OrderInfoList'
    },
    {
      path: 'fund',
      meta: {
        permission: { MemberCashflowList: ['MemberCashflowList'] },
        title: '金流管理'
      },
      component: () => import('views/cashFlow/fund'),
      name: 'MemberCashflowList'
    },
    {
      path: 'ReportOrderList/detail/:type',
      hidden: true,
      meta: {
        permission: '#',
        NoTag: true,
        title: '注单统计',
        activeMenu: '/orders/ReportOrderList'
      },
      component: () => import('views/orders/ReportOrderList/detailList'),
      name: 'ReportOrderListDetail'
    },
    // {
    //   path: 'ChaseNumberList',
    //   meta: {
    //     permission: { OrderTraceList: ['OrderTraceList', 'OrderTraceDetail'] },
    //     title: '追号查询'
    //   },
    //   component: () => import('views/orders/ChaseNumberList/index'),
    //   name: 'ChaseNumberList'
    // },
    {
      path: 'OrderGameReportList',
      meta: {
        permission: { OrderGameReportList: ['OrderGameReportList'] },
        title: '局数据查询'
      },
      component: () => import('views/orders/officeData/officeData'),
      name: 'OrderGameReportList'
    },
    {
      path: 'OrderReverseInfoList',
      meta: {
        permission: { OrderReverseInfoList: ['OrderReverseInfoList', 'UpOrderReverseInfo'] },
        title: '补差记录'
      },
      component: () => import('views/orders/ReverseList/index'),
      name: 'OrderReverseInfoList'
    }
  ]
}
