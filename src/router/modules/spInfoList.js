/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/spInfoList',
  component: Layout,
  redirect: '/spInfoList/spadd',
  name: 'spInfoList',
  meta: { permission: '#', icon: 'zujian', title: '商户管理' },
  children: [
    // {
    //   path: 'PlatformList',
    //   meta: {
    //     permission: { PlatformList: ['PlatformList'] },
    //     title: '平台列表'
    //   },
    //   component: () => import('views/spInfoList/platform/index'),
    //   name: 'PlatformList'
    // },
    {
      path: 'SpInfoList',
      meta: {
        permission: {
          SpInfoList: [
            'SpInfoList',
            'SpInfoUpCancelStatus',
            'SpInfoUpStatus',
            'SpInfoDetail',
            'SpInfoUpRebateShow',
            'SpInfoUpAbonusLevel',
            'SpInfoUpMoneyUnit'
          ]
        },
        title: '厅主列表'
      },
      component: () => import('views/spInfoList/spadd/spadd'),
      name: 'SpInfoList'
    },
    // {
    //   path: 'ShardingDBList',
    //   meta: {
    //     permission: {
    //       ShardingDBList: ['ShardingDBList', 'ShardingDBSave']
    //     },
    //     title: '分库数据源设置'
    //   },
    //   component: () => import('views/spInfoList/branLibrarySet/index'),
    //   name: 'ShardingDBList'
    // },
    // {
    //   path: 'ShardingDatasourceList',
    //   meta: {
    //     permission: {
    //       ShardingDatasourceList: ['ShardingDatasourceList', 'ShardingDatasourceMaintain']
    //     },
    //     title: '分库数据源列表'
    //   },
    //   component: () => import('views/spInfoList/shardingDatasourceList/index'),
    //   name: 'ShardingDatasourceList'
    // },
    // {
    //   path: "SpLotteryTypeList",
    //   meta: {
    //     permission: {
    //       SpLotteryTypeList: ["SpLotteryTypeList", "SpLotteryTypeUp"]
    //     },
    //     title: "彩种类型配置"
    //   },
    //   component: () => import("views/spInfoList/spset/spset"),
    //   name: "SpLotteryTypeList"
    // },
    {
      path: 'SpLotteryMaxRewardList',
      meta: {
        permission: {
          SpLotteryMaxRewardList: [
            'SpLotteryMaxRewardList',
            'SpLotteryMaxReward'
          ]
        },
        title: '中奖限额配置'
      },
      component: () => import('views/spInfoList/splimit/splimit'),
      name: 'SpLotteryMaxRewardList'
    },
    {
      path: 'SpTplInfoList',
      meta: {
        permission: {
          SpTplInfoList: [
            'SpTplInfoList',
            'SpTplInfoCopy',
            'SpTplInfoUpName',
            'SpTplRebateSetUp',
            'SpTplLotterySetUp',
            'SpTplPlaySetUp',
            'SpTplRebateSetList',
            'SpTplLotterySetList',
            'getBandingAgentInfoList',
            'SpTplPlayList',
            'SpTplLotteryHotList',
            'SpTplLotteryRecommendList',
            'SpTplHotLotterySetUp',
            'SpTplRecommendLotterySetUp',
            'SpTplLotteryPlayModeUp',
            'SpTplLotteryPlayModeSwitchUp',
            'DwdLimitList',
            'DwdLimitSet'
          ]
        },
        title: '盘口管理'
      },
      component: () => import('views/spInfoList/spltpl/preview/preview'),
      name: 'SpTplInfoList'
    },
    {
      path: 'SpMaintainList',
      meta: {
        permission: {
          SpMaintainList: [
            'SpMaintainList',
            'SpMaintainSatus',
            'SpMaintainSave'
          ]
        },
        title: '维护计划'
      },
      component: () => import('views/spInfoList/maintainlist/maintainlist'),
      name: 'SpMaintainList'
    },
    {
      path: 'SpLotteryCurrIssueList',
      component: () => import('views/spInfoList/currissuelist/index'),
      name: 'SpLotteryCurrIssueList',
      meta: {
        title: '开封盘设置',
        permission: {
          SpLotteryCurrIssueList: [
            'SpLotteryCurrIssueList',
            'SpLotteryCurrIssueEnabled',
            'SpLotteryCurrIssueDisabled'
          ],
          SpLotteryBetPauseList: ['SpLotteryBetPauseList', 'SpLotteryBetPause']
        }
      }
    },
    {
      path: 'OpenCodeCollectList',
      meta: {
        permission: {
          OpenCodeCollectList: [
            'OpenCodeCollectList',
            'OpenCodeCollectSave',
            'OpenCodeCollectUp',
            'OpenCodeCollectSwitch'
          ]
        },
        title: '开奖源采集设置'
      },
      component: () => import('views/spInfoList/OpenCodeCollectList/index'),
      name: 'OpenCodeCollectList'
    }
    // {
    //   path: 'LongdragonList',
    //   meta: {
    //     permission: {
    //       LongdragonConfList: ['LongdragonConfList', 'LongdragonConfSave']
    //     },
    //     title: '长龙设置'
    //   },
    //   component: () => import('views/dataBase/Longdragon/index'),
    //   name: 'LongdragonList'
    // }
  ]
}
