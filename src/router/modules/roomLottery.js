/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/roomLottery',
  component: Layout,
  redirect: '/roomLottery/globalSwitch',
  meta: {
    permission: '#',
    title: '房间模式',
    icon: 'dynamic'
  },
  name: 'roomLottery',
  children: [
    {
      path: 'roomLotteryList',
      meta: {
        title: '全局开关',
        permission: { roomLotteryList: ['roomLotteryList', 'roomLotterySwitch'] }
      },
      component: () => import('views/roomLottery/GlobalSwitch/index'),
      name: 'roomLotteryList'
    },
    {
      path: 'roomLotteryBindSNList',
      meta: {
        permission: { roomLotteryBindSNList: ['roomLotteryBindSNList', 'roomLotteryBindSN', 'roomLotteryBindSNSwitch'] },
        title: '彩种开通开关'
      },
      component: () => import('views/roomLottery/LotterySwitch/'),
      name: 'roomLotteryBindSNList'
    },
    {
      path: 'roomPlayList',
      meta: {
        permission: {
          roomLevelList: ['roomLevelList', 'roomLevelSwitch'],
          roomPlayList: ['roomPlayList', 'roomPlaySave'],
          roomPlaySpecialList: ['roomPlaySpecialList', 'roomPlaySpecialSave'],
          roomQuotaList: ['roomQuotaList', 'roomQuotaSave'],
          roomConditionList: ['roomConditionList', 'roomConditionSave']
        },
        title: '房间设置'
      },
      component: () => import('views/roomLottery/RoomSetting/'),
      name: 'roomPlayList'
    }
  ]
}
