/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/OrderCurrReportList',
  component: Layout,
  redirect: '/OrderCurrReportList/OrderCurrReportList',
  meta: {
    permission: '#',
    icon: 'flashlight',
    title: '即时注单'
  },
  name: 'OrderCurrReportList',
  children: [
    {
      path: 'OrderCurrReportList',
      component: () => import('views/OrderCurrReportList/index'),
      meta: {
        permission: {
          OrderCurrReportList: ['OrderCurrReportList'], OrderCreditCurrReportList: ['OrderCreditCurrReportList'],
          OrderAllSnCreditCurrReportList: ['OrderAllSnCreditCurrReportList']
        },
        title: '即时注单'
      },
      name: 'OrderCurrReportList'
    }
    // {
    //   path: 'OrderAllSnCreditCurrReportList',
    //   component: () => import('views/OrderCurrReportList/OrderAllSnCreditCurrReportList'),
    //   meta: {
    //     permission: {
    //       OrderAllSnCreditCurrReportList: ['OrderAllSnCreditCurrReportList']
    //     },
    //     title: '即时注单(全厅)'
    //   },
    //   name: 'OrderAllSnCreditCurrReportList'
    // }
  ]
}
