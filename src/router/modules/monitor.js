/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

export default {
  path: '/monitor',
  component: Layout,
  meta: {
    permission: '#',
    icon: 'c',
    title: '监控管理'
  },
  icon: 'c',
  name: 'monitor',
  children: [
    {
      path: '/monitor/content',
      meta: {
        permission: {
          MonitorOrderList: ['MonitorOrderList', 'MonitorOrderUpdate'],
          MonitorIssueList: ['MonitorIssueList', 'MonitorIssueUpdate']
        },
        title: '监控中心',
        noActive: true
      },
      name: 'MonitorOrderList',
      isBlank: true
    },
    {
      path: 'ReportOrderList',
      meta: {
        permission: { ReportOrderList: ['ReportOrderList', 'GlobalDealAgentRebate', 'GlobalOrderSettlement', 'GlobalPayOutReward'] },
        title: '异常注单统计',
        keepAlive: true
      },
      component: () => import('views/orders/ReportOrderList/index'),
      name: 'ReportOrderList'
    },
    {
      path: 'abnormalHandle',
      component: () => import('views/monitor/abnormalHandle/index'),
      name: '异常服务中心',
      meta: {
        permission: {
          OrderErrorList: [
            'OrderErrorList',
            'OrderErrorDetail',
            'OrderErrorHandleProcessor',
            'OrderErrorHandleSearch'
          ]
        },
        title: '异常服务中心'
      }
    },
    {
      path: 'OrderDoubtList',
      component: () => import('views/monitor/OrderDoubtList/index'),
      name: '可疑注单列表',
      meta: {
        permission: {
          OrderDoubtList: [
            'OrderDoubtList'
          ]
        },
        title: '可疑注单列表'
      }
    }
  ]
}
