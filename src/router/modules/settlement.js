/**
 * Created by Xiaowu on 2018/11/14.
 */
/* Layout */
import Layout from '@/layout'

const settlement = {
  path: '/settleManage',
  component: Layout,
  name: 'settleManage',
  redirect: '/settleManage/SettleSettingList',
  meta: { permission: '#', title: '结算管理', icon: 'b' },
  children: [
    {
      path: 'SettleSettingList',
      component: () => import('views/settleManage/SettleSettingList/index'),
      name: 'SettleSettingList',
      meta: {
        permission: { SettleSettingList: ['SettleSettingList', 'SettleSettingDetail', 'SettleSettingSave', 'SettleSettingAdd'] },
        title: '结算配置'
      }
    },
    {
      path: 'SettleUserblockList',
      component: () => import('views/settleManage/userBlock/index'),
      name: 'SettleUserblockList',
      meta: {
        permission: { SettleUserblockList: ['SettleUserblockList', 'SettleUserblockUserIds'] }, title: '冻结日志'
      }
    },
    {
      path: 'taskInfoList',
      meta: {
        permission: { taskInfoList: ['taskInfoList', 'taskSubInfoList'] },
        title: '结算任务列表'
      },
      component: () => import('views/settleManage/taskInfo/taskInfo'),
      name: 'taskInfoList'
    },
    {
      path: 'TaskRefundList',
      meta: {
        permission: { TaskRefundList: ['TaskRefundList', 'TaskRefundFailedList', 'TaskRefundFailedProcess'] },
        title: '撤单任务列表'
      },
      component: () => import('views/settleManage/TaskRefundList/index'),
      name: 'TaskRefundList'
    }
  ]
}
export default settlement
