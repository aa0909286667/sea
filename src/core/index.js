import Pagination from 'core/packages/pagination/'
import Dialog from 'core/packages/dialog/'
import Autocomplete from 'core/packages/autocomplete/'
import Dropdown from 'core/packages/dropdown/'
import DropdownMenu from 'core/packages/dropdown-menu/'
import DropdownItem from 'core/packages/dropdown-item/'
import Menu from 'core/packages/menu/'
import Submenu from 'core/packages/submenu/'
import MenuItem from 'core/packages/menu-item/'
import MenuItemGroup from 'core/packages/menu-item-group/'
import Input from 'core/packages/input/'
import InputNumber from 'core/packages/input-number/'
import Radio from 'core/packages/radio/'
import RadioGroup from 'core/packages/radio-group/'
import RadioButton from 'core/packages/radio-button/'
import Checkbox from 'core/packages/checkbox/'
import CheckboxButton from 'core/packages/checkbox-button/'
import CheckboxGroup from 'core/packages/checkbox-group/'
import Switch from 'core/packages/switch/'
import Select from 'core/packages/select/'
import Option from 'core/packages/option/'
import OptionGroup from 'core/packages/option-group/'
import Button from 'core/packages/button/'
import ButtonGroup from 'core/packages/button-group/'
import Table from 'core/packages/table/'
import TableColumn from 'core/packages/table-column/'
import DatePicker from 'core/packages/date-picker/'
import TimeSelect from 'core/packages/time-select/'
import TimePicker from 'core/packages/time-picker/'
import Popover from 'core/packages/popover/'
import Tooltip from 'core/packages/tooltip/'
import MessageBox from 'core/packages/message-box/'
import Breadcrumb from 'core/packages/breadcrumb/'
import BreadcrumbItem from 'core/packages/breadcrumb-item/'
import Form from 'core/packages/form/'
import FormItem from 'core/packages/form-item/'
import Tabs from 'core/packages/tabs/'
import TabPane from 'core/packages/tab-pane/'
import Tag from 'core/packages/tag/'
import Tree from 'core/packages/tree/'
import Alert from 'core/packages/alert/'
import Notification from 'core/packages/notification/'
import Slider from 'core/packages/slider/'
import Loading from 'core/packages/loading/'
import Icon from 'core/packages/icon/'
import Row from 'core/packages/row/'
import Col from 'core/packages/col/'
import Upload from 'core/packages/upload/'
import Progress from 'core/packages/progress/'
import Spinner from 'core/packages/spinner/'
import Message from 'core/packages/message/'
import Badge from 'core/packages/badge/'
import Card from 'core/packages/card/'
import Rate from 'core/packages/rate/'
import Steps from 'core/packages/steps/'
import Step from 'core/packages/step/'
import Carousel from 'core/packages/carousel/'
import Scrollbar from 'core/packages/scrollbar/'
import CarouselItem from 'core/packages/carousel-item/'
import Collapse from 'core/packages/collapse/'
import CollapseItem from 'core/packages/collapse-item/'
import Cascader from 'core/packages/cascader/'
import ColorPicker from 'core/packages/color-picker/'
import Transfer from 'core/packages/transfer/'
import Container from 'core/packages/container/'
import Header from 'core/packages/header/'
import Aside from 'core/packages/aside/'
import Main from 'core/packages/main/'
import Footer from 'core/packages/footer/'
import Timeline from 'core/packages/timeline/'
import TimelineItem from 'core/packages/timeline-item/'
import Link from 'core/packages/link/'
import Divider from 'core/packages/divider/'
import Image from 'core/packages/image/'
import Calendar from 'core/packages/calendar/'
import Backtop from 'core/packages/backtop/'
import InfiniteScroll from 'core/packages/infinite-scroll/'
import PageHeader from 'core/packages/page-header/'
import CascaderPanel from 'core/packages/cascader-panel/'
import Avatar from 'core/packages/avatar/'
import Drawer from 'core/packages/drawer/'
import Popconfirm from 'core/packages/popconfirm/'
import locale from 'core/locale'
import CollapseTransition from 'core/transitions/collapse-transition'

const components = [
  Pagination,
  Dialog,
  Autocomplete,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Input,
  InputNumber,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Switch,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Popover,
  Tooltip,
  Breadcrumb,
  BreadcrumbItem,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Tag,
  Tree,
  Alert,
  Slider,
  Icon,
  Row,
  Col,
  Upload,
  Progress,
  Spinner,
  Badge,
  Card,
  Rate,
  Steps,
  Step,
  Carousel,
  Scrollbar,
  CarouselItem,
  Collapse,
  CollapseItem,
  Cascader,
  ColorPicker,
  Transfer,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Timeline,
  TimelineItem,
  Link,
  Divider,
  Image,
  Calendar,
  Backtop,
  PageHeader,
  CascaderPanel,
  Avatar,
  Drawer,
  Popconfirm,
  CollapseTransition
]

const install = function (Vue, opts = {}) {
  locale.use(opts.locale)
  locale.i18n(opts.i18n)

  components.forEach(component => {
    Vue.component(component.name, component)
  })

  Vue.use(InfiniteScroll)
  Vue.use(Loading.directive)

  Vue.prototype.$ELEMENT = {
    size: opts.size || '',
    zIndex: opts.zIndex || 2000
  }

  Vue.prototype.$loading = Loading.service
  Vue.prototype.$msgbox = MessageBox
  Vue.prototype.$alert = MessageBox.alert
  Vue.prototype.$confirm = MessageBox.confirm
  Vue.prototype.$prompt = MessageBox.prompt
  Vue.prototype.$notify = Notification
  Vue.prototype.$message = Message
}

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  locale: locale.use,
  i18n: locale.i18n,
  install,
  CollapseTransition,
  Loading,
  Pagination,
  Dialog,
  Autocomplete,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Input,
  InputNumber,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Switch,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Popover,
  Tooltip,
  MessageBox,
  Breadcrumb,
  BreadcrumbItem,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Tag,
  Tree,
  Alert,
  Notification,
  Slider,
  Icon,
  Row,
  Col,
  Upload,
  Progress,
  Spinner,
  Message,
  Badge,
  Card,
  Rate,
  Steps,
  Step,
  Carousel,
  Scrollbar,
  CarouselItem,
  Collapse,
  CollapseItem,
  Cascader,
  ColorPicker,
  Transfer,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Timeline,
  TimelineItem,
  Link,
  Divider,
  Image,
  Calendar,
  Backtop,
  InfiniteScroll,
  PageHeader,
  CascaderPanel,
  Avatar,
  Drawer,
  Popconfirm
}
