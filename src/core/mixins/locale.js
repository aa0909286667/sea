import { t } from 'core/locale';

export default {
  methods: {
    t(...args) {
      return t.apply(this, args);
    }
  }
};
