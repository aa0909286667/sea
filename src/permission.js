import NProgress from 'nprogress' // progress bar
import router from './router'
import store from './store'
import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from 'utils/storage' // 验权
// import { setIsOpenQrDialog, removeQrInfo } from 'utils/storage'
// register global progress.
const whiteList = ['/login', '/401', '/404', '/maintenance']// 不重定向白名单
router.beforeEach((to, from, next) => {
  NProgress.start() // 开启Progress
  if (store.getters.token) { // 判断是否有token
    if (to.path === '/login') {
      next({ path: store.getters.redirect })
    } else {
      // if (to.path === '/selfLottery/SelfLotteryCurrIssueList') {
      //   removeQrInfo()
      //   setIsOpenQrDialog(0)
      //   store.dispatch('setIsOpen', true)
      // }
      next()
    }
  } else if (whiteList.includes(to.path)) { // 在免登录白名单，直接进入
    next()
  } else {
    next(`/login`) // 否则全部重定向到登录页
    NProgress.done() // 在hash模式下 改变手动改变hash 重定向回来 不会触发afterEach 暂时hack方案 ps：history模式下无问题，可删除该行！
  }
  NProgress.done()
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
