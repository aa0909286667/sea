export default {
  route: {},
  navbar: {
    logOut: '退出登录',
    dashboard: '首页',
    size: '布局大小'
  },
  login: {
    title: '系统登录',
    logIn: '登录',
    username: '账号',
    password: '密码',
    any: '随便填'
  },
  tagsView: {
    refresh: '刷新',
    close: '关闭',
    closeOthers: '关闭其它',
    closeAll: '关闭所有'
  },
  settings: {
    title: '顶部设置',
    theme: '主题色',
    sidebar: '左边栏',
    tagsView: '开启标签栏',
    fixedHeader: '固定顶部',
    sidebarLogo: '侧边栏LOGO'
  }
}
