import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// import Element from 'element-ui'
import Element from 'core/index'
import './styles/element-variables.scss'

import 'styles/index.scss' // global css

import VueLazyload from 'vue-lazyload'
import App from './App'
import router from './router'
import store from './store'
import 'styles/fontawesome/fontawesome.scss'
import i18n from './lang' // Internationalization
import './icons' // icon
import './permission' // permission control
import './utils/errorLog' // error log
import * as filters from './filters' // global filters
import mixin from './mixin'
// import XwCheckbox from 'components/checkbox'
// import XwCheckboxButton from 'components/checkbox-button'
// import XwCheckboxGroup from 'components/checkbox-group'
//
// Vue.use(XwCheckbox)
// Vue.use(XwCheckboxButton)
// Vue.use(XwCheckboxGroup)

Vue.use(Element, {
  size: Cookies.get('size') || 'small', // set element-ui default size
  zIndex: 3000,
  i18n: (key, value) => i18n.t(key, value)
})

// register global utility filters.
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})
Vue.prototype.hasPermission = function (v) {
  const { noPermissionList } = store.state.user
  return !!(noPermissionList && noPermissionList.length && noPermissionList.includes(v))
}
Vue.prototype.isAuth = function (v) {
  const { permission } = store.state.user
  return !!(permission && permission.length && permission.includes(v))
}
// console.log(process.env.npm_config_edition)
Vue.prototype.$LOGO = process.env.NODE_ENV !== 'production' ? '/static/icons/' : `${window.cdnPath}/seasys/${window.verPath}/icons/`
Vue.prototype.$ICON = process.env.NODE_ENV !== 'production' ? '/static/icons/smallicon/' : `${window.cdnPath}/seasys/${window.verPath}/icons/smallicon/`
Vue.config.productionTip = false
Vue.use(VueLazyload, {
  loading: require('@/assets/default.gif')
})
new Vue({
  el: '#app',
  router,
  mixins: [mixin],
  store,
  i18n,
  render: (h) => h(App)
})
