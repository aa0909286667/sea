export default {
  headerSkin: {
    blue: {
      header: '#2196f3',
      logoBg: '#007cd6',
      activeTextColor: '#2196f3'
    },
    teal: {
      header: '#007c6f',
      logoBg: '#009688',
      activeTextColor: '#007c6f'
    },
    red: {
      header: '#f44336',
      logoBg: '#d31f1f',
      activeTextColor: '#f44336'
    },
    orange: {
      header: '#ff9800',
      logoBg: '#df7e00',
      activeTextColor: '#ff9800'
    },
    purple: {
      header: '#9c27b0',
      logoBg: '#800095',
      activeTextColor: '#9c27b0'
    },
    indigo: {
      header: '#3f51b5',
      logoBg: '#193a9a',
      activeTextColor: '#3f51b5'
    },
    cyan: {
      header: '#00bcd4',
      logoBg: '#00a1b8',
      activeTextColor: '#00bcd4'
    },
    pink: {
      header: '#e91e63',
      logoBg: '#c9004b',
      activeTextColor: '#e91e63'
    },
    green: {
      header: '#4caf50',
      logoBg: '#2d9437',
      activeTextColor: '#4caf50'
    }
  },
  sidebarSkins: {
    dark: {
      activeBg: '#232323',
      textColor: '#fff'
    },
    light: {
      activeBg: '#fff',
      textColor: '#000'
    }
  },
  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
