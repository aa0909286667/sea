/**
 * '11' :彩种类型
 *  min : number, 暂无使用(可忽略)
 *  isHasSummary : boolean, 是否计算和值
 *  fillZero : boolean, 号码球是否补全0
 *  num : number[], 开奖号码数,84情况下拆分
 *  typeName : string, 彩种类型名称
 *  range : number[], 号码球范围
 *  text : string, 填充说明(注释文本)
 *  splitNumber : number, 总和大小分隔值
 *  canRepeat : boolean 开奖号码是否可重复
 *  regText : Array 校验数组
 */
export const LotteryText = {
  22: {
    typeName: '3D系列',
    num: [20],
    canRepeat: true,
    fillZero: false,
    min: 0,
    isHasSummary: false,
    text: ['开奖号码示例：875938,294,328,597,780,98'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9]{6}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{2})$/]
  },
  23: {
    typeName: '4D系列',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: true,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  24: {
    typeName: 'Mega 6/45系列',
    num: [12],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: true,
    text: ['开奖号码示例：12,34,56,78,91,01'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  25: {
    typeName: '北部越南彩系列',
    num: [6],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: true,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  26: {
    typeName: '極速系列',
    num: [6],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: true,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  66: {
    typeName: '泰国彩',
    num: [20],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: false,
    text: ['开奖号码示例：875938,294,328,597,780,98'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: null,
    regText: [/^([0-9]{6}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{3}[,])([0-9]{2})$/]
  },
  84: {
    typeName: '越南彩',
    num: [10, 11],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: false,
    text: ['开奖号码示例：92573,03777', '开奖号码示例：301413,00083'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: 206,
    regText: [/^([0-9]{5}[,])([0-9]{5})$/, /^([0-9]{6}[,])([0-9]{5})$/]
  },
  845: {
    typeName: '越南自開彩',
    num: [6, 107],
    canRepeat: true,
    fillZero: false,
    min: 1,
    isHasSummary: false,
    text: ['开奖号码示例：925735', '开奖号码示例：301413,00083'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    splitNumber: 206,
    regText: [/^([0-9]{5}[,])([0-9]{5})$/, /^([0-9]{6}[,])([0-9]{5})$/]
  },
  856: {
    typeName: '老挝彩',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    splitNumber: null,
    isHasSummary: false,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  60: {
    typeName: '马来西亚',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    splitNumber: null,
    isHasSummary: false,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  65: {
    typeName: '新加坡',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    splitNumber: null,
    isHasSummary: false,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  16: {
    typeName: '股票彩',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    splitNumber: null,
    isHasSummary: false,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  886: {
    typeName: '台湾',
    num: [4],
    canRepeat: true,
    fillZero: false,
    min: 1,
    splitNumber: null,
    isHasSummary: false,
    text: ['开奖号码示例：1,2,3,4'],
    range: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    regText: [/^([0-9][,])([0-9][,])([0-9][,])([0-9])$/]
  },
  21: {
    typeName: '快乐8系列',
    num: [20],
    canRepeat: false,
    fillZero: true,
    min: 1,
    isHasSummary: true,
    splitNumber: null,
    text: ['开奖号码必须是01-80中的20个不重复的号码组成，如：02,04,05,07,08,12,14,19,21,25,28,23,34,43,47,49,52,63,72,78'],
    range: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80],
    regText: []
  }
}
