import axios from 'axios'
import Message from 'core/packages/message/'
import MessageBox from 'core/packages/message-box/'
import { commonParams } from 'api/config'
import { removeQrInfo } from 'utils/storage'
import store from '../store'

if (process.env.NODE_ENV !== 'production') {
  window.APITIMEOUT = 3 * 1000
}
const platFormApis = ['auth/api/']
const valiidateAPIs = ['ReSettlement', 'UserLogin', 'SelfLotteryDetailAdd', 'SelfLotteryDetailUp', 'SpTplPlaySetUp', 'SpTplLotteryPlaySetUp', 'SpTplLotteryPlaySwitchUp']
// const isProd = process.env.NODE_ENV === 'production'
// const FRONT_BASEURL = window.API_ADMINFRONT_PROD
const service = axios.create({
  // timeout: window.APITIMEOUT || 5 * 1000,
  // headers: {
  //   'Content-Type': 'text/plain'
  // }
  // transformResponse: [function(data) {
  //   console.log(data)
  //   return data
  // }],
  // transformRequest: [function (data) {
  //   // 对 data 进行任意转换处理
  //   console.log(data)
  //   return data
  // }]
})

function allDownHandler () {
  const allDownError = {
    code: 'alldown',
    data: '',
    desc: '接口未能响应,请稍后重试!'
  }
  Message.error('网络检查异常 请重试')
  return Promise.reject(allDownError)
}

function tryPing (url, onComplete, onError, timeout) {
  let inited = false
  getImg.call(this, url)

  function getImg (urls) {
    if (timeout > 30 * 1000) timeout = 30 * 1000
    window.nanoajax.ajax(
      {
        url: `${urls}/cc.png?${Date.now()}`,
        method: 'get',
        timeout: timeout || window.TIMEOUT
      },
      (code, response) => {
        if (code === 200) {
          if (!inited) {
            inited = true
            onComplete(urls)
          }
        } else {
          onError()
        }
      }
    )
  }
}

/**
 * 设置config的url,返回config
 * @param config 原始axiosconfig配置
 * @param apiPath 默认为空,不为空时切换接口地址
 * @returns {{data}|*}
 */
// 配置rquest的config.url
function setConfigUrl (config, apiPath = '') {
  const cmd = config && config.data && config.data.cmd ? config.data.cmd : ''
  config.apiPaths = window.API_DOMAINS
  if (apiPath) window.API_ADMINFRONT_PROD = apiPath
  if (platFormApis.includes(config.url)) config.platform = 'platform'
  config.url = config.url !== undefined ? (platFormApis.includes(config.url) ? `${window.API_ADMINFRONT_PROD}/${config.url}` : config.url) : `${window.API_ADMINFRONT_PROD + window.API_F_PATH + window.API_PATH}/${cmd}`
  return config
}

// request拦截器
service.interceptors.request.use(
  (config) => {
    if (config.isRetry) { // 针对重试的config，发起重试时，加了该标记
      return config
    }
    // 生产环境添加header
    const urlConfig = setConfigUrl(config)
    config.url = urlConfig.url
    const cmd = config && config.data && config.data.cmd ? config.data.cmd : ''
    config.data = !valiidateAPIs.includes(cmd) ? JSON.stringify({
      ...commonParams(), ...config.data || {}
    }) : JSON.stringify({
      ...commonParams(), ...config.data || {}, qrText: store.getters.qrText
    })
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    return config
  },
  (error) => {
    console.log(`请求错误: ${error}`)
    Promise.reject(error)
  }
)

service.interceptors.response.use(
  (response) => {
    const conf = JSON.parse(response.config.data)
    const { config } = response
    const { cmd } = conf
    const res = response.data
    const { platform } = config
    // console.log(response.config.data)
    if (platform) return Promise.resolve(response)
    if (cmd === 'qrCodeCreate') store.dispatch('lotteryQrCode/setQrIdentity', conf.identity)
    if (res.code === 0) {
      if (valiidateAPIs.includes(cmd)) {
        Message({
          message: '操作成功！',
          type: 'success',
          duration: 5 * 1000
        })
      }
      return Promise.resolve(response)
    }
    if (res.code === 1101006 || res.code === 1101009) {
      MessageBox.confirm(`${res.desc}，或者可以取消继续留在该页面`, '确定登出', {
        confirmButtonText: '重新登录',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        store.dispatch('user/FedLogOut').then(() => {
          location.reload() // 为了重新实例化vue-router对象 避免bug
        })
      })
      return Promise.resolve(response)
    }
    if (res.code === 1101031) {
      removeQrInfo()
      store.dispatch('lotteryQrCode/setIsOpen', true)
      store.dispatch('lotteryQrCode/setShowCode', true)
      store.dispatch('app/IncreaseQrHashKey', Math.random() * 55555 + 1)
      store.dispatch('lotteryQrCode/setCallOptions', JSON.parse(response.config.data) || {})
      store.dispatch('lotteryQrCode/setQrErrInfo', res.desc)
      return Promise.reject(response)
    }
    // console.log(response)
    // const conf = JSON.parse(response.config.data) || {}
    // // console.log('response', response)
    // const cmd = conf.cmd || ''
    if (cmd === 'qrCodeScan') return Promise.resolve(response)
    let message = `<div>
        <p>错误描述：${res.desc}；</p> 
        <p>错误码:${res.identity}</p>
      </div>`
    if (res.reason) {
      message = `<div>
        <p>错误描述：${res.desc}；</p> 
        <p>错误原因：${res.reason}；</p> 
        <p>错误码:${res.identity}</p>
      </div>`
    }
    Message({
      dangerouslyUseHTMLString: true,
      message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.resolve(response)
  },
  async (error) => {
    if (process.env.NODE_ENV === 'development') {
      return allDownHandler()
    }
    // 重试
    const config = await error.config
    config.isRetry = true
    config.isRetryNow = config.isRetryNow || 0
    const backoff = (url) => new Promise((resolve, reject) => {
      tryPing(url, (apiPath) => {
        resolve(apiPath)
      }, () => {
        apitryPing()
      }, Math.pow(2, config.isRetryNow) * window.APITIMEOUT)
    })

    /**
     * 接口地址重试机制
     * @returns {Promise<unknown>}
     */
    async function apitryPing () {
      const url = config.apiPaths[config.isRetryNow]
      config.isRetryNow += 1
      if (!url) {
        return allDownHandler()
      }

      return await backoff(url).then((apiPath) => {
        const rTConfig = JSON.parse(error.config)
        const urlConfig = setConfigUrl(rTConfig, apiPath)
        config.url = urlConfig.url
        config.timeout = !config.timeout ? window.APITIMEOUT : (config.timeout > 30 * 1000 ? 30 * 1000 : config.timeout *= 2)
        return service(config)
      })
    }

    return apitryPing()
  }
)

export default service
