// 这里是做一些公共的业务处理
import {
  isEmpty, isNil, isArray, isNumber, isString, isObject
} from 'lodash'

const tools = {
  listBack (data, success, error, empty) {
    // 处理类似列表返回的一些数据功能处理， 针对数组在item
    // 如果error=null， 代表没有错误信息
    if (isNil(data.error)) {
      // 列表数据判断流程，result可能出现的值，[], null, {}, errror !=null还会出现result=0的情况, result.items=[]
      if (isEmpty(data.result) || isNil(data.result) || isEmpty(data.result.items) || isNil(data.result.items)) {
        // isEmpty(0) => true注意这类型的判断，不适合这样的判断
        // 相当于没有数据,empty
        empty()
      } else {
        // 返回正确数据的话,success
        if (Array.isArray(data.result.items)) {
          success()
        }
      }
    } else {
      // error 错误提示,message有可能是不返回的
      if (!isNil(data.error.message)) {
        error()
      }
    }
  },
  listResultBack (data, success, error, empty) {
    // 处理类似列表返回的一些数据功能处理， 针对数组在result这种情况
    if (isNil(data.error)) {
      if (isEmpty(data.result) || isNil(data.result)) {
        // isEmpty(0) => true注意这类型的判断，不适合这样的判断
        empty()
      } else if (Array.isArray(data.result)) {
        success()
      }
    } else if (!isNil(data.error.message)) {
      error()
    }
  },
  goRandomUrl (data, type, _this) {
    // 从数组中随机选取一个
    let randomArray = 0
    if (data.length > 0) {
      randomArray = data[Math.floor(Math.random() * data.length)] // 取数组某个随机数
      if (randomArray !== undefined) {
        // 没有http自动加上
        let _domain = randomArray.domain
        if (parseInt(type) === 2 || (parseInt(type) !== 1 && Math.round(Math.random() * 1) === 1)) {
          // _domain = 'm.' + _domain
          _domain += /\?/.test(_domain) ? '&' : '?'
          _domain += `sn=${_this.$cookie.get('SN')}`
        }
        _domain = /(http[s]{0,1}):\/\//.test(_domain) ? _domain : `http://${_domain}`
        // let http = 'http://' + randomArray.domain.replace(/^http:\/\//i, '')
        window.open(_domain)
      }
    }
  },
  transferAmerica (val) {
    // 本地时间转美东时间
    return Number(val) - 3600 * 1000 * 12 * 1
  },
  // 美东转为北京时间
  usToBjDateTime (value) {
    if (!value || isNil(value)) return ''
    return window.moment(new Date(value).getTime() + 12 * 3600 * 1000).format('YYYY-MM-DD HH:mm:ss')
  },
  getUTCTime2China (val) {
    // 获取utc时间+8小时转北京时间
    let timenow = ''
    let timeStamp = ''
    if (isNil(val)) {
      timenow = new Date()
      timeStamp = Number(new Date(
        timenow.getUTCFullYear(),
        timenow.getUTCMonth(),
        timenow.getUTCDate(),
        timenow.getUTCHours(),
        timenow.getUTCMinutes(),
        timenow.getUTCSeconds()
      ).getTime()) + 8 * 3600 * 1000
    } else {
      timeStamp = new Date(new Date(val).toISOString().slice(0, -5)).getTime() + 8 * 3600 * 1000
    }
    return timeStamp
  },
  editBackResult (data, success, error) {
    // 针对返回result = 1这种情况, 比如启用，停用，修改这种
    if (isNil(data.error)) {
      if (+data.result === 1) {
        success()
      }
    } else if (!isNil(data.error.message)) {
      error()
    }
  },
  addBackResult (data, success, error) {
    // 针对返回result > -1这种情况, 比如新增
    if (isNil(data.error)) {
      if (+data.result > -1) {
        success()
      }
    } else if (!isNil(data.error.message)) {
      error()
    }
  },
  valid: {
    passEmpty: /^[^ ]+$/, // 包含空格，含前后中间
    account: /^[a-zA-Z0-9-][\sa-zA-Z0-9-]*[a-zA-Z0-9-]$/, // 首尾不能空格，中间可以有的数字/字母/横线
    maxLength (val, len) { // 不能超过多少个字符
      return val.length > len
    },
    tofix2: /^([1-9]\d*|0)(\.([0-9]|\d[0-9]))?$/, // 整数，最多保留两位小数
    tofix1: /^([1-9]\d*|0)(\.([0-9]))?$/, // 数字，最多保留1位小数
    isint: /^[1-9]\d*$/ // 正整数
  },
  // 导出参数处理post
  splicingParamPost (items, arrayUnturn = false) {
    if (isEmpty(items)) {
      return {}
    }
    const params = {}
    for (const item in items) {
      let value = items[item]
      if (isNil(value)) {
        continue
      }
      if (isString(value) && value === '') {
        continue
      }
      if (isString(value) || isNumber(value)) {
        // 非对象数组
        if (value !== null && value !== '') {
          params[item] = value
        }
      } else if (isArray(value)) {
        if (!arrayUnturn) {
          value = value.filter((v) => v !== null && v !== '')
          if (value.length <= 0) {
            continue
          }
          if (value.some((i) => isObject(i))) {
            params[item] = value
          } else {
            params[item] = value.join(',')
          }
        } else {
          params[item] = value
        }
      } else if (isObject(value)) {
        params[item] = value
      }
    }
    return params
  },
  splicingParam (items) {
    function isEmpty (str) {
      return str === null || str === undefined
    }

    function isNum (str) {
      return !isEmpty(str) && typeof (str) === 'number'
    }

    function isStr (str) {
      return !isEmpty(str) && typeof (str) === 'string'
    }

    function isArr (str) {
      return !isEmpty(str) && typeof (str) === 'object'
    }

    if (isEmpty(items)) {
      return ''
    }
    let value = ''
    const params = []
    let pItem = null
    for (const item in items) {
      pItem = ''
      value = items[item]
      if (isEmpty(value)) {
        continue
      }
      if (isStr(value) && value === '') {
        continue
      }
      if (isArr(value) && value.length <= 0) {
        continue
      }
      if (isStr(value) || isNum(value)) {
        // 对象数组
        if (value !== null && value !== '') {
          pItem = `${item}=${value}`
        }
      } else {
        for (let i = 0; i < value.length; i++) {
          if (value[i] !== null && value[i] !== '') {
            pItem = `${item}=${value.join(',')}`
          }
        }
      }
      if (pItem) {
        params.push(pItem)
      }
    }
    return params.join('&')
  },
  /**
   * 移除get参数 & 和 =
   * @param { String } str get参数请求部分
   * @return { String } 参数字符串
   */
  removeStrEqual (str) {
    if (!str) return
    return str.replace(/[&=]/g, '')
  },
  /**
   * 获取字符串中的key
   * @param { String } str get参数请求部分
   * @return { String } get参数中的key部分
   */
  getParamNamesLink (str) {
    if (!str) return
    const paramsItem = str.split('&')
    const result = []
    for (let i = 0; i < paramsItem.length; i++) {
      result.push(paramsItem[i].split('=')[0])
    }
    return result.join(',')
  },
  // 验证时间控件值是否为空
  dateRangeNull (dr) {
    return !dr || (!dr[0] && !dr[1])
  }
}

// 过滤一维对象中值为空
export function filterObjValNil (objParams) {
  if (isEmpty(objParams)) return Object.create({})

  /* eslint-disable */
  const objectToPairs = obj => Object.keys(obj).map(k => [k, obj[k]])
  const objectFromPairs = arr => arr.reduce((a, v) => ((a[v[0]] = v[1]), a), {})
  /* eslint-enable */

  let t = objectToPairs(objParams)
  t = t.filter((item) => {
    if (`${item[1]}` === '0' || item[1]) return item
  })
  return objectFromPairs(t)
}

// [], {} // 空数组，空对象
export function isNone (val) {
  return val !== null && typeof val === 'object' && Object.keys(val).length === 0
}

// 0, -0, '', false, null, undefined, 0.00 // 否定
export function isNot (val) {
  return (val === null && typeof val === 'object') || !val
}

// 后台列表返回数组判断
export function listBack (data, rootPath, path, success, empty) {
  const tPath = objectFindVaule(data, path)
  if (!data.error) {
    if (!objectFindVaule(data, rootPath) || !tPath || tPath.length === 0) {
      if (typeof empty === 'function') {
        empty()
      }
    } else if (Array.isArray(tPath)) {
      success()
    }
  }
}

// 根据path来找到对应object的value值
export function objectFindVaule (obj, path) {
  return path.split('.').reduce((target, key) => target && target[key], obj)
}

// 多维数组平铺一维数组
export function deepFlatten (arr) {
  const df = (arr) => [].concat(...arr.map((v) => (Array.isArray(v) ? deepFlatten(v) : v)))
  return df(arr)
}

export default tools
