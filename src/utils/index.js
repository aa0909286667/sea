import Cookie from 'js-cookie'
import Settime from 'utils/settime'
import Message from 'core/packages/message/'

export function convertBase64UrlToBlob (urlData) {
  const arr = urlData.split(',')
  const mime = arr[0].match(/:(.*?);/)[1]
  const bar = atob(arr[1])
  let n = bar.length
  const u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bar.charCodeAt(n)
  }
  return new Blob([u8arr], {
    type: mime
  })
}

export function chunkArray (arr, num) {
  num = num * 1 || 1
  const ret = []
  arr.forEach((item, i) => {
    if (i % num === 0) {
      ret.push([])
    }
    ret[ret.length - 1].push(item)
  })
  return ret
}

export function queryFormatter (url) {
  let String = ''
  const Arr = url.substring(url.indexOf('?') + 1, url.length).split('&')
  Arr.forEach((v) => {
    const a = v.split('=')
    String += `"${a[0]}":"${a[1]}",`
  })
  String = `{${String.substr(0, String.lastIndexOf(','))}}`
  Cookie.set('PAUSEINFO', String)
  return JSON.parse(String)
}

export function parseTime (time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
      time = parseInt(time)
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time *= 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  return format.replace(/{([ymdhisa])+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = `0${value}`
    }
    return value || 0
  })
}

/**
 * @return {number}
 */
export function DayNumOfMonth (Year, Month) {
  return new Date(Year, Month, 0).getDate()
}

export function GetDateObj (n) {
  const dd = new Date()
  dd.setDate(dd.getDate() + n) // 获取n天后的日期
  const y = dd.getFullYear()
  const m = dd.getMonth() // 获取当前月份的日期
  const d = dd.getDate()
  return { y, m, d }
}

export function dd (time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{h}:{i}:{s}'
  const date = new Date(time)
  const formatObj = {
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  return format.replace(/{([ymdhisa])+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = `0${value}`
    }
    return value || 0
  })
}

export function formatTime (time, option) {
  if ((`${time}`).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  }
  if (diff < 3600) {
    // less 1 hour
    return `${Math.ceil(diff / 60)}分钟前`
  }
  if (diff < 3600 * 24) {
    return `${Math.ceil(diff / 3600)}小时前`
  }
  if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  }
  return `${d.getMonth() + 1}月${d.getDate()}日${d.getHours()}时${d.getMinutes()}分`
}

// 格式化时间
export function getQueryObject (url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

/**
 * @param {Sting} input value
 * @returns {number} output value
 */
export function byteLength (str) {
  // returns the byte length of an utf8 string
  let s = str.length
  for (let i = str.length - 1; i >= 0; i--) {
    const code = str.charCodeAt(i)
    if (code > 0x7f && code <= 0x7ff) {
      s++
    } else if (code > 0x7ff && code <= 0xffff) s += 2
    if (code >= 0xdc00 && code <= 0xdfff) i--
  }
  return s
}

export function cleanArray (actual) {
  const newArray = []
  for (let i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i])
    }
  }
  return newArray
}

export function param (json) {
  if (!json) return ''
  return cleanArray(
    Object.keys(json).map((key) => {
      if (json[key] === undefined) return ''
      return `${encodeURIComponent(key)}=${encodeURIComponent(json[key])}`
    })
  ).join('&')
}

export function param2Obj (url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    `{"${
      decodeURIComponent(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"').replace(/\+/g, ' ')
    }"}`
  )
}

export function html2Text (val) {
  const div = document.createElement('div')
  div.innerHTML = val
  return div.textContent || div.innerText
}

export function objectMerge (target, source) {
  /* Merges two  objects,
   giving the last one precedence */

  if (typeof target !== 'object') {
    target = {}
  }
  if (Array.isArray(source)) {
    return source.slice()
  }
  Object.keys(source).forEach((property) => {
    const sourceProperty = source[property]
    if (typeof sourceProperty === 'object') {
      target[property] = objectMerge(target[property], sourceProperty)
    } else {
      target[property] = sourceProperty
    }
  })
  return target
}

export function toggleClass (element, className) {
  if (!element || !className) {
    return
  }
  let classString = element.className
  const nameIndex = classString.indexOf(className)
  if (nameIndex === -1) {
    classString += `${className}`
  } else {
    classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length)
  }
  element.className = classString
}

export const EnUSPickerOptions = {
  disabledDate: (time) => {
    const nowTime = new Date(Settime.RefTime())
    return (
      time.getTime() > new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 23, 59, 59).getTime() ||
      time.getTime() <
      new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 0, 0, 0).getTime() -
      3600 * 1000 * 24 * 89
    )
    // return time.getTime() < new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 0, 0, 0).getTime() - 3600 * 1000 * 24 * 45
  },
  shortcuts: [
    {
      text: '最近一周',
      onClick (picker) {
        const end = new Date(Settime.RefTime())
        const start = new Date(Settime.RefTime())
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
        picker.$emit('pick', [start, end])
      }
    },
    {
      text: '最近一个月',
      onClick (picker) {
        const end = new Date(Settime.RefTime())
        const start = new Date(Settime.RefTime())
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
        picker.$emit('pick', [start, end])
      }
    },
    {
      text: '最近三个月',
      onClick (picker) {
        const end = new Date(Settime.RefTime())
        const start = new Date(Settime.RefTime())
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 89)
        picker.$emit('pick', [start, end])
      }
    }
  ]
}
export const EnUSPicker40Options = {
  disabledDate: (time) => {
    const nowTime = new Date(Settime.RefTime())
    return (
      time.getTime() > new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 23, 59, 59).getTime() ||
      time.getTime() <
      new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 0, 0, 0).getTime() -
      3600 * 1000 * 24 * 40
    )
  },
  shortcuts: [
    {
      text: '最近一周',
      onClick (picker) {
        const end = new Date(Settime.RefTime())
        const start = new Date(Settime.RefTime())
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
        picker.$emit('pick', [start, end])
      }
    },
    {
      text: '最近一个月',
      onClick (picker) {
        const end = new Date(Settime.RefTime())
        const start = new Date(Settime.RefTime())
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
        picker.$emit('pick', [start, end])
      }
    }
  ]
}
export const BGPickerOptions = {
  disabledDate: (time) => {
    const nowTime = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
    return (
      time.getTime() > new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 23, 59, 59).getTime() ||
      time.getTime() <
      new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate(), 0, 0, 0).getTime() -
      3600 * 1000 * 24 * 89
    )
  },
  shortcuts: [
    {
      text: '最近一周',
      onClick (picker) {
        const end = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        const start = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
        picker.$emit('pick', [start, end])
      }
    },
    {
      text: '最近一个月',
      onClick (picker) {
        const end = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        const start = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
        picker.$emit('pick', [start, end])
      }
    },
    {
      text: '最近三个月',
      onClick (picker) {
        const end = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        const start = new Date(new Date(Settime.RefTime()).getTime() + 12 * 3600 * 1000)
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 89)
        picker.$emit('pick', [start, end])
      }
    }
  ]
}

export function getTime (type) {
  if (type === 'start') {
    return new Date().getTime() - 3600 * 1000 * 24 * 89
  }
  return new Date(new Date().toDateString())
}

export function debounce (func, wait, immediate) {
  let timeout,
      args,
      context,
      timestamp,
      result

  const later = function () {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔 last 小于设定时间间隔 wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function (...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}

/**
 * This is just a simple version of deep copy
 * Has a lot of edge cases bug
 * If you want to use a perfect deep copy, use lodash's _.cloneDeep
 */
export function deepClone (source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach((keys) => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * 分转元
 * @param n
 * @returns {string}
 * By zyx
 */
export function formatCent (n) {
  let _n = n.toString()
  switch (_n.length) {
    case 0:
      _n = '000'
      break
    case 1:
      _n = `00${_n}`
      break
    case 2:
      _n = `0${_n}`
      break
  }
  _n = `${_n.slice(0, _n.length - 2)}.${_n.slice(_n.length - 2)}`
  return _n
}

/*
 * 保留两位小数
 * @param v
 * @returns {string}
 * */
export function dotFormatter (v) {
  let t = v.toString()
  // 先把非数字的都替换掉，除了数字和.
  t = t.replace(/[^\d.]/g, '')
  // 只允许一个小数点
  t = t.replace(/^\./g, '').replace(/\.{2,}/g, '.')
  // 只能输入小数点后两位
  t = t.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.')
  t = t.replace(/^(-)*(\d+)\.(\d\d).*$/, '$1$2.$3')
  return t
}

/*
 * 小数点结尾自动补零
 * @param v 值
 * @param n 小数点几位
 * @returns {string}
 */
export function zeroFill (v, n) {
  let a,
      i
  a = v.toString()
  const b = a.indexOf('.')
  const c = a.length
  if (n === 0) {
    if (b !== -1) {
      a = a.substring(0, b)
    }
  } else {
    // 没数点
    if (b === -1) {
      a += '.'
      for (i = 1; i <= n; i++) {
        a += '0'
      }
    } else {
      // 数点超位数自截取否则补0
      a = a.substring(0, b + n + 1)
      for (i = c; i <= b + n; i++) {
        a += '0'
      }
    }
  }
  return a
}

// 浮点数加法运算
/**
 * @return {number}
 */
export function FloatAdd (arg1, arg2) {
  let r1,
      r2
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) {
    r2 = 0
  }
  const m = Math.pow(10, Math.max(r1, r2))
  console.log(m)
  return Math.floor(arg1 * m + arg2 * m) / m
}

// 浮点数减法运算
/**
 * @return {string}
 */
export function FloatSub (arg1, arg2) {
  let r1,
      r2
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) {
    r2 = 0
  }
  const m = Math.pow(10, Math.max(r1, r2))
  // 动态控制精度长度
  const n = r1 === r2 ? r1 : r2
  return ((arg1 * m - arg2 * m) / m).toFixed(n)
}

// 时间跨度不超过3个自然月
export function check3Month (start, end) {
  const begintime = parseTime(start)
  const endtime = parseTime(end)
  // 判断时间跨度是否大于3个月
  const arr1 = begintime.split('-')
  const arr2 = endtime.split('-')
  arr1[1] = parseInt(arr1[1])
  arr1[2] = parseInt(arr1[2])
  arr2[1] = parseInt(arr2[1])
  arr2[2] = parseInt(arr2[2])
  let flag = true
  // 同年
  if (arr1[0] === arr2[0]) {
    if (arr2[1] - arr1[1] > 3) {
      // 月间隔超过3个月
      flag = false
    } else if (arr2[1] - arr1[1] === 3) {
      // 月相隔3个月，比较日
      if (arr2[2] > arr1[2]) {
        // 结束日期的日大于开始日期的日
        flag = false
      }
    }
  } else {
    // 不同年
    if (arr2[0] - arr1[0] > 1) {
      flag = false
    } else if (arr2[0] - arr1[0] === 1) {
      if (arr1[1] < 10) {
        // 开始年的月份小于10时，不需要跨年
        flag = false
      } else if (arr1[1] + 3 - arr2[1] < 12) {
        // 月相隔大于3个月
        flag = false
      } else if (arr1[1] + 3 - arr2[1] === 12) {
        // 月相隔3个月，比较日
        if (arr2[2] > arr1[2]) {
          // 结束日期的日大于开始日期的日
          flag = false
        }
      }
    }
  }
  if (!flag) {
    Message({
      type: 'error',
      message: '开始时间和结束时间之间的跨度最大不超过3个月',
      duration: 3 * 1000,
      showClose: true
    })
    return false
  }
  return true
}

// 把'hh:mm:ss'格式字符串转化为date类型
export function toDateWithOutTimeZone (date) {
  const tempTime = date.split(':')
  const dt = new Date()
  dt.setHours(tempTime[0])
  dt.setMinutes(tempTime[1])
  dt.setSeconds(tempTime[2])
  return dt
}

export function uniqueArr (arr) {
  return Array.from(new Set(arr))
}

export function createUniqueString () {
  const timestamp = `${+new Date()}`
  const randomNum = `${parseInt((1 + Math.random()) * 65536)}`
  return (+(randomNum + timestamp)).toString(32)
}

export function hasClass (ele, cls) {
  return !!ele.className.match(new RegExp(`(\\s|^)${cls}(\\s|$)`))
}

export function addClass (ele, cls) {
  if (!hasClass(ele, cls)) ele.className += ` ${cls}`
}

export function removeClass (ele, cls) {
  if (hasClass(ele, cls)) {
    const reg = new RegExp(`(\\s|^)${cls}(\\s|$)`)
    ele.className = ele.className.replace(reg, ' ')
  }
}

/* 随机数范围 */
export function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

// 生成不重复数组
export function getRandomArray (array, length, flag) {
  const newArray = []
  for (let i = 0; i < array.length; i++) {
    newArray.push(array[i])
  }

  const arrayBack = []
  for (let i = 0; i < length; i++) {
    const random = getRandomInt(0, newArray.length)
    const randomNum = flag
      ? parseInt(newArray[random]) < 10
        ? `0${parseInt(newArray[random])}`
        : parseInt(newArray[random])
      : parseInt(newArray[random])
    arrayBack.push(randomNum.toString())
    newArray.splice(random, 1)
  }
  return arrayBack
}

// 生成可重复数组
export function getCanRepeatArray (array, length, flag) {
  const arrayBack = []
  for (let i = 0; i < length; i++) {
    const random = getRandomInt(0, array.length)
    const randomNum = flag
      ? parseInt(array[random]) < 10
        ? `0${parseInt(array[random])}`
        : parseInt(array[random])
      : parseInt(array[random])
    arrayBack.push(randomNum.toString())
  }
  return arrayBack
}

/**
 *
 * @param beginMonth 开始月份
 * @param n 间隔月份数
 * @returns {string}
 */
export function getBetweenMonth (beginMonth, n) {
  let strYear = parseInt(beginMonth.substr(0, 4), 10)
  let strMonth = parseInt(beginMonth.substr(4, 6), 10)
  console.log(strMonth)
  console.log(strYear)
  if (strMonth - n <= 0) {
    strYear -= 1
    strMonth = strMonth - n + 12
  } else {
    strMonth -= n
  }
  if (strMonth < 10) {
    strMonth = `0${strMonth}`
  }
  return `${strYear}${strMonth}`
}

/**
 *
 * @param x
 * @param y
 * @returns {string}
 */
export function date2str (x, y) {
  // eslint-disable-next-line no-unused-vars
  const z = {
    M: x.getMonth() + 1, d: x.getDate(), h: x.getHours(), m: x.getMinutes(), s: x.getSeconds()
  }
  y = y.replace(/(M+|d+|h+|m+|s+)/g, (v) =>
    // eslint-disable-next-line no-eval
    ((v.length > 1 ? '0' : '') + eval(`z.${v.slice(-1)}`)).slice(-2))
  return y.replace(/(y+)/g, (v) => x.getFullYear().toString().slice(-v.length))
}

/**
 * 进制转换工具
 * @param num 转换的数值
 * @param fromRadix 原进制
 * @param toRadix 待转换的进制
 */
export function radixConvert (num, fromRadix, toRadix) {
  return parseInt(num, fromRadix).toString(toRadix)
}

/**
 * 进制转换指定长度进制
 * @param num 转换的数值
 * @param fromRadix 原进制
 * @param toRadix 待转换的进制
 * @param len 指定长度
 * @returns {string}
 */
export function radixConvertForLength (num, fromRadix, toRadix, len = 3) {
  let n = parseInt(num, fromRadix).toString(toRadix)
  let l = n.length
  while (l < len) {
    n = `0${n}`
    l++
  }
  return n
}
