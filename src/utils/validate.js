/* 验证空格 */
export function reg (str) {
  const reg = /\s/
  return reg.test(str)
}

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal (path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/* 验证key */
export function validateKey (str) {
  const reg = /^[a-z_\-:]+$/
  return reg.test(str)
}

/**
 * @param {string} url
 * @returns {Boolean}
 */
export function validateURL (url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validateLowerCase (str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validateUpperCase (str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validatAlphabets (str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/**
 * @param {string} email
 * @returns {Boolean}
 */
export function validEmail (email) {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return reg.test(email)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function isString (str) {
  return typeof str === 'string' || str instanceof String
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isArray (arg) {
  if (typeof Array.isArray === 'undefined') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return Array.isArray(arg)
}

/* 数字和英文 */
export function validatAlphabetsAndNumber (str) {
  const reg = /^[A-Za-z0-9]+$/
  return reg.test(str)
}

// 校验整数
export function validatNumber (val) {
  const reg_expression = /^\d+$/
  return reg_expression.test(val)
}

export function validateNum (val) {
  const reg = /^[0-9]+$/
  return reg.test(val)
}

export function validataFloat (val) {
  const reg = /^\d+(\.\d{2})?$/
  return reg.test(val)
}

// 中英文
export function vChineseOrEnglish (v) {
  const reg = /^[\u4e00-\u9fa5a-zA-Z0-9]*$/g
  return reg.test(v)
}

// 校验手机号码
export function validateMobile (v) {
  const reg = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/
  return reg.test(v)
}

// 校验日期
export function validateDate (str) {
  const reg = /^[1-2][0-9][0-9][0-9]-[0-1]{0,1}[0-9]-[0-3]{0,1}[0-9]$/
  return reg.test(str.trim())
}

// 校验三围
export function validateBwh (str) {
  const reg = /^[0-9][0-9]{1,2}-[0-9][0-9]-[0-9][0-9]$/
  return reg.test(str.trim())
}

// 开奖号码校验
export function vOpenCode (v) {
  const reg = /^(\d+[,])*(\d+)$/
  return reg.test(v)
}

// 中英文数字验证
export function stripScript (v) {
  const reg = /^[\d|A-z|\u4E00-\u9FFF]+$/g
  return reg.test(v)
}

// 验证邮编号
export function validateCityCode (str) {
  const reg = /^[1-9]\d{5}(?!\d)$/
  return reg.test(str.trim())
}

// 只能由英文、数字、下划线组成
export function validateNORE (str) {
  const reg = /^\w+$/
  return reg.test(str.trim())
}

// 校验至多两位小数
export function vDotNumber (v) {
  const reg = /^\d+(\.\d{0,2})?$/
  return reg.test(v)
}

// 校验ip
export function validateIP (v) {
  const reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
  return reg.test(v)
}

// 校验纯中文
export function validateChina (v) {
  const reg = /[\u4e00-\u9fa5]/gm
  return reg.test(v)
}

