/**
 * 随机数范围[min, max)
 * @param min
 * @param max
 * @returns {*}
 */
const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min

/**
 * 随机数 时间戳去掉前两位 + 随机三位数
 * @returns {string}
 */
function genIdentity () {
  return `${new Date().getTime().toString().slice(2)}${getRandomInt(100, 1000)}`
}

export { genIdentity }
