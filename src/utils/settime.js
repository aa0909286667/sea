const bgSetTime = {
  RefTime: () => {
    const dd2 = new Date()
    const wn = bgSetTime.getMonthWeekNumber()
    const dm = dd2.getMonth() + 1
    let xl = 240
    if (dm > 3 && dm < 11) {
      xl = 240
    }
    if (dm === 3 && wn >= 2) {
      xl = 240
    }
    const fff = bgSetTime.getFt()
    if (dm === 11 && (dd2.getTime() - fff.getTime() <= 0)) {
      xl = 240
    }
    dd2.setMinutes(dd2.getMinutes() + dd2.getTimezoneOffset() - xl)
    const y = dd2.getFullYear()
    const m = bgSetTime.fixNum(dd2.getMonth() + 1)
    const d = bgSetTime.fixNum(dd2.getDate())
    const h = bgSetTime.fixNum(dd2.getHours())
    const mm = bgSetTime.fixNum(dd2.getMinutes())
    const s = bgSetTime.fixNum(dd2.getSeconds())
    return `${y}-${m}-${d} ${h}:${mm}:${s}`
  },
  getFt: () => {
    const d = new Date()
    d.setHours(0, 0, 0, 0)
    let didx = 1
    d.setMonth(10, didx)
    let md = d.getDay()
    while (md !== 0) {
      didx += 1
      d.setMonth(10, didx)
      md = d.getDay()
    }
    return d
  },
  getMdTime: () => {
    const dd2 = new Date()
    const wn = bgSetTime.getMonthWeekNumber()
    const dm = dd2.getMonth() + 1
    let xl = 240
    if (dm > 3 && dm < 11) {
      xl = 240
    }
    if (dm === 3 && wn >= 2) {
      xl = 240
    }
    const fff = bgSetTime.getFt()
    if (dm === 11 && (dd2.getTime() - fff.getTime() <= 0)) {
      xl = 240
    }
    dd2.setMinutes(dd2.getMinutes() + dd2.getTimezoneOffset() - xl)
    const y = dd2.getFullYear()
    const m = bgSetTime.fixNum(dd2.getMonth() + 1)
    const d = bgSetTime.fixNum(dd2.getDate())
    return `${y}-${m}-${d}`
  },
  fixNum: (num) => {
    let t = parseInt(num)
    if (t < 0) {
      t *= -1
    }
    return t < 10 ? `0${t}` : t
  },
  getYearWeekNumber: () => {
    const now = new Date()
    const year = now.getFullYear()
    const month = now.getMonth()
    let days = now.getDate()
    for (let i = 0; i < month; i++) {
      days += bgSetTime.getMonthDays(year, i)
    }
    const yearFirstDay = new Date(year, 0, 1).getDay() || 7
    let week = null
    if (yearFirstDay === 1) {
      week = Math.ceil(days / yearFirstDay)
    } else {
      days -= 7 - yearFirstDay + 1
      week = Math.ceil(days / 7) + 1
    }
    return week
  },
  getMonthWeekNumber: () => {
    const now = new Date()
    const a = now.getFullYear()
    const b = now.getMonth() + 1
    const c = now.getDate()
    const date = new Date(a, parseInt(b) - 1, c)
    const w = date.getDay()
    const d = date.getDate()
    return Math.ceil((d + 6 - w) / 7)
  },
  isLeapYear: (year) => (year % 400 === 0) || (year % 4 === 0 && year % 100 !== 0),
  getMonthDays: (year, month) => [31, null, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month] || (bgSetTime.isLeapYear(year) ? 29 : 28)
}

export default bgSetTime
