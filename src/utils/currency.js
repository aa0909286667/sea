/*
  人民币(CNY)、泰铢(THB)、印尼盾(IDR/RP)、
  马币(MYR/RM)、越南盾1：1000(VND)、韩元(KRW)、
  新加坡币(SGD)、新西兰币(NZD)、澳币(AUD)、
  日圆(JPY）、美元（USD）、印度卢比（INR）、
  英磅(GBP)、俄罗斯卢布(RUB)、缅甸元(MMK/BUK)、
  加拿大元（CAD）、哥斯达黎加科朗（CRC）、比特币(BTC)、
  赞比亚克瓦查(ZMK)、越南盾1：1(VND)、欧元（EUR）、
  泰达币(USDT)
*/
export const currencyList = [
  { label: '人民币(CNY)', value: 'CNY' },
  { label: '泰铢(THB)', value: 'THB' },
  { label: '印尼盾(IDR)', value: 'IDR' },
  { label: '马币(MYR)', value: 'MYR' },
  { label: '越南盾(VND)', value: 'VND' },
  { label: '韩元(KRW)', value: 'KRW' },
  { label: '新加坡币(SGD)', value: 'SGD' },
  { label: '新西兰币(NZD)', value: 'NZD' },
  { label: '澳币(AUD)', value: 'AUD' },
  { label: '日圆(JPY)', value: 'JPY' },
  { label: '美元(USD)', value: 'USD' },
  { label: '印度卢比(INR)', value: 'INR' },
  { label: '英磅(GBP)', value: 'GBP' },
  { label: '俄罗斯卢布(RUB)', value: 'RUB' },
  { label: '缅甸元(MMK)', value: 'MMK' },
  { label: '加拿大元(CAD)', value: 'CAD' },
  { label: '哥斯达黎加科朗(CRC)', value: 'CRC' },
  { label: '比特币(BTC)', value: 'BTC' },
  { label: '赞比亚克瓦查(ZMK)', value: 'ZMK' },
  { label: '欧元(EUR)', value: 'EUR' },
  { label: '泰达币(USDT)', value: 'USDT' }
]
