export const replaceInput = (value, limited, maxNumInt, maxNumFloat, useMinus = false) => {
  // value = value.replace(/^0*(0\.|[1-9])/, '$1') // 第一個字只能是數字
  const useMinusRegex = new RegExp(`[^\\d\\.${useMinus ? '\\-' : ''}]`)
  value = value.replace(useMinusRegex, '') // 清除數字和「.」、「-」以外的字符
  value = value.replace(/^\./g, '') // 驗證第一個字元是否為數字
  value = value.replace(/\.{1,}/g, '.') // 只保留第一个「.」，不能有0.0.1之類的非法數值
  value = value.replace(/^\-{1,}/g, '-') // 只保留第一个「-」
  value = value.replace(/^\-{1,}\./g, '-') // 只保留第一个「-」後避免「.」可以與之連接
  value = value.replace(/\.{1,}\-/g, '.') // 「.」的後面不能是「-」
  // 任何數字後不能是「-」
  value = value.replace(/(^\d{1,}|\.\d{1,}|^\-\d{1,})\-/g, '$1')
  value = value.replace(/[^\-]{1,}[^\-\d*\.]{1,}/g, '')
  value = value.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.')

  // 正整數部分限制最多位數
  let limitedNum = 0
  if (limited) {
    limitedNum = maxNumInt
  } else {
    // 等於value的長度代表無限制
    limitedNum = value.length
  }

  // 如果小數點位數為0時，則僅能輸入整數位數
  const floatRegex = new RegExp(`^(\\-)*(\\d*)\\.(\\d{${maxNumFloat}})\.\*$`)
  // 如果是負數，避免佔用字元數量限制
  const isMinus = new RegExp(/^-/g)
  isMinus.test(value) ? limitedNum++ : value

  value = value.replace(/\.{1,}/g, maxNumFloat === 0 ? '' : '.')
  value = value.replace(floatRegex, maxNumFloat === 0 ? '$1$2' : '$1$2.$3')
  value = value.indexOf('.') > 0 ? value.split('.')[0].substring(0, limitedNum) + '.' + value.split('.')[1] : value.substring(0, limitedNum)

  return value
}
