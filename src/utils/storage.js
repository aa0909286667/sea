import Cookies from 'js-cookie'

const storage_priffix = 'sys_'
const TokenKey = `${storage_priffix}Token`
const UserInfoKey = `${storage_priffix}USERINFO`
const PermissionKey = `${storage_priffix}PERMISSION`
const noPermissionKey = `${storage_priffix}NOPERMISSION`
const RoleKey = `${storage_priffix}Role`
const MenusKey = `${storage_priffix}Menus`
const qrInfoKey = `${storage_priffix}qrInfo`
const routesStorageKey = `${storage_priffix}routesStorage`

export function getRoutesStorage () {
  return JSON.parse(localStorage.getItem(routesStorageKey)) || []
}

export function removeRoutesStorage () {
  return localStorage.removeItem(routesStorageKey)
}

export function setRoutesStorage (routes) {
  return localStorage.setItem(routesStorageKey, JSON.stringify(routes))
}

export function getQrInfo () {
  return JSON.parse(localStorage.getItem(qrInfoKey)) || {}
}

export function setQrInfo (info) {
  return localStorage.setItem(qrInfoKey, JSON.stringify(info))
}

export function removeQrInfo () {
  return localStorage.removeItem(qrInfoKey)
}

export function getToken () {
  return Cookies.get(TokenKey)
}

export function getMenusInfo () {
  return localStorage.getItem(MenusKey) ? JSON.parse(localStorage.getItem(MenusKey)) : []
}

export function getUserRole () {
  return Cookies.get(RoleKey)
}

export function getPermissionInfo () {
  return localStorage.getItem(PermissionKey) ? JSON.parse(localStorage.getItem(PermissionKey)) : []
}

export function getNoPermissionInfo () {
  return localStorage.getItem(noPermissionKey) ? JSON.parse(localStorage.getItem(noPermissionKey)) : []
}

export function getUserInfo () {
  return Cookies.get(UserInfoKey) ? JSON.parse(Cookies.get(UserInfoKey)) : {}
}

export function setToken (token) {
  return Cookies.set(TokenKey, token)
}

export function setRole (role) {
  return Cookies.set(RoleKey, role)
}

export function setPermissionInfo (info) {
  return localStorage.setItem(PermissionKey, info)
}

export function setNoPermissionInfo (info) {
  return localStorage.setItem(noPermissionKey, info)
}

export function setMenusInfo (info) {
  return localStorage.setItem(MenusKey, info)
}

export function setUserInfo (info) {
  return Cookies.set(UserInfoKey, info)
}

export function removeToken () {
  return Cookies.remove(UserInfoKey)
}

export function removeUserInfoKey () {
  return Cookies.remove(TokenKey)
}

export function removeUserRole () {
  return Cookies.remove(RoleKey)
}

export function removePermissionInfo () {
  return localStorage.removeItem(PermissionKey)
}

export function removeMenusInfo () {
  return localStorage.removeItem(MenusKey)
}

export function removeNoPermission () {
  return localStorage.removeItem(noPermissionKey)
}

export function removeAll () {
  removeNoPermission()
  removeMenusInfo()
  removePermissionInfo()
  removeUserInfoKey()
  removeToken()
  removeUserRole()
  removeQrInfo()
  removeRoutesStorage()
  localStorage.clear()
}
