/* eslint-disable */
// @log delete setDefault(headers, 'X-Requested-With', 'XMLHttpRequest')
!function(t,e){function n(t){return t&&e.XDomainRequest&&!/MSIE 1/.test(navigator.userAgent)?new XDomainRequest:e.XMLHttpRequest?new XMLHttpRequest:void 0}function o(t,e,n){t[e]=t[e]||n}var r=["responseType","withCredentials","timeout","onprogress"];t.ajax=function(t,a){function s(t,e){return function(){c||(a(void 0===f.status?t:f.status,0===f.status?"Error":f.response||f.responseText||e,f),c=!0)}}var u=t.headers||{},i=t.body,d=t.method||(i?"POST":"GET"),c=!1,f=n(t.cors);f.open(d,t.url,!0);var l=f.onload=s(200);f.onreadystatechange=function(){4===f.readyState&&l()},f.onerror=s(null,"Error"),f.ontimeout=s(null,"Timeout"),f.onabort=s(null,"Abort"),i&&(e.FormData&&i instanceof e.FormData||o(u,"Content-Type","application/x-www-form-urlencoded"));for(var p,m=0,v=r.length;v>m;m++)p=r[m],void 0!==t[p]&&(f[p]=t[p]);for(var p in u)f.setRequestHeader(p,u[p]);return f.send(i),f},e.nanoajax=t}({},function(){return this}());
/* 参考实现jQuery.param序列化方法 */
(function(a){var b=a.Tools||{},toString={}.toString;b.stringify=function(f){var e=[];for(var c in f){d(c,f[c],function(h,g){var i=typeof g==="function"?g():g;e[e.length]=encodeURIComponent(h)+"="+encodeURIComponent(i==null?"":i)})}function d(g,k,j){if(toString.apply(k)==="[object Array]"){for(var h=0;h<k.length;h++){if(/\[\]$/.test(g)){j(g,k[h])}else{d(g+"["+(typeof k[h]==="object"&&k[h]!=null?h:"")+"]",k[h])}}}else{if(toString.apply(k)==="[object Object]"){for(name in k){d(g+"["+name+"]",k[name],j)}}else{j(g,k)}}}return e.join("&")};a.Tools=b})(window);
/* $_cookie */
(function(w){w.$_cookie={get:function(name,options){this.validateCookieName(name);if(typeof options==="function"){options={converter:options}}else{options=options||{}}var cookies=this.parseCookieString(document.cookie,!options["raw"]);return(options.converter||this.same)(cookies[name])},set:function(name,value,options){this.validateCookieName(name);options=options||{};var expires=options["expires"];var domain=options["domain"];var path=options["path"];if(!options["raw"]){value=encodeURIComponent(String(value))}var text=name+"="+value;var date=expires;if(typeof date==="number"){date=new Date();date.setDate(date.getDate()+expires)}if(date instanceof Date){text+="; expires="+date.toUTCString()}if(this.isNonEmptyString(domain)){text+="; domain="+domain}if(this.isNonEmptyString(path)){text+="; path="+path}if(options["secure"]){text+="; secure"}document.cookie=text;return text},remove:function(name,options){options=options||{};options["expires"]=new Date(0);return this.set(name,"",options)},parseCookieString:function(text,shouldDecode){var cookies={};if(this.isString(text)&&text.length>0){var decodeValue=shouldDecode?decodeURIComponent:this.same;var cookieParts=text.split(/;\s/g);var cookieName;var cookieValue;var cookieNameValue;for(var i=0,len=cookieParts.length;i<len;i++){cookieNameValue=cookieParts[i].match(/([^=]+)=/i);if(cookieNameValue instanceof Array){try{cookieName=decodeURIComponent(cookieNameValue[1]);cookieValue=decodeValue(cookieParts[i].substring(cookieNameValue[1].length+1))}catch(ex){}}else{cookieName=decodeURIComponent(cookieParts[i]);cookieValue=""}if(cookieName){cookies[cookieName]=cookieValue}}}return cookies},isString:function(o){return typeof o==="string"},isNonEmptyString:function(s){return this.isString(s)&&s!==""},validateCookieName:function(name){if(!this.isNonEmptyString(name)){throw new TypeError("Cookie name must be a non-empty string")}},same:function(s){return s}}})(window);
/* 获取URL参数 */
(function(w){w.$_getUrlParams=function(name){var reg=new RegExp('(^|&)'+name+'=([^&]*)(&|$)');var r=location.href.substring(location.href.indexOf('?')).substr(1).match(reg);if(r!==null){return decodeURIComponent(r[2])}return null}})(window);
/* (safari < 10) 无痕兼容 - implement memory store spec'd to Storage prototype */
(function(w){var items={};function MemoryStorage(){}MemoryStorage.prototype.setItem=function(key,value){items[key]=value};MemoryStorage.prototype.getItem=function(key){return(items[key]?items[key]:null)};MemoryStorage.prototype.removeItem=function(key){return(delete items[key])};MemoryStorage.prototype.key=function(index){return Object.keys(items)[index]};MemoryStorage.prototype.get=function(){return items};MemoryStorage.prototype.clear=function(){return(items={})};Object.defineProperty(MemoryStorage.prototype,"length",{get:function length(){return Object.keys(items).length}});w.MemoryStorage=MemoryStorage})(window);
(function(w){var x="__storage_test__";try{w.localStorage.setItem(x,x);w.localStorage.removeItem(x)}catch(err){var memoryStorage=new w.MemoryStorage();Storage.prototype.setItem=memoryStorage.setItem.bind(memoryStorage);Storage.prototype.getItem=memoryStorage.getItem.bind(memoryStorage);Storage.prototype.removeItem=memoryStorage.removeItem.bind(memoryStorage);Storage.prototype.clear=memoryStorage.clear.bind(memoryStorage)}})(window);
// https://github.com/then/promise
!function n(t,e,r){function o(u,f){if(!e[u]){if(!t[u]){var c="function"==typeof require&&require;if(!f&&c)return c(u,!0);if(i)return i(u,!0);var s=new Error("Cannot find module '"+u+"'");throw s.code="MODULE_NOT_FOUND",s}var l=e[u]={exports:{}};t[u][0].call(l.exports,function(n){var e=t[u][1][n];return o(e?e:n)},l,l.exports,n,t,e,r)}return e[u].exports}for(var i="function"==typeof require&&require,u=0;u<r.length;u++)o(r[u]);return o}({1:[function(n,t,e){"use strict";function r(){}function o(n){try{return n.then}catch(t){return d=t,w}}function i(n,t){try{return n(t)}catch(e){return d=e,w}}function u(n,t,e){try{n(t,e)}catch(r){return d=r,w}}function f(n){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof n)throw new TypeError("not a function");this._37=0,this._12=null,this._59=[],n!==r&&v(n,this)}function c(n,t,e){return new n.constructor(function(o,i){var u=new f(r);u.then(o,i),s(n,new p(t,e,u))})}function s(n,t){for(;3===n._37;)n=n._12;return 0===n._37?void n._59.push(t):void y(function(){var e=1===n._37?t.onFulfilled:t.onRejected;if(null===e)return void(1===n._37?l(t.promise,n._12):a(t.promise,n._12));var r=i(e,n._12);r===w?a(t.promise,d):l(t.promise,r)})}function l(n,t){if(t===n)return a(n,new TypeError("A promise cannot be resolved with itself."));if(t&&("object"==typeof t||"function"==typeof t)){var e=o(t);if(e===w)return a(n,d);if(e===n.then&&t instanceof f)return n._37=3,n._12=t,void h(n);if("function"==typeof e)return void v(e.bind(t),n)}n._37=1,n._12=t,h(n)}function a(n,t){n._37=2,n._12=t,h(n)}function h(n){for(var t=0;t<n._59.length;t++)s(n,n._59[t]);n._59=null}function p(n,t,e){this.onFulfilled="function"==typeof n?n:null,this.onRejected="function"==typeof t?t:null,this.promise=e}function v(n,t){var e=!1,r=u(n,function(n){e||(e=!0,l(t,n))},function(n){e||(e=!0,a(t,n))});e||r!==w||(e=!0,a(t,d))}var y=n("asap/raw"),d=null,w={};t.exports=f,f._99=r,f.prototype.then=function(n,t){if(this.constructor!==f)return c(this,n,t);var e=new f(r);return s(this,new p(n,t,e)),e}},{"asap/raw":4}],2:[function(n,t,e){"use strict";function r(n){var t=new o(o._99);return t._37=1,t._12=n,t}var o=n("./core.js");t.exports=o;var i=r(!0),u=r(!1),f=r(null),c=r(void 0),s=r(0),l=r("");o.resolve=function(n){if(n instanceof o)return n;if(null===n)return f;if(void 0===n)return c;if(n===!0)return i;if(n===!1)return u;if(0===n)return s;if(""===n)return l;if("object"==typeof n||"function"==typeof n)try{var t=n.then;if("function"==typeof t)return new o(t.bind(n))}catch(e){return new o(function(n,t){t(e)})}return r(n)},o.all=function(n){var t=Array.prototype.slice.call(n);return new o(function(n,e){function r(u,f){if(f&&("object"==typeof f||"function"==typeof f)){if(f instanceof o&&f.then===o.prototype.then){for(;3===f._37;)f=f._12;return 1===f._37?r(u,f._12):(2===f._37&&e(f._12),void f.then(function(n){r(u,n)},e))}var c=f.then;if("function"==typeof c){var s=new o(c.bind(f));return void s.then(function(n){r(u,n)},e)}}t[u]=f,0===--i&&n(t)}if(0===t.length)return n([]);for(var i=t.length,u=0;u<t.length;u++)r(u,t[u])})},o.reject=function(n){return new o(function(t,e){e(n)})},o.race=function(n){return new o(function(t,e){n.forEach(function(n){o.resolve(n).then(t,e)})})},o.prototype["catch"]=function(n){return this.then(null,n)}},{"./core.js":1}],3:[function(n,t,e){"use strict";function r(){if(c.length)throw c.shift()}function o(n){var t;t=f.length?f.pop():new i,t.task=n,u(t)}function i(){this.task=null}var u=n("./raw"),f=[],c=[],s=u.makeRequestCallFromTimer(r);t.exports=o,i.prototype.call=function(){try{this.task.call()}catch(n){o.onerror?o.onerror(n):(c.push(n),s())}finally{this.task=null,f[f.length]=this}}},{"./raw":4}],4:[function(n,t,e){(function(n){"use strict";function e(n){f.length||(u(),c=!0),f[f.length]=n}function r(){for(;s<f.length;){var n=s;if(s+=1,f[n].call(),s>l){for(var t=0,e=f.length-s;e>t;t++)f[t]=f[t+s];f.length-=s,s=0}}f.length=0,s=0,c=!1}function o(n){var t=1,e=new a(n),r=document.createTextNode("");return e.observe(r,{characterData:!0}),function(){t=-t,r.data=t}}function i(n){return function(){function t(){clearTimeout(e),clearInterval(r),n()}var e=setTimeout(t,0),r=setInterval(t,50)}}t.exports=e;var u,f=[],c=!1,s=0,l=1024,a=n.MutationObserver||n.WebKitMutationObserver;u="function"==typeof a?o(r):i(r),e.requestFlush=u,e.makeRequestCallFromTimer=i}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],5:[function(n,t,e){"function"!=typeof Promise.prototype.done&&(Promise.prototype.done=function(n,t){var e=arguments.length?this.then.apply(this,arguments):this;e.then(null,function(n){setTimeout(function(){throw n},0)})})},{}],6:[function(n,t,e){n("asap");"undefined"==typeof Promise&&(Promise=n("./lib/core.js"),n("./lib/es6-extensions.js")),n("./polyfill-done.js")},{"./lib/core.js":1,"./lib/es6-extensions.js":2,"./polyfill-done.js":5,asap:3}]},{},[6]);

var ASSETSDIR = '/seasys/'

var initLottery = {
  apiEnv: 'seasys',
  configsHttp: 'CONFIGSHTTP',
  configsHttps: 'CONFIGSHTTPS',
  protocol: window.location.protocol,
  configs: [],
  start: function() {
    if (this.utils.oldBrowser()) {
      alert('您使用的浏览器版本过低，请升级浏览器')
    }
    var protocol = window.location.protocol
    this.configs = protocol === 'https:' ? this.configsHttps : this.configsHttp
    this.configs = typeof this.configs === 'string' ? JSON.parse(this.configs) : this.configs
    this.tryConf(0)
  },
  sendAjax: function(url, next) {
    this.utils.xhr(
      {
        url: this.protocol + '//' + (url === '.' ? window.location.host : url) + '/ocs/' + this.apiEnv + '?ts=' + Date.now(),
        timeout: window.ocsTimeout || 3000
      },
      function(code, res, req) {
        if (code === 200) {
          var result
          try {
            result = JSON.parse(res)
          } catch (e) {
            // this.tryConf(next)
            this.allDown('无法读取配置，请联系管理员!!!')
            return
          }
          window.protocolHttpsForce = result.httpsForce === 1 ? 'https:' : window.location.protocol
          var protocol = window.protocolHttpsForce
          var _res = result[protocol.slice(0, -1)]
          // 处理数据 - 追加协议
          for (var k in _res) {
            var v = _res[k]
            if (v instanceof Array) {
              v = v.map(function(_v) {
                return protocol + '//' + (_v === '.' ? window.location.host : _v)
              })
              _res[k] = v
            }
          }
          window.verPath = result.versions
          window.API_DOMAINS = _res.API_DOMAINS
          this.utils.resetTimeout()
          this.utils.tryPing(_res.CDN_PATHS,function(cdn) {
            window.cdnPath = cdn
            this.setResourceURL(cdn, window.verPath)
            var loadingEL = document.getElementById('loading')
            if (loadingEL) loadingEL.style.display = 'none'
          }.bind(this),function() {
            this.allDown('加载网络资源失败, 重试请点击')
          }.bind(this), 5 * 1000)
          window.API_ADMINFRONT_PROD = _res.API_DOMAINS[0]
          for(var prop in result.config) {
            window[prop] = result.config[prop]
          }
        } else {
          this.tryConf(next)
        }
      }.bind(this)
    )
  },
  tryConf: function(idx) {
    var next = idx + 1
    this.utils.addTimeout()
    if (next > this.configs.length) {
      this.allDown('网速缓慢，重试请点击')
      return
    }
    this.utils.tryPing(this.configs,function(url) {
      this.sendAjax(url, next)
    }.bind(this), function() {
      this.tryConf(next)
    }.bind(this), sessionStorage.ocsTimeout)
  },
  appendToDom: function(pre, file, prop) {
    var el = document.createElement('script')
    el.src = pre + '/js/' + file
    el.async = false
    document[prop].appendChild(el)
  },
  setResourceURL: function(cdn, ver) {
    var prefix = cdn + ASSETSDIR + ver
    var linkFiles = ['globals', 'vendor', 'app']
    for (var i = 0; i < linkFiles.length; i++) {
      var link = document.createElement('link')
      link.href = prefix + '/css/' + linkFiles[i] + '.css'
      link.setAttribute('rel', 'stylesheet')
      document.head.appendChild(link)
    }

    var files = ['vue.dll.js?v2515250', 'manifest', 'globals', 'vendor', 'app']
    for (var i = 0; i < files.length; i++) {
      if (i > 0) {
        this.appendToDom(prefix, files[i] + '.js', 'body')
      } else {
        this.appendToDom(prefix, files[i], 'head')
      }
    }
  },
  utils: {
    xhr: window.nanoajax.ajax,
    oldBrowser: function() {
      var userAgent = navigator.userAgent // 取得浏览器的userAgent字符串
      var isOpera = userAgent.indexOf('Opera') > -1 // 判断是否Opera浏览器
      var isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1 && !isOpera // 判断是否IE浏览器
      if (isIE) {
        var reIE = new RegExp('MSIE (\\d+\\.\\d+);')
        reIE.test(userAgent)
        var fIEVersion = parseFloat(RegExp['$1'])
        if (fIEVersion < 10) {
          return 'IE'
        }
      } else {
        var reSa = new RegExp('Version/(\\d+)|Chrome/(\\d+)|Firefox/(\\d+)|Opera')
        reSa.test(userAgent)
        var fSaVersion = RegExp['$1']
        var fChVersion = RegExp['$2']
        var fFFVersion = RegExp['$3']
        var fOpVersion = RegExp['$4']

        if (fSaVersion && fSaVersion < 6) {
          return 'Safari'
        }
        if (fChVersion && fChVersion < 31) {
          return 'Chrome'
        }
        if (fFFVersion && fFFVersion < 31) {
          return 'Firefox'
        }
        if (fOpVersion) {
          return 'Opera'
        }
      }
    },
    isLSSupport: function() {
      try {
        var hasLS = 'localStorage' in window && window['localStorage'] !== null
        if (hasLS) {
          localStorage.setItem('__test__', 1)
          localStorage.removeItem('__test__')
        }
        return true
      } catch (err) {
        return false
      }
    },
    addTimeout: function() {
      if (this.isLSSupport()) {
        sessionStorage.ocsTimeout
          ? (sessionStorage.ocsTimeout = sessionStorage.ocsTimeout * 2 > 30 * 1000 ? 30 * 1000 : sessionStorage.ocsTimeout * 2)
          : (sessionStorage.ocsTimeout = window.ocsTimeout || 2000)
      }
    },
    resetTimeout: function() {
      if (this.isLSSupport()) {
        sessionStorage.ocsTimeout = ''
      }
    },
    tryPing: function(urls, onComplete, onError, timeout) {
      var inited = false
      var errCount = 0
      for (var i = 0; i < urls.length; i++) {
        getImg.call(this, i, urls)
      }

      function getImg(index, urls) {
        if (timeout > 30 * 1000) timeout = 30 * 1000
        var _url = urls[index]
        window.nanoajax.ajax(
          {
            url: (_url.indexOf('http') >= 0 ? '' : (!window.protocolHttpsForce ? window.location.protocol : window.protocolHttpsForce) + '//') + urls[index] + '/cc.png?' + Date.now(),
            method: 'get',
            timeout: timeout || sessionStorage.ocsTimeout
          },
          function(code, response, req) {
            if (code === 200 && response !== 'Error') {
              if (!inited) {
                inited = true
                onComplete(urls[index])
              }
            } else {
              errCount++
              if (errCount >= urls.length) {
                onError()
              }
            }
          }
        )
      }
    },
    tryAPIPing: function(urls, callback, errorCb) {
      var inited = false
      var errCount = 0
      var _loop = function(i) {
        var img = new Image()
        img.onload = function() {
          if (!inited) {
            inited = true
            callback(urls[i])
          }
        }
        img.onerror = function() {
          errCount++
          if (errCount > urls.length - 1) {
            typeof errorCb === 'function' && errorCb()
          }
        }
        img.src = urls[i] + window.API_F_PATH + '/captcha?width=100&height=50&length=4&t=' + Date.now() + '&key=' + Math.floor(Math.random() * 149545454) // ping
      }
      for (var i = 0; i < urls.length; i++) _loop(i)
    }
  },
  allDown: function(msg) {
    var msgbegin = '<div style="text-align:center;height:3rem;width:80%;line-height:3rem;padding: 0 .5rem;box-sizing:border-box;position: absolute;top: 10%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);" onclick="location.reload()">'
    var msgend = '</div>'
    var time = 30

    timer()
    function timer() {
      var content
      setTimeout(function() {
        if (time >= 0) {
          content = msgbegin + msg + '(倒计<span style="color: #f55; margin: 0; padding: 0;">' + time + '</span>秒)' + msgend
          timer()
        } else {
          location.reload()
          return
        }
        document.body.innerHTML = content
        time--
      }, 1000)
    }
  }
}
window.onload = function() {
  initLottery.start()
}
